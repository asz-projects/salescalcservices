﻿using MySql.Data.MySqlClient;
using SalesCalc.Business.Constants;
using SalesCalc.Business.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace SalesCalc.Business.Repository
{
    public class RoleRepository
    {

        public static object GetAllRoles()
        {
            try
            {
                List<Role> roles = new List<Role>();

                using (MySqlConnection connection = new MySqlConnection(SalesCalcConstants.DBConnectionString))
                {
                    using (MySqlCommand command = new MySqlCommand(SalesCalcConstants.sp_GetAllRoles, connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Connection.Open();
                        command.CommandTimeout = 180000;

                        MySqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            Role role = new Role();
                            role.RoleId = Convert.ToInt32(Convert.ToString(reader["RoleId"]));
                            role.RoleDescription = Convert.ToString(reader["RoleDescription"]);
                            role.RoleCode = Convert.ToString(reader["RoleCode"]);
                            var FirstName = (Convert.ToString(reader["FirstName"]));
                            var LastName = (Convert.ToString(reader["LastName"]));
                            role.CreatedBy = FirstName + " " + LastName;
                            role.RoleName = Convert.ToString(reader["RoleName"]);
                            role.CreatedDate = Convert.ToDateTime(reader["CreatedDate"]);
                            role.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
                           // role.UserTypeCode = Convert.ToString(reader["UserTypeCode"]);
                            roles.Add(role);
                        }
                        command.Connection.Close();
                    }
                }


                var response = new { sucess = true, roles = roles };
                return response;
            }
            catch (Exception e)
            {
                var response1 = new { sucess = false, Error = e.ToString() };
                return response1;
            }
        }

        public static object GetRolesByUserType(int UserTypeId)
        {
            try
            {
                List<Role> roles = new List<Role>();

                using (MySqlConnection connection = new MySqlConnection(SalesCalcConstants.DBConnectionString))
                {
                    using (MySqlCommand command = new MySqlCommand(SalesCalcConstants.sp_GetRolesByUserType, connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Connection.Open();
                        command.CommandTimeout = 180000;
                        command.Parameters.AddWithValue("@User_TypeId", UserTypeId);
                        MySqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            Role role = new Role();
                            role.RoleId = Convert.ToInt32(Convert.ToString(reader["RoleId"]));
                            role.RoleDescription = Convert.ToString(reader["RoleDescription"]);
                            role.RoleCode = Convert.ToString(reader["RoleCode"]);
                            var FirstName = (Convert.ToString(reader["FirstName"]));
                            var LastName = (Convert.ToString(reader["LastName"]));
                            role.CreatedBy = FirstName + " " + LastName;
                            role.RoleName = Convert.ToString(reader["RoleName"]);
                            role.CreatedDate = Convert.ToDateTime(reader["CreatedDate"]);
                            role.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
                           // role.UserTypeCode = Convert.ToString(reader["UserTypeCode"]);
                            roles.Add(role);
                        }
                        command.Connection.Close();
                    }
                }


                var response = new { sucess = true, roles = roles };
                return response;
            }
            catch (Exception e)
            {
                var response1 = new { sucess = false, Error = e.ToString() };
                return response1;
            }
        }
        public static object GetRoleById(Role role)
        {
            try
            {
                Role roledata = new Role();

                roledata.ParentNodes = new List<ParentNodes>();
                using (MySqlConnection connection = new MySqlConnection(SalesCalcConstants.DBConnectionString))
                {
                    using (MySqlCommand command = new MySqlCommand(SalesCalcConstants.sp_GetRoleById, connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Connection.Open();
                        command.CommandTimeout = 180000;
                        command.Parameters.AddWithValue("@Role_Id", role.RoleId);
                        MySqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            roledata.RoleId = Convert.ToInt32(Convert.ToString(reader["RoleId"]));
                            roledata.RoleDescription = Convert.ToString(reader["RoleDescription"]);
                            roledata.RoleCode = Convert.ToString(reader["RoleCode"]);
                            roledata.RoleName = Convert.ToString(reader["RoleName"]);
                            roledata.CreatedBy = (Convert.ToString(reader["CreatedBy"]));
                            roledata.CreatedDate = Convert.ToDateTime(reader["CreatedDate"]);
                            roledata.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
                            //roledata.UserTypeCode = Convert.ToString(reader["UserTypeCode"]);
                        }
                        command.Connection.Close();
                        roledata.RoleMappings = GetRoleMappingData(roledata.RoleId);
                        var NodeIds = GetNodesIdsForRole(roledata.RoleId);
                        var permissionids = GetallPermissionids(roledata.RoleId);
                        var permissions = PermissionsRepository.GetAllPermissions();
                        foreach (var check in permissions)
                        {
                            foreach (var nodeid in NodeIds)
                            {
                                if (check.NodeId == nodeid.NodeId)
                                {
                                    check.IsChecked = true;
                                }
                                
                            }
                            foreach (var chekpermis in check.Permissions)
                            {
                                foreach (var permis in permissionids)
                                {


                                    if (chekpermis.PermissionId == permis.PermissionId)
                                    {
                                        chekpermis.IsChecked = true;
                                    }
                                   
                                }
                            }
                            roledata.ParentNodes.Add(check);
                        }

                    }
                }
                roledata.ParentNodes = roledata.ParentNodes.OrderBy(x => x.ArrangeNodes).ToList();
                var response = new { sucess = true, roledata = roledata };
                return response;
            }
            catch (Exception e)
            {
                var response1 = new { sucess = false, Error = e.ToString() };
                return response1;
            }
        }





        public static object SideNavRolesById(Role role)
        {
            try
            {
                Role roledata = new Role();

                roledata.ParentNodes = new List<ParentNodes>();
                using (MySqlConnection connection = new MySqlConnection(SalesCalcConstants.DBConnectionString))
                {
                    using (MySqlCommand command = new MySqlCommand(SalesCalcConstants.sp_GetRoleById, connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Connection.Open();
                        command.CommandTimeout = 180000;
                        command.Parameters.AddWithValue("@Role_Id", role.RoleId);
                        MySqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            roledata.RoleId = Convert.ToInt32(Convert.ToString(reader["RoleId"]));
                            roledata.RoleDescription = Convert.ToString(reader["RoleDescription"]);
                            roledata.RoleCode = Convert.ToString(reader["RoleCode"]);
                            roledata.RoleName = Convert.ToString(reader["RoleName"]);
                            roledata.CreatedBy = (Convert.ToString(reader["CreatedBy"]));
                            roledata.CreatedDate = Convert.ToDateTime(reader["CreatedDate"]);
                            roledata.IsDeleted = Convert.ToBoolean(reader["IsDeleted"]);
                            //roledata.UserTypeCode = Convert.ToString(reader["UserTypeCode"]);
                        }
                        command.Connection.Close();
                        roledata.RoleMappings = GetRoleMappingData(roledata.RoleId);
                        var NodeIds = GetNodesIdsForRole(roledata.RoleId);
                        foreach (var NodeId in NodeIds)
                        {
                            var NodesData = GetParentNodesData(NodeId.NodeId, roledata.RoleId);
                            roledata.ParentNodes.Add(NodesData);
                        }
                    }
                }
                roledata.ParentNodes = roledata.ParentNodes.OrderBy(x => x.ArrangeNodes).ToList();
                var response = new { sucess = true, roledata = roledata };
                return response;

            }
            catch (Exception e)
            {
                var response1 = new { sucess = false, Error = e.ToString() };
                return response1;
            }
        }
        public static List<Permissions> GetallPermissionids(int roleId)
        {

            List<Permissions> ids = new List<Permissions>();
            using (MySqlConnection connection = new MySqlConnection(SalesCalcConstants.DBConnectionString))
            {
                using (MySqlCommand command = new MySqlCommand(SalesCalcConstants.sp_GetallPermissionids, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Connection.Open();
                    command.CommandTimeout = 180000;
                    command.Parameters.AddWithValue("@Role_Id", roleId);
                    MySqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        Permissions permission = new Permissions();
                        permission.PermissionId = Convert.ToInt32(reader["PermissionId"]);
                        ids.Add(permission);
                    }
                    command.Connection.Close();
                }
            }

            return ids;

        }

        public static List<RoleMapping> GetRoleMappingData(int roleId)
        {
            List<RoleMapping> roleMappings = new List<RoleMapping>();
            using (MySqlConnection connection = new MySqlConnection(SalesCalcConstants.DBConnectionString))
            {
                using (MySqlCommand command = new MySqlCommand(SalesCalcConstants.sp_GetRoleMappingData, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Connection.Open();
                    command.CommandTimeout = 180000;
                    command.Parameters.AddWithValue("@Role_Id", roleId);
                    MySqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        RoleMapping roleMapping = new RoleMapping();
                        roleMapping.RoleMappingId = Convert.ToInt32(reader["RoleMappingId"]);
                        roleMapping.RoleId = Convert.ToInt32(reader["RoleId"]);
                        roleMapping.PermissionId = Convert.ToInt32(reader["PermissionId"]);
                        roleMapping.CreatedBy = (Convert.ToString(reader["CreatedBy"]));
                        roleMapping.CreatedDate = Convert.ToDateTime(reader["CreatedDate"]);
                        roleMappings.Add(roleMapping);
                    }
                    command.Connection.Close();
                }
            }
            return roleMappings;
        }


        public static List<ParentNodes> GetNodesIdsForRole(int roleId)
        {
            List<ParentNodes> nodeIds = new List<ParentNodes>();
            using (MySqlConnection connection = new MySqlConnection(SalesCalcConstants.DBConnectionString))
            {
                using (MySqlCommand command = new MySqlCommand(SalesCalcConstants.sp_GetNodesIdsForRole, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Connection.Open();
                    command.CommandTimeout = 180000;
                    command.Parameters.AddWithValue("@Role_Id", roleId);
                    MySqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        ParentNodes node = new ParentNodes();
                        node.NodeId = Convert.ToInt32(reader["ParentNodeId"]);
                        nodeIds.Add(node);
                    }
                    command.Connection.Close();
                }
            }
            return nodeIds;
        }
        public static ParentNodes GetParentNodesData(int nodeId, int roleId)
        {

            ParentNodes parentNodes = new ParentNodes();
            parentNodes.Permissions = new List<Permissions>();
            using (MySqlConnection connection = new MySqlConnection(SalesCalcConstants.DBConnectionString))
            {
                using (MySqlCommand command = new MySqlCommand(SalesCalcConstants.sp_GetParentNodesData, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Connection.Open();
                    command.CommandTimeout = 180000;
                    command.Parameters.AddWithValue("@Node_Id", nodeId);
                    command.Parameters.AddWithValue("@Role_Id", roleId);
                    MySqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        Permissions permission = new Permissions();
                        parentNodes.NodeId = Convert.ToInt32(reader["ParentNodeId"]);
                        if (reader["ArrangeNodes"] != System.DBNull.Value)
                        {
                            parentNodes.ArrangeNodes = Convert.ToInt32(reader["ArrangeNodes"]);
                        }

                        parentNodes.NodeName = Convert.ToString(reader["ParentNodeName"]);
                        parentNodes.NodeIcon = Convert.ToString(reader["NodeIcon"]);
                        parentNodes.CreatedBy = (Convert.ToString(reader["CreatedBy"]));
                        parentNodes.CreatedDate = Convert.ToDateTime(reader["CreatedDate"]);
                        parentNodes.Path = Convert.ToString(reader["ParentPath"]);
                        parentNodes.Type = Convert.ToString(reader["Type"]);
                        permission.PermissionId = Convert.ToInt32(reader["PermissionId"]);
                        if (reader["ParentNodeCode"] != System.DBNull.Value)
                        {
                            permission.Code = Convert.ToString(reader["ParentNodeCode"]);
                        }

                        permission.PermissionName = Convert.ToString(reader["PermissionName"]);
                        permission.PermissionDescription = Convert.ToString(reader["PermissionDescription"]);
                        permission.NodeId = Convert.ToInt32(reader["ParentNodeId"]);
                        permission.Icon = Convert.ToString(reader["Icon"]);
                        permission.State = Convert.ToString(reader["State"]);
                        permission.Type = Convert.ToString(reader["PermissionType"]);
                        permission.CreatedBy = (Convert.ToString(reader["CreatedBy"]));
                        permission.CreatedDate = Convert.ToDateTime(reader["CreatedDate"]);
                        permission.Path = Convert.ToString(reader["Path"]);

                        parentNodes.Permissions.Add(permission);
                    }
                    command.Connection.Close();
                }
            }

            return parentNodes;

        }



        public static int SaveRole(Role role)
        {

            int roleId = 0;
            using (MySqlConnection connection = new MySqlConnection(SalesCalcConstants.DBConnectionString))
            {
                using (MySqlCommand command = new MySqlCommand(SalesCalcConstants.sp_CreateRole, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Connection.Open();
                    command.CommandTimeout = 180000;
                    command.Parameters.AddWithValue("@Role_Description", role.RoleDescription);
                    command.Parameters.AddWithValue("@Role_Code", role.RoleCode);
                    command.Parameters.AddWithValue("@Role_Name", role.RoleName);
                    command.Parameters.AddWithValue("@Created_By", role.CreatedBy);
                    command.Parameters.AddWithValue("@_Id", role.RoleId);
                  //  command.Parameters.AddWithValue("@UserType_Code", role.UserTypeCode);
                    MySqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        roleId = Convert.ToInt32(reader["Role_Id"]);
                    }
                    command.Connection.Close();
                    //CreateRoleMappingData(role);
                    // isSuccessFull = true;
                }
            }

            return roleId;

        }


        public static object CreateRoleMappingData(Role role)
        {
            try
            {
                bool isSuccessFull = false;
                foreach (var item in role.RoleMappings)
                {
                    using (MySqlConnection connection = new MySqlConnection(SalesCalcConstants.DBConnectionString))
                    {
                        using (MySqlCommand command = new MySqlCommand(SalesCalcConstants.sp_CreateRoleMappingData, connection))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.Connection.Open();
                            command.CommandTimeout = 180000;
                            command.Parameters.AddWithValue("@Role_Id", role.RoleId);
                            command.Parameters.AddWithValue("@Permission_Id", item.PermissionId);
                            command.Parameters.AddWithValue("@Created_By", role.CreatedBy);
                            command.ExecuteNonQuery();
                            command.Connection.Close();
                            isSuccessFull = true;
                        }
                    }
                }
                var response = new { sucess = true, isSuccessFull = isSuccessFull };
                return response;
            }
            catch (Exception e)
            {
                var response1 = new { sucess = false, Error = e.ToString() };
                return response1;
            }
        }


        public static object DeleteRoleMappingData(Role role)
        {
            try
            {
                bool isSuccessFull = false;
                using (MySqlConnection connection = new MySqlConnection(SalesCalcConstants.DBConnectionString))
                {
                    using (MySqlCommand command = new MySqlCommand(SalesCalcConstants.sp_DeleteRoleMappingData, connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Connection.Open();
                        command.CommandTimeout = 180000;
                        command.Parameters.AddWithValue("@Role_Id", role.RoleId);
                        command.ExecuteNonQuery();
                        command.Connection.Close();
                        isSuccessFull = true;
                    }
                }
                var response = new { sucess = true, isSuccessFull = isSuccessFull };
                return response;
            }
            catch (Exception e)
            {
                var response1 = new { sucess = false, Error = e.ToString() };
                return response1;
            }
        }

        public static object DeleteRole(Role role)
        {
            try
            {
                bool isSucessfull = false;

                using (MySqlConnection connection = new MySqlConnection(SalesCalcConstants.DBConnectionString))
                {
                    using (MySqlCommand command = new MySqlCommand(SalesCalcConstants.sp_DeleteRole, connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Connection.Open();
                        command.CommandTimeout = 180000;
                        command.Parameters.AddWithValue("@Role_Id", role.RoleId);
                        MySqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            isSucessfull = Convert.ToBoolean(reader["Success"]);
                        }
                        command.Connection.Close();
                    }
                }
                var response = new { sucess = true, isSuccessFull = isSucessfull };
                return response;
            }
            catch (Exception e)
            {
                var response1 = new { sucess = false, Error = e.ToString() };
                return response1;
            }
        }
    }
}
