﻿using MySql.Data.MySqlClient;
using SalesCalc.Business.Constants;
using SalesCalc.Business.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace SalesCalc.Business.Repository
{
    public class MasterDataRepository
    {
        public static Folding GetAllFoldingsData()
        {
            Folding foldings = new Folding();
            foldings.Folded = new List<Folded>();
            foldings.Opened = new List<Opened>();
            foldings.Finishing = new List<Finishing>();
            foldings.PaperWrapped = new List<PaperWrapped>();
            foldings.SheetSize = new List<SheetSize>();
            foldings.Paper = new List<Paper>();
            foldings.colours = new List<Colours>();
            foldings.Folded.AddRange(GetFoldedSizeData());
            foldings.Opened.AddRange(GetOpenedSizeData());
            foldings.Finishing.AddRange(GetFoldingFinishingData());
            foldings.PaperWrapped.AddRange(GetPaperWrappedData());
            foldings.SheetSize.AddRange(GetSheetSizeData());
            foldings.Paper.AddRange(GetPaperData());
            foldings.colours.AddRange(GetAllColoursData());
            return foldings;
        }

        public static List<Folded> GetFoldedSizeData()
        {
            List<Folded> foldeds = new List<Folded>();
            using (MySqlConnection connection = new MySqlConnection(SalesCalcConstants.SalesCalcConnection))
            {
                using (MySqlCommand command = new MySqlCommand(SalesCalcConstants.sp_GetFoldedSizeData, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Connection.Open();
                    command.CommandTimeout = 180000;
                    MySqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        Folded folded = new Folded();
                        folded.FoldedSizeId = Convert.ToInt32(reader["FoldedSizeId"]);
                        folded.FoldedSize = Convert.ToString(reader["FoldedSize"]);
                        folded.FoldedHeight = Convert.ToString(reader["FoldedHeight"]);
                        folded.FoldedWidth = Convert.ToString(reader["FoldedWidth"]);
                        foldeds.Add(folded);
                    }
                    command.Connection.Close();
                }
            }
            return foldeds;

        }

        public static List<Opened> GetOpenedSizeData()
        {
            List<Opened> openeds = new List<Opened>();
            using (MySqlConnection connection = new MySqlConnection(SalesCalcConstants.SalesCalcConnection))
            {
                using (MySqlCommand command = new MySqlCommand(SalesCalcConstants.sp_GetOpenedSizeData, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Connection.Open();
                    command.CommandTimeout = 180000;
                    MySqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        Opened opened = new Opened();
                        opened.OpenedSizeId = Convert.ToInt32(reader["OpenedSizeId"]);
                        opened.OpenedSize = Convert.ToString(reader["OpenedSize"]);
                        opened.OpenedHeight = Convert.ToString(reader["OpenedHeight"]);
                        opened.OpenedWidth = Convert.ToString(reader["OpenedWidth"]);
                        openeds.Add(opened);
                    }
                    command.Connection.Close();
                }
            }
            return openeds;

        }

        public static List<Finishing> GetFoldingFinishingData()
        {
            List<Finishing> finishings = new List<Finishing>();
            using (MySqlConnection connection = new MySqlConnection(SalesCalcConstants.SalesCalcConnection))
            {
                using (MySqlCommand command = new MySqlCommand(SalesCalcConstants.sp_GetFoldingFinishingData, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Connection.Open();
                    command.CommandTimeout = 180000;
                    MySqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        Finishing finishing = new Finishing();
                        finishing.FoldingFinishingId = Convert.ToInt32(reader["FoldingFinishingId"]);
                        finishing.FinishingType = Convert.ToString(reader["Finishing"]);
                        finishings.Add(finishing);
                    }
                    command.Connection.Close();
                }
            }
            return finishings;

        }


        public static List<PaperWrapped> GetPaperWrappedData()
        {
            List<PaperWrapped> paperWrappeds = new List<PaperWrapped>();
            using (MySqlConnection connection = new MySqlConnection(SalesCalcConstants.SalesCalcConnection))
            {
                using (MySqlCommand command = new MySqlCommand(SalesCalcConstants.sp_GetPaperWrappedData, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Connection.Open();
                    command.CommandTimeout = 180000;
                    MySqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        PaperWrapped paperWrapped = new PaperWrapped();
                        paperWrapped.PaperWrappedId = Convert.ToInt32(reader["PaperWrappedId"]);
                        paperWrapped.PaperWrappedQuantity = Convert.ToString(reader["PaperWrappedQuantity"]);
                        paperWrappeds.Add(paperWrapped);
                    }
                    command.Connection.Close();
                }
            }
            return paperWrappeds;

        }

        public static List<SheetSize> GetSheetSizeData()
        {
            List<SheetSize> sheetSizes = new List<SheetSize>();
            using (MySqlConnection connection = new MySqlConnection(SalesCalcConstants.SalesCalcConnection))
            {
                using (MySqlCommand command = new MySqlCommand(SalesCalcConstants.sp_GetSheetSizeData, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Connection.Open();
                    command.CommandTimeout = 180000;
                    MySqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        SheetSize sheetSize = new SheetSize();
                        sheetSize.SheetSizeId = Convert.ToInt32(reader["SheetSizeId"]);
                        sheetSize.Size = Convert.ToString(reader["SheetSize"]);
                        sheetSize.SheetHeight = Convert.ToString(reader["SheetHeight"]);
                        sheetSize.SheetWidth = Convert.ToString(reader["SheetWidth"]);
                        sheetSizes.Add(sheetSize);
                    }
                    command.Connection.Close();
                }
            }
            return sheetSizes;

        }

        public static List<Paper> GetPaperData()
        {
            List<Paper> papers = new List<Paper>();
            using (MySqlConnection connection = new MySqlConnection(SalesCalcConstants.SalesCalcConnection))
            {
                using (MySqlCommand command = new MySqlCommand(SalesCalcConstants.sp_GetPaperData, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Connection.Open();
                    command.CommandTimeout = 180000;
                    MySqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        Paper paper = new Paper();
                        paper.PaperId = Convert.ToInt32(reader["PaperId"]);
                        paper.PaperType = Convert.ToString(reader["PaperType"]);
                        paper.GSM = GetGSMPricesById(paper.PaperId);
                        //paper.Price = Convert.ToString(reader["Price"]);
                        paper.PaperHeight = Convert.ToString(reader["PaperHeight"]);
                        paper.PaperWidth = Convert.ToString(reader["PaperWidth"]);
                        paper.Validity = Convert.ToString(reader["Validity"]);
                        papers.Add(paper);
                    }
                    command.Connection.Close();
                }
            }
            return papers;

        }

        public static CaseBook GetAllCaseBookData()
        {
            CaseBook caseBook = new CaseBook();
            caseBook.Folded = new List<Folded>();
            caseBook.Opened = new List<Opened>();
            caseBook.Paper = new List<Paper>();
            caseBook.extents = new List<Extent>();
            caseBook.extentMappings = new List<ExtentMapping>();
            caseBook.Folded.AddRange(GetFoldedSizeData());
            caseBook.Opened.AddRange(GetOpenedSizeData());
            caseBook.extents.AddRange(GetAllExtents());
            caseBook.Paper.AddRange(GetPaperData());
            caseBook.extentMappings.AddRange(GetExtentMapping());
            return caseBook;
        }

        public static List<Extent> GetAllExtents()
        {
            List<Extent> extents = new List<Extent>();
            using (MySqlConnection connection = new MySqlConnection(SalesCalcConstants.SalesCalcConnection))
            {
                using (MySqlCommand command = new MySqlCommand(SalesCalcConstants.sp_GetAllExtents, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Connection.Open();
                    command.CommandTimeout = 180000;
                    MySqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        Extent extent = new Extent();
                        extent.ExtentId = Convert.ToInt32(reader["ExtentId"]);
                        extent.ExtentName = Convert.ToString(reader["ExtentName"]);
                        extent.ExtentCode = Convert.ToString(reader["ExtentCode"]);
                        extents.Add(extent);
                    }
                    command.Connection.Close();
                }
            }
            return extents;

        }
        public static List<GSMPrice> GetGSMPricesById(int PaperId)
        {
            List<GSMPrice> GsmPrices = new List<GSMPrice>();
            using (MySqlConnection connection = new MySqlConnection(SalesCalcConstants.SalesCalcConnection))
            {
                using (MySqlCommand command = new MySqlCommand(SalesCalcConstants.sp_GetGSMPricesById, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Connection.Open();
                    command.CommandTimeout = 180000;
                    command.Parameters.AddWithValue("@_PaperId", PaperId);
                    MySqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        GSMPrice GsmPrice = new GSMPrice();
                        GsmPrice.GSM = Convert.ToString(reader["GSM"]);
                        GsmPrice.Price = Convert.ToString(reader["Price"]);                       
                        GsmPrices.Add(GsmPrice);
                    }
                    command.Connection.Close();
                }
            }
            return GsmPrices;

        }
        public static List<ExtentMapping> GetExtentMapping()
        {
            List<ExtentMapping> extentMappings = new List<ExtentMapping>();
            using (MySqlConnection connection = new MySqlConnection(SalesCalcConstants.SalesCalcConnection))
            {
                using (MySqlCommand command = new MySqlCommand(SalesCalcConstants.sp_GetExtentMapping, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Connection.Open();
                    command.CommandTimeout = 180000;
                    MySqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        ExtentMapping extentMapping = new ExtentMapping();
                        extentMapping.caseBookingFinishings = new List<CaseBookingFinishing>();
                        CaseBookingFinishing caseBookingFinishing = new CaseBookingFinishing();
                        extentMapping.ExtentId = Convert.ToInt32(reader["ExtentId"]);
                        extentMapping.ExtentName = Convert.ToString(reader["ExtentName"]);
                        caseBookingFinishing.CaseBookFinishingName = Convert.ToString(reader["CaseBookFinishingName"]);
                        var Result = extentMappings.Exists(x => x.ExtentId == extentMapping.ExtentId);
                        if (Result)
                        {
                            foreach (var items in extentMappings)
                            {
                                if(items.ExtentId == extentMapping.ExtentId)
                                {
                                    items.caseBookingFinishings.Add(caseBookingFinishing);
                                }
                            }
                        }
                        else
                        {
                            extentMapping.caseBookingFinishings.Add(caseBookingFinishing);
                            extentMappings.Add(extentMapping);
                        }

                    }
                    command.Connection.Close();
                }
            }
            return extentMappings;

        }



        public static LimpBound GetAllLimpBoundData()
        {
            LimpBound limpBound = new LimpBound();
            limpBound.Folded = new List<Folded>();
            limpBound.Opened = new List<Opened>();
            limpBound.Paper = new List<Paper>();
            limpBound.Folded.AddRange(GetFoldedSizeData());
            limpBound.Opened.AddRange(GetOpenedSizeData());
            limpBound.Paper.AddRange(GetPaperData());
            return limpBound;
        }


        public static SaddleStitched GetAllSaddleStitchedData()
        {
            SaddleStitched saddleStitched = new SaddleStitched();
            saddleStitched.Folded = new List<Folded>();
            saddleStitched.Opened = new List<Opened>();
            saddleStitched.Paper = new List<Paper>();
            saddleStitched.Folded.AddRange(GetFoldedSizeData());
            saddleStitched.Opened.AddRange(GetOpenedSizeData());
            saddleStitched.Paper.AddRange(GetPaperData());
            return saddleStitched;
        }

        public static List<Colours> GetAllColoursData()
        {
            List<Colours> colours = new List<Colours>();
            using (MySqlConnection connection = new MySqlConnection(SalesCalcConstants.SalesCalcConnection))
            {
                using (MySqlCommand command = new MySqlCommand(SalesCalcConstants.sp_GetAllColoursData, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Connection.Open();
                    command.CommandTimeout = 180000;
                    MySqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        Colours colour = new Colours();
                        colour.ColourId = Convert.ToInt32(reader["ColourId"]);
                        colour.ColourTypeName = Convert.ToString(reader["ColourTypeName"]);
                        colour.ColourTypeCode = Convert.ToString(reader["ColourTypeCode"]);
                        colour.ColourRates = Convert.ToDouble(reader["ColourRates"]);
                        colours.Add(colour);
                    }
                    command.Connection.Close();
                }
            }
            return colours;

        }

    }
}
