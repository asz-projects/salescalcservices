﻿using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using OfficeOpenXml;
using SalesCalc.Business.Common;
using SalesCalc.Business.Constants;
using SalesCalc.Business.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;

namespace SalesCalc.Business.Repository
{
    public class ProductsRepository
    {
        public static List<Products> GetAllProducts()
        {
            List<Products> products = new List<Products>();
            using (MySqlConnection connection = new MySqlConnection(SalesCalcConstants.SalesCalcConnection))
            {
                using (MySqlCommand command = new MySqlCommand(SalesCalcConstants.sp_GetAllProducts, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Connection.Open();
                    command.CommandTimeout = 180000;
                    MySqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        Products product = new Products();
                        product.ProductId = Convert.ToInt32(reader["ProductId"]);
                        product.ProductName = Convert.ToString(reader["ProductName"]);
                        product.ProductCode = Convert.ToString(reader["ProductCode"]);
                        products.Add(product);

                    }
                    command.Connection.Close();
                }
            }
            return products;
        }


        public static string GetExcelReportForFolding(FoldingFinalData finalData)
        {
            try
            {
                GetFoldedSizeData(JsonConvert.SerializeObject(finalData));
                FoldingFinalResult finalResult = new FoldingFinalResult();
                finalResult.PackingForFolding = new PackingForFolding();
                finalResult.DeliveryForFolding = new DeliveryForFolding();
                var ColorFront = "";
                var ColorBack = "";
                finalResult.prepress = new List<Prepress>();
                finalResult.press = new List<Press>();
                finalResult.postPress = new List<PostPress>();
                if (finalData.GlossVarnish == 2)
                {
                    finalResult.WorkAndTurn = 2;
                }
                else if (finalData.MattVarnish == 2)
                {
                    finalResult.WorkAndTurn = 2;
                }
                else if (finalData.SpotGlossVarnish == 2)
                {
                    finalResult.WorkAndTurn = 2;
                }
                else if (finalData.SpotMattVarnish == 2)
                {
                    finalResult.WorkAndTurn = 2;
                }
                else if (finalData.SpotGlossandMattVarnish == 2)
                {
                    finalResult.WorkAndTurn = 2;
                }
                else if (finalData.GlossLam == 2)
                {
                    finalResult.WorkAndTurn = 2;
                }
                else if (finalData.MattLam == 2)
                {
                    finalResult.WorkAndTurn = 2;
                }
                else if (finalData.ThroughoutUVVarnish == 2)
                {
                    finalResult.WorkAndTurn = 2;
                }
                else
                {
                    finalResult.WorkAndTurn = 1;
                }
                var Calc = CommonCalculations.GetCommonCalculation(finalData.OpenedSize, finalData.FoldedSize);
                var ColoursData = MasterDataRepository.GetAllColoursData();

                var FoldingWastage = GetWastageDataForFolding();
                var FoldingRates = GetFoldingRates();
                //finalResult.WorkAndTurn = Calc.WorkAndTurn;
                finalResult.SheetSize = Calc.SheetSize;
                var SheetHeight = Convert.ToDouble(finalResult.SheetSize.Split('X').FirstOrDefault());
                var SheetWidth = Convert.ToDouble(finalResult.SheetSize.Split('X').LastOrDefault());
                finalResult.PriceByReam = ((((SheetHeight / 1000) * (SheetWidth / 1000) * (Convert.ToDouble(finalData.GSM))) / 2) * 2.2046 * (Convert.ToDouble(finalData.Price)));
                finalResult.PriceBySheet = finalResult.PriceByReam / 500;
                finalResult.CutValue = Calc.Cut;
                finalResult.NoOfUps = Calc.Ups;
                List<Quantity> quantities = new List<Quantity>();
                //int i;
                //for (i = 0; i <= 2; i++)
                int i = 1;
                foreach (var Quantities in finalData.QuantityRequired)
                {
                   Prepress prepress = new Prepress();
                   Press press = new Press();
                   PostPress postPress = new PostPress();
                    var Quantity = Quantities + finalData.AdvanceCopy;
                    Quantity quantity = new Quantity();
                    if (Enumerable.Range(1, 1000).Contains(Quantity))
                    {
                        foreach (var items in FoldingRates)
                        {
                            items.Price = items.To1000;
                        }
                    }
                    else if (Enumerable.Range(1001, 10000).Contains(Quantity))
                    {
                        foreach (var items in FoldingRates)
                        {
                            items.Price = items.To10000;
                        }
                    }
                    else if (Enumerable.Range(10001, 50000).Contains(Quantity))
                    {
                        foreach (var items in FoldingRates)
                        {
                            items.Price = items.To50000;
                        }
                    }
                    else
                    {
                        foreach (var items in FoldingRates)
                        {
                            items.Price = items.Above50000;
                        }
                    }
                    //if(i == 1)
                    //{
                    //    finalData.QuantityRequired = Convert.ToString(Convert.ToInt32(finalData.QuantityRequired) * 10);
                    //}
                    //if (i == 2)
                    //{
                    //    finalData.QuantityRequired = Convert.ToString(Convert.ToInt32(finalData.QuantityRequired) * 15);
                    //}
                    finalResult.ActualPaperReq = Quantity / finalResult.CutValue;
                    var ColorHeight = Convert.ToDouble(finalData.ProcessColor.Split('X').FirstOrDefault());
                    var ColorWidth = Convert.ToDouble(finalData.ProcessColor.Split('X').LastOrDefault());


                    var PMSColorHeight = Convert.ToDouble(finalData.PMSColor.Split('X').FirstOrDefault());
                    var PMSColorWidth = Convert.ToDouble(finalData.PMSColor.Split('X').LastOrDefault());

                    var MetalicColorHeight = Convert.ToDouble(finalData.MetalicColor.Split('X').FirstOrDefault());
                    var MetalicColorWidth = Convert.ToDouble(finalData.MetalicColor.Split('X').LastOrDefault());


                    if (i == 1)
                    {
                        ColorFront = "Front - " + ColorHeight + "c " + "Process +" + PMSColorHeight + "c " + "PMS +" + MetalicColorHeight + "c " + "Metallic";
                        ColorBack = "Back - " + ColorWidth + "c " + "Process +" + PMSColorWidth + "c " + "PMS +" + MetalicColorWidth + "c " + "Metallic";
                    }


                    var PrintingWastage = FoldingWastage.Where(x => x.FoldingWastageCode == "Printing").Select(x => x.FoldingWastageCost).FirstOrDefault();
                    finalResult.Printing = (((ColorHeight + ColorWidth) * PrintingWastage) / (finalResult.CutValue / finalResult.NoOfUps));

                    var VarnishWastage = FoldingWastage.Where(x => x.FoldingWastageCode == "Varnish").Select(x => x.FoldingWastageCost).FirstOrDefault();
                    finalResult.Varnish = Math.Ceiling((((finalData.GlossVarnish + finalData.MattVarnish) * VarnishWastage) / (finalResult.CutValue / finalResult.NoOfUps)));

                    var SpotVarnishWastage = FoldingWastage.Where(x => x.FoldingWastageCode == "SpotVarnish").Select(x => x.FoldingWastageCost).FirstOrDefault();
                    finalResult.SpotVarnish = Math.Ceiling((((finalData.SpotGlossandMattVarnish + finalData.SpotGlossVarnish + finalData.SpotMattVarnish) * SpotVarnishWastage) / (finalResult.CutValue / finalResult.NoOfUps)));

                    //finalResult.SpotVarnish = 0;

                    var GlossLamWastage = FoldingWastage.Where(x => x.FoldingWastageCode == "GlossLam").Select(x => x.FoldingWastageCost).FirstOrDefault();
                    finalResult.GlossLam = Math.Ceiling(((finalData.GlossLam * GlossLamWastage) / (finalResult.CutValue / finalResult.NoOfUps)));

                    var MattLamWastage = FoldingWastage.Where(x => x.FoldingWastageCode == "MattLam").Select(x => x.FoldingWastageCost).FirstOrDefault();
                    finalResult.MattLam = Math.Ceiling(((finalData.MattLam * MattLamWastage) / (finalResult.CutValue / finalResult.NoOfUps)));

                    var VelvetWastage = FoldingWastage.Where(x => x.FoldingWastageCode == "Velvet").Select(x => x.FoldingWastageCost).FirstOrDefault();
                    finalResult.VelvetLam = Math.Ceiling(((finalData.VelvetLam * VelvetWastage) / (finalResult.CutValue / finalResult.NoOfUps)));

                    var AntiScuffMattWastage = FoldingWastage.Where(x => x.FoldingWastageCode == "AntiScuffMatt").Select(x => x.FoldingWastageCost).FirstOrDefault();
                    finalResult.AntiScuffMattLam = Math.Ceiling(((finalData.AntiScuffMattLam * AntiScuffMattWastage) / (finalResult.CutValue / finalResult.NoOfUps)));

                    var UVVarnishWastage = FoldingWastage.Where(x => x.FoldingWastageCode == "UVVarnish").Select(x => x.FoldingWastageCost).FirstOrDefault();
                    finalResult.UVVarnish = Math.Ceiling(((finalData.ThroughoutUVVarnish * UVVarnishWastage) / (finalResult.CutValue / finalResult.NoOfUps)));

                    var SpotUVWastage = FoldingWastage.Where(x => x.FoldingWastageCode == "SpotUV").Select(x => x.FoldingWastageCost).FirstOrDefault();
                    finalResult.SpotUV = Math.Ceiling(((finalData.SpotUV * SpotUVWastage) / (finalResult.CutValue / finalResult.NoOfUps)));

                    var OrangePeelEffectWastage = FoldingWastage.Where(x => x.FoldingWastageCode == "OrangePeelEffect").Select(x => x.FoldingWastageCost).FirstOrDefault();
                    finalResult.OrangePeelEffect = Math.Ceiling(((finalData.OrangePeelEffect * OrangePeelEffectWastage) / (finalResult.CutValue / finalResult.NoOfUps)));

                    var FoilStampWastage = FoldingWastage.Where(x => x.FoldingWastageCode == "FoilStamp").Select(x => x.FoldingWastageCost).FirstOrDefault();
                    finalResult.FoilStamp = Math.Ceiling(((finalData.FoilStamp * FoilStampWastage) / (finalResult.CutValue / finalResult.NoOfUps)));

                    var BlindEmbossWastage = FoldingWastage.Where(x => x.FoldingWastageCode == "BlindEmboss").Select(x => x.FoldingWastageCost).FirstOrDefault();
                    finalResult.BlindEmboss = Math.Ceiling(((finalData.BlindEmboss * BlindEmbossWastage) / (finalResult.CutValue / finalResult.NoOfUps)));

                    var Folding = FoldingWastage.Where(x => x.FoldingWastageCode == "Folding").Select(x => x.FoldingWastageCost).FirstOrDefault();
                    finalResult.Folding = Math.Ceiling((Folding / finalResult.CutValue));

                    finalResult.TotalPaper = finalResult.ActualPaperReq + finalResult.Printing + finalResult.Varnish + finalResult.SpotVarnish + finalResult.GlossLam + finalResult.MattLam + finalResult.VelvetLam + finalResult.AntiScuffMattLam + finalResult.OrangePeelEffect + finalResult.FoilStamp + finalResult.BlindEmboss + finalResult.UVVarnish + finalResult.SpotUV + finalResult.Folding;
                    var PaperRate = FoldingRates.Where(x => x.FoldingRatesName == "Paper").Select(x => x.Price).FirstOrDefault();
                    finalResult.PaperRate = finalResult.TotalPaper * PaperRate;

                    var OpenedHeight = Convert.ToDouble(finalData.OpenedSize.Split('X').FirstOrDefault());
                    var OpenedWidth = Convert.ToDouble(finalData.OpenedSize.Split('X').LastOrDefault());
                    var Plotter = FoldingRates.Where(x => x.FoldingRatesName == "Plotter").Select(x => x.Price).FirstOrDefault();
                    prepress.Plotter = ((OpenedHeight * OpenedWidth) * Plotter * 1 * 2);

                    if (Quantity <= 2000)
                    {
                        var CTPPlates = FoldingRates.Where(x => x.FoldingRatesName == "CTPPlatesSmall").Select(x => x.Price).FirstOrDefault();
                        prepress.CTPPlates = (((ColorHeight + ColorWidth + (finalData.SpotGlossandMattVarnish + finalData.SpotGlossVarnish + finalData.SpotMattVarnish)) / finalResult.WorkAndTurn) * CTPPlates);
                    }
                    else
                    {
                        var CTPPlates = FoldingRates.Where(x => x.FoldingRatesName == "CTPPlatesBig").Select(x => x.Price).FirstOrDefault();
                        prepress.CTPPlates = (((ColorHeight + ColorWidth + (finalData.SpotGlossandMattVarnish + finalData.SpotGlossVarnish + finalData.SpotMattVarnish)) / finalResult.WorkAndTurn) * CTPPlates);
                    }


                    var PrintingMakeReady = FoldingRates.Where(x => x.FoldingRatesName == "PrintingMakeReady").Select(x => x.Price).FirstOrDefault();
                    press.PrintingMakeready = (((ColorHeight + ColorWidth) / finalResult.WorkAndTurn) * PrintingMakeReady);

                    var RepeatedCal = (Convert.ToDouble(Quantity) / finalResult.NoOfUps) / 1000;

                    //var PrintingProcess = FoldingRates.Where(x => x.FoldingRatesName == "PrintingProcess").Select(x => x.Price).FirstOrDefault();
                    //if (finalData.Processed)
                    //{
                    var PrintingProcess = ColoursData.Where(x => x.ColourTypeCode == "Processed").Select(x => x.ColourRates).FirstOrDefault();
                    press.PrintingProcess = ((RepeatedCal) * ((ColorHeight + ColorWidth) * (PrintingProcess)));
                    //}
                    //if (finalData.PMS)
                    //{
                    var PrintingPMS = ColoursData.Where(x => x.ColourTypeCode == "pms").Select(x => x.ColourRates).FirstOrDefault();
                    press.PrintingPMS = ((RepeatedCal) * ((PMSColorHeight + PMSColorWidth) * (PrintingPMS)));
                    //}
                    //if (finalData.Metalic)
                    //{
                    var PrintingMetalic = ColoursData.Where(x => x.ColourTypeCode == "Metalic").Select(x => x.ColourRates).FirstOrDefault();
                    press.PrintingMetalic = ((RepeatedCal) * ((MetalicColorHeight + MetalicColorWidth) * (PrintingMetalic)));
                    //}


                    var VarnishMakeReady = FoldingRates.Where(x => x.FoldingRatesName == "VarnishMakeReady").Select(x => x.Price).FirstOrDefault();
                    press.VarnishMakeready = (finalData.GlossVarnish + finalData.MattVarnish) * VarnishMakeReady;

                    var VarnishProcess = FoldingRates.Where(x => x.FoldingRatesName == "VarnishProcess").Select(x => x.Price).FirstOrDefault();
                    press.VarnishProcess = ((RepeatedCal) * (finalData.GlossVarnish + finalData.MattVarnish) * VarnishProcess);

                    var SpotVarnishMakeReady = FoldingRates.Where(x => x.FoldingRatesName == "SpotVarnishMakeReady").Select(x => x.Price).FirstOrDefault();
                    press.SpotVarnishMakeready = SpotVarnishMakeReady * (finalData.SpotGlossandMattVarnish + finalData.SpotGlossVarnish + finalData.SpotMattVarnish);

                    var SpotVarnishProcess = FoldingRates.Where(x => x.FoldingRatesName == "SpotVarnishProcess").Select(x => x.Price).FirstOrDefault();
                   press.SpotVarnishProcess = ((RepeatedCal) * (finalData.SpotGlossandMattVarnish + finalData.SpotGlossVarnish + finalData.SpotMattVarnish) * SpotVarnishProcess);

                    if (finalData.GlossLam != 0)
                    {
                        var GlossLaminate = FoldingRates.Where(x => x.FoldingRatesName == "GlossLaminate").Select(x => x.Price).FirstOrDefault();
                        var GlossLaminateValue = ((OpenedHeight * OpenedWidth * finalData.GlossLam) * 0.00000038 * Convert.ToDouble(Quantity));
                        if (GlossLaminateValue <= 50)
                        {
                            postPress.GlossLaminate = 50;
                        }
                        else
                        {
                            postPress.GlossLaminate = GlossLaminateValue;
                        }
                    }

                    if (finalData.MattLam != 0)
                    {
                        var MattLaminate = FoldingRates.Where(x => x.FoldingRatesName == "MattLaminate").Select(x => x.Price).FirstOrDefault();
                        var MattLaminateValue = ((OpenedHeight * OpenedWidth * finalData.MattLam) * MattLaminate * Quantity);
                        if (MattLaminateValue <= 50)
                        {
                            postPress.MattLaminate = 50;
                        }
                        else
                        {
                            postPress.MattLaminate = MattLaminateValue;
                        }
                    }
                    if (finalData.VelvetLam != 0)
                    {
                        var VelvetLaminate = FoldingRates.Where(x => x.FoldingRatesName == "VelvetLaminate").Select(x => x.Price).FirstOrDefault();
                        var VelvetLaminateValue = ((OpenedHeight * OpenedWidth * finalData.VelvetLam) * 0.00000038 * Convert.ToDouble(Quantity));
                        if (VelvetLaminateValue <= 50)
                        {
                            postPress.VelvetLaminate = 50;
                        }
                        else
                        {
                            postPress.VelvetLaminate = VelvetLaminateValue;
                        }
                    }

                    if (finalData.AntiScuffMattLam != 0)
                    {
                        var AntiScuffMattLaminate = FoldingRates.Where(x => x.FoldingRatesName == "AntiScuffMattLaminate").Select(x => x.Price).FirstOrDefault();
                        var AntiScuffMattLaminateValue = ((OpenedHeight * OpenedWidth * finalData.MattLam) * AntiScuffMattLaminate * Quantity);
                        if (AntiScuffMattLaminateValue <= 50)
                        {
                            postPress.AntiScuffMattLaminate = 50;
                        }
                        else
                        {
                            postPress.AntiScuffMattLaminate = AntiScuffMattLaminateValue;
                        }
                    }

                    if (finalData.ThroughoutUVVarnish != 0)
                    {
                        var UVVarnish = FoldingRates.Where(x => x.FoldingRatesName == "UVVarnish").Select(x => x.Price).FirstOrDefault();
                        var UVVarnishValue = ((OpenedHeight * OpenedWidth * finalData.ThroughoutUVVarnish) * UVVarnish * Quantity);
                        if (UVVarnishValue <= 50)
                        {
                            postPress.UVVarnish = 50;
                        }
                        else
                        {
                            postPress.UVVarnish = UVVarnishValue;
                        }
                    }
                    if (finalData.OrangePeelEffect != 0)
                    {
                        var OrangePeelEffect = FoldingRates.Where(x => x.FoldingRatesName == "OrangePeelEffect").Select(x => x.Price).FirstOrDefault();
                        var OrangePeelEffectValue = ((OpenedHeight * OpenedWidth * finalData.OrangePeelEffect) * OrangePeelEffect * Quantity);
                        if (OrangePeelEffectValue <= 50)
                        {
                            postPress.OrangePeelEffect = 50;
                        }
                        else
                        {
                            postPress.OrangePeelEffect = OrangePeelEffectValue;
                        }
                    }
                    if (finalData.FoilStamp != 0)
                    {
                        var FoilStamp = FoldingRates.Where(x => x.FoldingRatesName == "FoilStamp").Select(x => x.Price).FirstOrDefault();
                        var FoilStampValue = ((OpenedHeight * OpenedWidth * finalData.FoilStamp) * FoilStamp * Quantity);
                        if (FoilStampValue <= 50)
                        {
                            postPress.FoilStamp = 50;
                        }
                        else
                        {
                            postPress.FoilStamp = FoilStampValue;
                        }
                    }
                    if (finalData.BlindEmboss != 0)
                    {
                        var BlindEmboss = FoldingRates.Where(x => x.FoldingRatesName == "BlindEmboss").Select(x => x.Price).FirstOrDefault();
                        var BlindEmbossValue = ((OpenedHeight * OpenedWidth * finalData.BlindEmboss) * BlindEmboss * Quantity);
                        if (BlindEmbossValue <= 50)
                        {
                            postPress.BlindEmboss = 50;
                        }
                        else
                        {
                            postPress.BlindEmboss = BlindEmbossValue;
                        }
                    }


                    var FoldingMakeReady = FoldingRates.Where(x => x.FoldingRatesName == "FoldingMakeReady").Select(x => x.Price).FirstOrDefault();
                    postPress.FoldingMakeready = FoldingMakeReady;

                    var FoldingProcess = FoldingRates.Where(x => x.FoldingRatesName == "FoldingProcess").Select(x => x.Price).FirstOrDefault();
                   postPress.FoldingProcess = (((Quantity / 1000) * FoldingProcess));
                    postPress.DieCut = 0;
                    if (finalData.DieCutMould.ToLower() == "simple")
                    {
                        postPress.DieCut = 80;
                    }
                    if (finalData.DieCutMould.ToLower() == "difficult")
                    {
                        postPress.DieCut = 300;
                    }
                    if (finalData.DieCutMould.ToLower() == "laser")
                    {
                       postPress.DieCut = 600;
                    }

                    double TotalPrice1 = 0;
                    double TotalPrice2 = 0;

                    if (i == 1)
                    {
                        finalResult.UnitCost1 = Quantity * finalData.UnitCost1.UnitCost1;
                        finalResult.UnitCost2 = Quantity * finalData.UnitCost2.UnitCost1;
                        finalResult.UnitCost3 = Quantity * finalData.UnitCost3.UnitCost1;

                        finalResult.PerThousand1 = ((Quantity / 1000) * finalData.PerThousand1.PerThousand1);
                        finalResult.PerThousand2 = ((Quantity / 1000) * finalData.PerThousand2.PerThousand1);

                        TotalPrice1 = finalData.TotalPrice1.TotalPrice1;
                        TotalPrice2 = finalData.TotalPrice2.TotalPrice1;
                    }
                    if (i == 2)
                    {
                        finalResult.UnitCost1 = Quantity * finalData.UnitCost1.UnitCost2;
                        finalResult.UnitCost2 = Quantity * finalData.UnitCost2.UnitCost2;
                        finalResult.UnitCost3 = Quantity * finalData.UnitCost3.UnitCost2;

                        finalResult.PerThousand1 = ((Quantity / 1000) * finalData.PerThousand1.PerThousand2);
                        finalResult.PerThousand2 = ((Quantity / 1000) * finalData.PerThousand2.PerThousand2);

                        TotalPrice1 = finalData.TotalPrice1.TotalPrice2;
                        TotalPrice2 = finalData.TotalPrice2.TotalPrice2;
                    }
                    if (i == 3)
                    {
                        finalResult.UnitCost1 = Quantity * finalData.UnitCost1.UnitCost3;
                        finalResult.UnitCost2 = Quantity * finalData.UnitCost2.UnitCost3;
                        finalResult.UnitCost3 = Quantity * finalData.UnitCost3.UnitCost3;

                        finalResult.PerThousand1 = ((Quantity / 1000) * finalData.PerThousand1.PerThousand3);
                        finalResult.PerThousand2 = ((Quantity / 1000) * finalData.PerThousand2.PerThousand3);

                        TotalPrice1 = finalData.TotalPrice1.TotalPrice3;
                        TotalPrice2 = finalData.TotalPrice2.TotalPrice3;
                    }


                    //Packing
                    //var Packing = FoldingRates.Where(x => x.FoldingRatesName == "Packing").Select(x => x.Price).FirstOrDefault();

                    var PaperWrap = FoldingRates.Where(x => x.FoldingRatesName == "Packing").Select(x => x.Price).FirstOrDefault();
                    try
                    {
                        if (finalData.PaperWrapped != 0)
                        {
                            finalResult.PackingForFolding.PaperWrap = (Quantity / finalData.PaperWrapped) * PaperWrap;
                        }
                        else
                        {
                            finalResult.PackingForFolding.PaperWrap = 0;
                        }
                    }
                    catch
                    {
                        finalResult.PackingForFolding.PaperWrap = 0;
                    }

                    var Cartonize = FoldingRates.Where(x => x.FoldingRatesName == "Cartonize").Select(x => x.Price).FirstOrDefault();
                    try
                    {
                        if (finalData.Cartonize != 0)
                        {
                            finalResult.PackingForFolding.Cartonize = (Quantity / finalData.Cartonize) * Cartonize;
                        }
                        else
                        {
                            finalResult.PackingForFolding.Cartonize = 0;
                        }
                    }
                    catch
                    {
                        finalResult.PackingForFolding.Cartonize = 0;
                    }

                    var Pallet = FoldingRates.Where(x => x.FoldingRatesName == "Pallet").Select(x => x.Price).FirstOrDefault();
                    try
                    {
                        if (finalData.Pallet != 0)
                        {
                            finalResult.PackingForFolding.Pallet = ((Quantity / finalData.Cartonize) / finalData.Pallet) * Pallet;
                        }
                        else
                        {
                            finalResult.PackingForFolding.Pallet = 0;
                        }
                    }
                    catch
                    {
                        finalResult.PackingForFolding.Pallet = 0;
                    }

                    var ShrinkWrap = FoldingRates.Where(x => x.FoldingRatesName == "ShrinkWrap").Select(x => x.Price).FirstOrDefault();
                    try
                    {
                        if (finalData.ShrinkWrap != 0)
                        {
                            finalResult.PackingForFolding.ShrinkWrap = (Quantity / finalData.ShrinkWrap) * ShrinkWrap;
                        }
                        else
                        {
                            finalResult.PackingForFolding.ShrinkWrap = 0;
                        }
                    }
                    catch
                    {
                        finalResult.PackingForFolding.ShrinkWrap = 0;
                    }

                    //Delivery

                    var Courier = FoldingRates.Where(x => x.FoldingRatesName == "Courier").Select(x => x.Price).FirstOrDefault();
                    finalResult.DeliveryForFolding.Courier = Courier * finalData.Courier;

                    var Delivery = FoldingRates.Where(x => x.FoldingRatesName == "Delivery").Select(x => x.Price).FirstOrDefault();
                    finalResult.DeliveryForFolding.Location = Delivery * finalData.DeliveryLocations;


                    finalResult.Price = finalResult.PaperRate + prepress.Plotter + prepress.CTPPlates + press.PrintingMakeready + press.PrintingProcess + press.PrintingPMS + press.PrintingMetalic + press.VarnishMakeready + press.VarnishProcess + press.SpotVarnishMakeready + press.SpotVarnishProcess + postPress.GlossLaminate + postPress.MattLaminate + postPress.VelvetLaminate + postPress.AntiScuffMattLaminate + postPress.UVVarnish + postPress.OrangePeelEffect + postPress.FoilStamp + postPress.BlindEmboss + postPress.FoldingMakeready + postPress.FoldingProcess + postPress.DieCut + finalResult.UnitCost1 + finalResult.UnitCost2 + finalResult.UnitCost3 + finalResult.PerThousand1 + finalResult.PerThousand2 + finalResult.PackingForFolding.PaperWrap + finalResult.PackingForFolding.Cartonize + finalResult.PackingForFolding.Pallet + finalResult.PackingForFolding.ShrinkWrap + finalResult.DeliveryForFolding.Courier + finalResult.DeliveryForFolding.Location;

                    var MarkupPercentage = FoldingRates.Where(x => x.FoldingRatesName == "MarkupPercentage").Select(x => x.Price).FirstOrDefault();
                    finalResult.Markup = ((finalResult.Price * MarkupPercentage) / 100);
                    finalResult.TotalPrice = finalResult.Price + finalResult.Markup;
                    finalResult.QutotedPrice = finalResult.TotalPrice;
                    finalResult.UnitPrice = finalResult.QutotedPrice / Quantity;

                    quantity.NoOfQuantity = Quantity;
                    quantity.Price = finalResult.TotalPrice;
                    finalResult.prepress.Add(prepress);
                    finalResult.press.Add(press);
                    finalResult.postPress.Add(postPress);
                    quantities.Add(quantity);
                    i++;
                }



                int a = 1;
                string FilePath ="";
                if (a == 1)
                {




                    string DestinationName = DateTime.Now.ToString("ddMMyyyyhhmmss") + ".xlsx";
                    string FoldingName = SalesCalcConstants.FoldingName;
                    string sourcePath = SalesCalcConstants.SourcePath;
                    string targetPath = SalesCalcConstants.TargetPath;

                    // Use Path class to manipulate file and directory paths.
                    string sourceFile = System.IO.Path.Combine(sourcePath, FoldingName);
                    string destFile = System.IO.Path.Combine(targetPath, DestinationName);

                    // To copy a folder's contents to a new location:
                    // Create a new target folder.
                    // If the directory already exists, this method does not create a new directory.....
                    System.IO.Directory.CreateDirectory(targetPath);

                    // To copy a file to another location and
                    // overwrite the destination file if it already exists.
                    System.IO.File.Copy(sourceFile, destFile, true);
                    FileInfo file = new FileInfo(destFile);

                    //create a new Excel package from the file
                    using (ExcelPackage excelPackage = new ExcelPackage(file))
                    {
                        //create an instance of the the first sheet in the loaded file
                        using (ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets["Sheet1"])
                        {
                            worksheet.Cells[5, 2].Value = DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss");
                            worksheet.Cells[7, 2].Value = finalData.Company;
                            worksheet.Cells[8, 2].Value = finalData.Attn;
                            worksheet.Cells[12, 2].Value = finalData.Title;
                            worksheet.Cells[14, 2].Value = finalData.FoldedSize;
                            worksheet.Cells[15, 2].Value = finalData.OpenedSize;
                            worksheet.Cells[16, 2].Value = finalData.Extent + "pp";
                            worksheet.Cells[17, 2].Value = ColorFront;
                            worksheet.Cells[18, 2].Value = ColorBack;
                            worksheet.Cells[19, 2].Value = finalData.GSM + " gsm " + finalData.PaperType;
                            worksheet.Cells[20, 2].Value = "Print ready PDF files supplied by client";
                            worksheet.Cells[21, 2].Value = finalData.Proofs;
                            worksheet.Cells[22, 2].Value = "Die-cut and fold";
                            worksheet.Cells[26, 2].Value = "Paper wrapped in " + finalData.PaperWrapped;
                            worksheet.Cells[27, 2].Value = finalData.DeliveryLocations + "  location in Singapore";
                            worksheet.Cells[28, 2].Value = finalData.AdvanceCopy + " copies";

                            int n = 1;
                            foreach (var items in quantities)
                            {
                                if (n == 1)
                                {
                                    worksheet.Cells[30, 2].Value = items.NoOfQuantity;
                                    worksheet.Cells[31, 2].Value = items.Price;
                                }
                                if (n == 2)
                                {
                                    worksheet.Cells[30, 3].Value = items.NoOfQuantity;
                                    worksheet.Cells[31, 3].Value = items.Price;
                                }
                                if (n == 3)
                                {
                                    worksheet.Cells[30, 4].Value = items.NoOfQuantity;
                                    worksheet.Cells[31, 4].Value = items.Price;
                                }
                                if (n == 4)
                                {
                                    worksheet.Cells[30, 5].Value = items.NoOfQuantity;
                                    worksheet.Cells[31, 5].Value = items.Price;
                                }
                                n++;
                            }
                            worksheet.Cells[4, 4].Value = "";
                            a++;

                            //save the changes
                            excelPackage.Save();
                            FilePath =  SalesCalcConstants.TargetPathIIS + DestinationName;
                        }
                    }

                }
                if (a == 2)
                {
                    string DestinationName = DateTime.Now.ToString("ddMMyyyyhhmmss") + ".xlsx";
                    string FoldingName = SalesCalcConstants.FoldingName;
                    string sourcePath = SalesCalcConstants.SourcePath;
                    string targetPath = SalesCalcConstants.TargetPath;

                    // Use Path class to manipulate file and directory paths.
                    string sourceFile = System.IO.Path.Combine(sourcePath, FoldingName);
                    string destFile = System.IO.Path.Combine(targetPath, DestinationName);

                    // To copy a folder's contents to a new location:
                    // Create a new target folder.
                    // If the directory already exists, this method does not create a new directory.....
                    System.IO.Directory.CreateDirectory(targetPath);

                    // To copy a file to another location and
                    // overwrite the destination file if it already exists.
                    System.IO.File.Copy(sourceFile, destFile, true);
                    FileInfo file = new FileInfo(destFile);

                    //create a new Excel package from the file
                    using (ExcelPackage excelPackage = new ExcelPackage(file))
                    {
                        //create an instance of the the first sheet in the loaded file
                        using (ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets["Sheet1"])
                        {
                            worksheet.Cells[5, 2].Value = DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss");
                            worksheet.Cells[7, 2].Value = finalData.Company;
                            worksheet.Cells[8, 2].Value = finalData.Attn;
                            worksheet.Cells[12, 2].Value = finalData.Title;
                            worksheet.Cells[14, 2].Value = finalData.FoldedSize;
                            worksheet.Cells[15, 2].Value = finalData.OpenedSize;
                            worksheet.Cells[16, 2].Value = finalData.Extent + "pp";
                            worksheet.Cells[17, 2].Value = ColorFront;
                            worksheet.Cells[18, 2].Value = ColorBack;
                            worksheet.Cells[19, 2].Value = finalData.GSM + " gsm " + finalData.PaperType;
                            worksheet.Cells[20, 2].Value = "Print ready PDF files supplied by client";
                            worksheet.Cells[21, 2].Value = finalData.Proofs;
                            worksheet.Cells[22, 2].Value = "Die-cut and fold";
                            worksheet.Cells[26, 2].Value = "Paper wrapped in " + finalData.PaperWrapped;
                            worksheet.Cells[27, 2].Value = finalData.DeliveryLocations + "  location in Singapore";
                            worksheet.Cells[28, 2].Value = finalData.AdvanceCopy + " copies";

                            int n = 1;
                            foreach (var items in quantities)
                            {
                                if (n == 1)
                                {
                                    worksheet.Cells[30, 2].Value = items.NoOfQuantity;
                                    worksheet.Cells[31, 2].Value = items.Price;
                                }
                                if (n == 2)
                                {
                                    worksheet.Cells[30, 3].Value = items.NoOfQuantity;
                                    worksheet.Cells[31, 3].Value = items.Price;
                                }
                                if (n == 3)
                                {
                                    worksheet.Cells[30, 4].Value = items.NoOfQuantity;
                                    worksheet.Cells[31, 4].Value = items.Price;
                                }
                                if (n == 4)
                                {
                                    worksheet.Cells[30, 5].Value = items.NoOfQuantity;
                                    worksheet.Cells[31, 5].Value = items.Price;
                                }
                                n++;
                            }
                            worksheet.Cells[4, 4].Value = "";
                            a++;

                            //save the changes
                            excelPackage.Save();
                           
                            FilePath = FilePath +","+ SalesCalcConstants.TargetPathIIS + DestinationName;
                        }
                    }

                }
                return FilePath;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }

        }

        public static bool GetFoldedSizeData(string FinalData)
        {
            bool IsSuccessful = false;
            using (MySqlConnection connection = new MySqlConnection(SalesCalcConstants.SalesCalcConnection))
            {
                using (MySqlCommand command = new MySqlCommand(SalesCalcConstants.sp_InsertFinalData, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Connection.Open();
                    command.CommandTimeout = 180000;
                    command.Parameters.AddWithValue("@Final_Data", FinalData);
                    command.ExecuteNonQuery();
                    command.Connection.Close();
                    IsSuccessful = true;
                }
            }
            return IsSuccessful;

        }

        public static List<FoldingWastage> GetWastageDataForFolding()
        {
            List<FoldingWastage> foldingWastages = new List<FoldingWastage>();
            using (MySqlConnection connection = new MySqlConnection(SalesCalcConstants.SalesCalcConnection))
            {
                using (MySqlCommand command = new MySqlCommand(SalesCalcConstants.sp_GetWastageDataForFolding, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Connection.Open();
                    command.CommandTimeout = 180000;
                    MySqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        FoldingWastage foldingWastage = new FoldingWastage();
                        foldingWastage.FoldingWastageId = Convert.ToInt32(reader["FoldingWastageId"]);
                        foldingWastage.FoldingWastageName = Convert.ToString(reader["FoldingWastageName"]);
                        foldingWastage.FoldingWastageCode = Convert.ToString(reader["FoldingWastageCode"]);
                        foldingWastage.FoldingWastageCost = Convert.ToDouble(reader["FoldingWastageCost"]);
                        foldingWastages.Add(foldingWastage);

                    }
                    command.Connection.Close();
                }
            }
            return foldingWastages;
        }

        //public static int CalculateWorkAndTurn()
        //{
        //    return 2;
        //}

        //public static string GetSheetSize()
        //{
        //    return "631X901";
        //}

        public static int CalculatePrintingNoOfUps()
        {
            return 2;
        }

        //public static int GetCutValue()
        //{
        //    return 8;
        //}

        public static List<FoldingRates> GetFoldingRates()
        {
            List<FoldingRates> foldingRates = new List<FoldingRates>();
            using (MySqlConnection connection = new MySqlConnection(SalesCalcConstants.SalesCalcConnection))
            {
                using (MySqlCommand command = new MySqlCommand(SalesCalcConstants.sp_GetFoldingRates, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Connection.Open();
                    command.CommandTimeout = 180000;
                    MySqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        FoldingRates foldingRate = new FoldingRates();
                        foldingRate.FoldingRatesId = Convert.ToInt32(reader["FoldingRatesId"]);
                        foldingRate.FoldingRatesName = Convert.ToString(reader["FoldingRatesName"]);
                        foldingRate.To1000 = Convert.ToDouble(reader["1-1000"]);
                        foldingRate.To10000 = Convert.ToDouble(reader["1001-10000"]);
                        foldingRate.To50000 = Convert.ToDouble(reader["10001-50000"]);
                        foldingRate.Above50000 = Convert.ToDouble(reader["50001-Above"]);
                        foldingRates.Add(foldingRate);

                    }
                    command.Connection.Close();
                }
            }
            return foldingRates;
        }


        public static string GetExcelReportForCaseBook(CaseBookFinalData caseBookFinalData)
        {
            try
            {
                var CaseBookRates = GetCaseBookRates();
                var WastageDataForCaseBook = GetWastageDataForCaseBook();
                CaseBookFinalResult caseBookFinalResult = new CaseBookFinalResult();
                caseBookFinalResult.arlinResult = new ArlinResult();
                caseBookFinalResult.binding = new Binding();
                caseBookFinalResult.caseWrapResult = new CaseWrapResult();
                caseBookFinalResult.endPaperResult = new EndPaperResult();
                caseBookFinalResult.greyChipboardResult = new GreyChipboardResult();
                caseBookFinalResult.jacketResult = new JacketResult();
                caseBookFinalResult.packingAndDelivery = new PackingAndDelivery();
                caseBookFinalResult.textResult = new TextResult();
                int Bulkiness = 0;
                if (string.IsNullOrEmpty(caseBookFinalData.text.PPValue))
                {
                    caseBookFinalData.text.PPValue = "0";
                }
                caseBookFinalData.SpineWidth = Convert.ToString(Math.Ceiling(((((Convert.ToDecimal(caseBookFinalData.text.PPValue) / 2) * Bulkiness) / 1000) + Convert.ToDecimal(0.5))));
                var PrintingWastage = Convert.ToDecimal(WastageDataForCaseBook.Where(x => x.CaseBookWastageCode == "Printing").Select(x => x.CaseBookWastageCost).FirstOrDefault());

                var VarWastage = Convert.ToDecimal(WastageDataForCaseBook.Where(x => x.CaseBookWastageCode == "Varnish").Select(x => x.CaseBookWastageCost).FirstOrDefault());

                var SpotVarnishWastage = Convert.ToDecimal(WastageDataForCaseBook.Where(x => x.CaseBookWastageCode == "SpotVarnish").Select(x => x.CaseBookWastageCost).FirstOrDefault());

                var GlossLaminateWastage = Convert.ToDecimal(WastageDataForCaseBook.Where(x => x.CaseBookWastageCode == "GlossLaminate").Select(x => x.CaseBookWastageCost).FirstOrDefault());

                var MattLaminateWastage = Convert.ToDecimal(WastageDataForCaseBook.Where(x => x.CaseBookWastageCode == "MattLaminate").Select(x => x.CaseBookWastageCost).FirstOrDefault());

                var UVVarnishWas = Convert.ToDecimal(WastageDataForCaseBook.Where(x => x.CaseBookWastageCode == "UVVarnish").Select(x => x.CaseBookWastageCost).FirstOrDefault());

                var SpotUVWastage = Convert.ToDecimal(WastageDataForCaseBook.Where(x => x.CaseBookWastageCode == "SpotUV").Select(x => x.CaseBookWastageCost).FirstOrDefault());

                var FoilStampWastage = Convert.ToDecimal(WastageDataForCaseBook.Where(x => x.CaseBookWastageCode == "FoilStamp").Select(x => x.CaseBookWastageCost).FirstOrDefault());

                var BlindEmbossWastage = Convert.ToDecimal(WastageDataForCaseBook.Where(x => x.CaseBookWastageCode == "BlindEmboss").Select(x => x.CaseBookWastageCost).FirstOrDefault());

                var FoldingWastages = Convert.ToDecimal(WastageDataForCaseBook.Where(x => x.CaseBookWastageCode == "Folding").Select(x => x.CaseBookWastageCost).FirstOrDefault());

                var FoldedBookHeight = Convert.ToDecimal(caseBookFinalData.FoldedSize.Split('X').FirstOrDefault());
                var FoldedBookWidth = Convert.ToDecimal(caseBookFinalData.FoldedSize.Split('X').LastOrDefault());
                var OpenedBookHeight = Convert.ToDecimal(caseBookFinalData.OpenedSize.Split('X').FirstOrDefault());
                var OpenedBookWidth = Convert.ToDecimal(caseBookFinalData.OpenedSize.Split('X').LastOrDefault());
                //var CutValueForCaseWrap = GetCutValueForCaseWrap();

                var UPs = GetUps();
                var Sig = GetSig();
                var SW = GetSW();
                var WT = GetWT();
                //var CaseWrapCut = GetCutValueForCaseWrap();
                //var EndPaperCut = GetCutValueForEndPaper();
                //var JacketCut = GetCutValueForJacket();
                //var TextCut = GetCutValueForText();
                var Text32Sig = GetText32Sig();
                var Text16Sig = GetText16Sig();
                var Text8Sig = GetText8Sig();
                var Text4Sig = GetText4Sig();

                List<Quantity> quantities = new List<Quantity>();
                foreach (var Quantity in caseBookFinalData.QuantityRequired)
                {
                    Quantity quantity = new Quantity();
                    if (Enumerable.Range(1, 1000).Contains(Quantity))
                    {
                        foreach (var items in CaseBookRates)
                        {
                            items.Price = items.To1000;
                        }
                    }
                    else if (Enumerable.Range(1001, 10000).Contains(Quantity))
                    {
                        foreach (var items in CaseBookRates)
                        {
                            items.Price = items.To10000;
                        }
                    }
                    else if (Enumerable.Range(10001, 50000).Contains(Quantity))
                    {
                        foreach (var items in CaseBookRates)
                        {
                            items.Price = items.To50000;
                        }
                    }
                    else
                    {
                        foreach (var items in CaseBookRates)
                        {
                            items.Price = items.Above50000;
                        }
                    }


                    if (caseBookFinalData.IsCaseWrap)
                    {

                        var CaseWrapPrintingColour = CaseBookRates.Where(x => x.CaseBookRatesName == "CaseWrapPrintingColour").Select(x => x.Price).FirstOrDefault();
                        caseBookFinalResult.caseWrapResult.PrintingColour = ((Quantity / UPs / 1000) * (Convert.ToDecimal(caseBookFinalData.caseWrap.ColorFront) + Convert.ToDecimal(caseBookFinalData.caseWrap.ColorBack)) * Sig * CaseWrapPrintingColour);

                        var CaseWrapMakereadyPrintingColour = CaseBookRates.Where(x => x.CaseBookRatesName == "CaseWrapMakereadyPrintingColour").Select(x => x.Price).FirstOrDefault();
                        caseBookFinalResult.caseWrapResult.MakereadyPrintingColour = ((Convert.ToDecimal(caseBookFinalData.caseWrap.ColorFront) + Convert.ToDecimal(caseBookFinalData.caseWrap.ColorBack)) * Sig * CaseWrapMakereadyPrintingColour);

                        if (Quantity <= 2000)
                        {
                            var CTPPlatesColoursSmall = CaseBookRates.Where(x => x.CaseBookRatesName == "CTPPlatesColoursSmall").Select(x => x.Price).FirstOrDefault();
                            caseBookFinalResult.caseWrapResult.CTPPlatesColours = (((Convert.ToDecimal(caseBookFinalData.caseWrap.ColorFront) + Convert.ToDecimal(caseBookFinalData.caseWrap.ColorBack)) * Sig * CTPPlatesColoursSmall) / SW);
                        }
                        else
                        {
                            var CTPPlatesColoursBig = CaseBookRates.Where(x => x.CaseBookRatesName == "CTPPlatesColoursBig").Select(x => x.Price).FirstOrDefault();
                            caseBookFinalResult.caseWrapResult.CTPPlatesColours = (((Convert.ToDecimal(caseBookFinalData.caseWrap.ColorFront) + Convert.ToDecimal(caseBookFinalData.caseWrap.ColorBack)) * Sig * CTPPlatesColoursBig) / SW);
                        }


                        var CaseWrapPlotter = CaseBookRates.Where(x => x.CaseBookRatesName == "CaseWrapPlotter").Select(x => x.Price).FirstOrDefault();
                        caseBookFinalResult.caseWrapResult.Plotter = ((FoldedBookHeight * FoldedBookWidth) * Convert.ToDecimal(caseBookFinalData.caseWrap.PPValue) * CaseWrapPlotter * caseBookFinalData.caseWrap.Proofs); //1= yes; 0=no proofs value

                        var Varnish = CaseBookRates.Where(x => x.CaseBookRatesName == "CaseWrapVarnish").Select(x => x.Price).FirstOrDefault();
                        caseBookFinalResult.caseWrapResult.Varnish = (((Quantity * UPs) / 1000) * caseBookFinalData.caseWrap.Varnish * Varnish * Sig);

                        var PrintingSpotVarnish = CaseBookRates.Where(x => x.CaseBookRatesName == "CaseWrapPrintingSpotVarnish").Select(x => x.Price).FirstOrDefault();
                        caseBookFinalResult.caseWrapResult.PrintingSpotVarnish = (((Quantity * UPs) / 1000) * caseBookFinalData.caseWrap.SpotVarnish * PrintingSpotVarnish * Sig);

                        var MakereadySpotVarnish = CaseBookRates.Where(x => x.CaseBookRatesName == "CaseWrapMakereadySpotVarnish").Select(x => x.Price).FirstOrDefault();
                        caseBookFinalResult.caseWrapResult.MakereadySpotVarnish = (caseBookFinalData.caseWrap.SpotVarnish * Sig * MakereadySpotVarnish);

                        var CTPPlatesSpotVarnish = CaseBookRates.Where(x => x.CaseBookRatesName == "CaseWrapCTPPlatesSpotVarnish").Select(x => x.Price).FirstOrDefault();
                        caseBookFinalResult.caseWrapResult.CTPPlatesSpotVarnish = (caseBookFinalData.caseWrap.SpotVarnish * Sig * CTPPlatesSpotVarnish);

                        var MinimumPaperSizeHeight = OpenedBookHeight + 46;
                        var MinimumPaperSizeWidth = OpenedBookWidth + Convert.ToDecimal(caseBookFinalData.SpineWidth) + 5 + 40 + 20;
                        var Result = CommonCalculations.GetCommonCalculation((Convert.ToString(MinimumPaperSizeHeight) + "X" + Convert.ToString(MinimumPaperSizeWidth)), "");
                        var CaseWrapCut = Result.Cut;

                        if (caseBookFinalData.caseWrap.GlossLam != 0)
                        {
                            var LaminateGloss = CaseBookRates.Where(x => x.CaseBookRatesName == "CaseWrapLaminateGloss").Select(x => x.Price).FirstOrDefault();
                            var LamGloss = (MinimumPaperSizeHeight * MinimumPaperSizeWidth * caseBookFinalData.caseWrap.GlossLam * LaminateGloss * Quantity); //need to clarify doubts
                            if (LamGloss <= 50)
                            {
                                caseBookFinalResult.caseWrapResult.LaminateGloss = 50;
                            }
                            else
                            {
                                caseBookFinalResult.caseWrapResult.LaminateGloss = LamGloss;
                            }
                        }

                        if (caseBookFinalData.caseWrap.MattLam != 0)
                        {
                            var LaminateMatt = CaseBookRates.Where(x => x.CaseBookRatesName == "CaseWrapLaminateMatt").Select(x => x.Price).FirstOrDefault();
                            var LamMatt = (MinimumPaperSizeHeight * MinimumPaperSizeWidth * caseBookFinalData.caseWrap.MattLam * LaminateMatt * Quantity); //need to clarify doubts
                            if (LamMatt <= 50)
                            {
                                caseBookFinalResult.caseWrapResult.LaminateMatt = 50;
                            }
                            else
                            {
                                caseBookFinalResult.caseWrapResult.LaminateMatt = LamMatt;
                            }
                        }

                        if (caseBookFinalData.caseWrap.UVVarnish != 0)
                        {
                            var UVVarnish = CaseBookRates.Where(x => x.CaseBookRatesName == "CaseWrapUVVarnish").Select(x => x.Price).FirstOrDefault();
                            var UVVarn = (MinimumPaperSizeHeight * MinimumPaperSizeWidth * caseBookFinalData.caseWrap.UVVarnish * UVVarnish * Quantity); //need to clarify doubts
                            if (UVVarn <= 50)
                            {
                                caseBookFinalResult.caseWrapResult.UVVarnish = 50;
                            }
                            else
                            {
                                caseBookFinalResult.caseWrapResult.UVVarnish = UVVarn;
                            }
                        }

                        if (!string.IsNullOrEmpty(caseBookFinalData.caseWrap.SpotUVVarnish))
                        {
                            caseBookFinalResult.caseWrapResult.SpotUVVarnish = Convert.ToDecimal(caseBookFinalData.caseWrap.SpotUVVarnish) * Quantity;
                        }
                        if (!string.IsNullOrEmpty(caseBookFinalData.caseWrap.FoilStampProcess))
                        {
                            caseBookFinalResult.caseWrapResult.FoilStampProcess = Convert.ToDecimal(caseBookFinalData.caseWrap.FoilStampProcess) * Quantity;
                        }

                        //caseBookFinalResult.caseWrapResult.FoilStampBlock
                        if (!string.IsNullOrEmpty(caseBookFinalData.caseWrap.BlindEmbossProcess))
                        {
                            caseBookFinalResult.caseWrapResult.BlindEmbossProcess = Convert.ToDecimal(caseBookFinalData.caseWrap.BlindEmbossProcess) * Quantity;
                        }

                        //caseBookFinalResult.caseWrapResult.BlindEmbossBlock
                        if (!string.IsNullOrEmpty(caseBookFinalData.caseWrap.UnitPrice))
                        {
                            caseBookFinalResult.caseWrapResult.UnitPrice = Convert.ToDecimal(caseBookFinalData.caseWrap.UnitPrice) * Quantity;
                        }


                        var Height = caseBookFinalData.caseWrap.PaperSize.Split('X').FirstOrDefault();
                        var Width = caseBookFinalData.caseWrap.PaperSize.Split('X').LastOrDefault();
                        var PriceReam = (((Convert.ToDecimal(caseBookFinalData.caseWrap.GSM) / 1000 * Convert.ToDecimal(Height) / 1000 * Convert.ToDecimal(Width)) / 2) * Convert.ToDecimal(2.2046) * Convert.ToDecimal(caseBookFinalData.caseWrap.PaperCost));
                        var PriceSheet = PriceReam / 500;

                        var PrintingColorPaperWastage = (Quantity / CaseWrapCut);
                        var MakereadyPrintingColourWastage = (((Convert.ToDecimal(caseBookFinalData.caseWrap.ColorFront) + Convert.ToDecimal(caseBookFinalData.caseWrap.ColorBack)) * PrintingWastage / CaseWrapCut));
                        var VarnishWastage = ((caseBookFinalData.caseWrap.Varnish * VarWastage) / CaseWrapCut);
                        var MakereadySpotVarnishWastage = ((caseBookFinalData.caseWrap.SpotVarnish * SpotVarnishWastage) / CaseWrapCut);
                        var LaminateGlossWastage = Math.Ceiling(((caseBookFinalData.caseWrap.GlossLam * GlossLaminateWastage) / CaseWrapCut));
                        var LaminateMattWastage = Math.Ceiling(((caseBookFinalData.caseWrap.MattLam * MattLaminateWastage) / CaseWrapCut));
                        var UVVarnishWastage = Math.Ceiling(((caseBookFinalData.caseWrap.UVVarnish * UVVarnishWas) / CaseWrapCut));
                        var SpotUVVarnishWastage = ((caseBookFinalData.caseWrap.SpotUV * SpotUVWastage) / CaseWrapCut);
                        var FoilStampProcessWastage = Math.Ceiling((FoilStampWastage / CaseWrapCut));
                        var BlindEmbossProcessWastage = Math.Ceiling((BlindEmbossWastage / CaseWrapCut));
                        var PaperTotalWastage = PrintingColorPaperWastage + MakereadyPrintingColourWastage + VarnishWastage + MakereadySpotVarnishWastage + LaminateGlossWastage + LaminateMattWastage + UVVarnishWastage + SpotUVVarnishWastage + FoilStampProcessWastage + BlindEmbossProcessWastage;

                        caseBookFinalResult.caseWrapResult.PaperTotal = (PaperTotalWastage * PriceSheet);

                        //caseBookFinalResult.caseWrapResult.UnitPrice = Convert.ToDecimal(caseBookFinalData.caseWrap.UnitPrice) * Quantity;

                    }

                    if (caseBookFinalData.IsEndPaper)
                    {
                        var EndPaperPrintingColour = CaseBookRates.Where(x => x.CaseBookRatesName == "EndPaperPrintingColour").Select(x => x.Price).FirstOrDefault();
                        caseBookFinalResult.endPaperResult.PrintingColour = ((Quantity / UPs / 1000) * (Convert.ToDecimal(caseBookFinalData.endPaper.ColorFront) + Convert.ToDecimal(caseBookFinalData.endPaper.ColorBack)) * Sig * EndPaperPrintingColour * 2);

                        var EndPaperMakereadyPrintingColour = CaseBookRates.Where(x => x.CaseBookRatesName == "EndPaperMakereadyPrintingColour").Select(x => x.Price).FirstOrDefault();
                        caseBookFinalResult.endPaperResult.MakereadyPrintingColour = ((Convert.ToDecimal(caseBookFinalData.endPaper.ColorFront) + Convert.ToDecimal(caseBookFinalData.endPaper.ColorBack)) * Sig * EndPaperMakereadyPrintingColour);

                        if (Quantity <= 2000)
                        {
                            var CTPPlatesColoursSmall = CaseBookRates.Where(x => x.CaseBookRatesName == "CTPPlatesColoursSmall").Select(x => x.Price).FirstOrDefault();
                            caseBookFinalResult.endPaperResult.CTPPlatesColours = (((Convert.ToDecimal(caseBookFinalData.endPaper.ColorFront) + Convert.ToDecimal(caseBookFinalData.endPaper.ColorBack)) * Sig * CTPPlatesColoursSmall) / SW);
                        }
                        else
                        {
                            var CTPPlatesColoursBig = CaseBookRates.Where(x => x.CaseBookRatesName == "CTPPlatesColoursBig").Select(x => x.Price).FirstOrDefault();
                            caseBookFinalResult.endPaperResult.CTPPlatesColours = (((Convert.ToDecimal(caseBookFinalData.endPaper.ColorFront) + Convert.ToDecimal(caseBookFinalData.endPaper.ColorBack)) * Sig * CTPPlatesColoursBig) / SW);
                        }


                        var EndPaperPlotter = CaseBookRates.Where(x => x.CaseBookRatesName == "EndPaperPlotter").Select(x => x.Price).FirstOrDefault();
                        caseBookFinalResult.endPaperResult.Plotter = (((FoldedBookHeight * FoldedBookWidth) * Convert.ToDecimal(caseBookFinalData.endPaper.PPValue) * EndPaperPlotter) * caseBookFinalData.endPaper.Proofs);//1= yes; 0=no proofs value

                        var Varnish = CaseBookRates.Where(x => x.CaseBookRatesName == "EndPaperVarnish").Select(x => x.Price).FirstOrDefault();
                        caseBookFinalResult.endPaperResult.Varnish = ((Quantity * UPs / 1000) * caseBookFinalData.endPaper.Varnish * Varnish * Sig);

                        var PrintingSpotVarnish = CaseBookRates.Where(x => x.CaseBookRatesName == "EndPaperPrintingSpotVarnish").Select(x => x.Price).FirstOrDefault();
                        caseBookFinalResult.endPaperResult.PrintingSpotVarnish = ((Quantity * UPs / 1000) * caseBookFinalData.endPaper.SpotVarnish * PrintingSpotVarnish * 2 * Sig);

                        var MakereadySpotVarnish = CaseBookRates.Where(x => x.CaseBookRatesName == "EndPaperMakereadySpotVarnish").Select(x => x.Price).FirstOrDefault();
                        caseBookFinalResult.endPaperResult.MakereadySpotVarnish = (caseBookFinalData.endPaper.SpotVarnish * Sig * MakereadySpotVarnish);

                        var CTPPlatesSpotVarnish = CaseBookRates.Where(x => x.CaseBookRatesName == "EndPaperCTPPlatesSpotVarnish").Select(x => x.Price).FirstOrDefault();
                        caseBookFinalResult.endPaperResult.CTPPlatesSpotVarnish = (caseBookFinalData.endPaper.SpotVarnish * Sig * CTPPlatesSpotVarnish);

                        var EndPaperFolding = CaseBookRates.Where(x => x.CaseBookRatesName == "EndPaperFolding").Select(x => x.Price).FirstOrDefault();
                        caseBookFinalResult.endPaperResult.Folding = (((Quantity * 2) / 1000) * EndPaperFolding);

                        var EndPaperMakereadyFolding = CaseBookRates.Where(x => x.CaseBookRatesName == "EndPaperMakereadyFolding").Select(x => x.Price).FirstOrDefault();

                        var Result = CommonCalculations.GetCommonCalculation(caseBookFinalData.OpenedSize, "");
                        var EndPaperCut = Result.Cut;

                        try
                        {
                            caseBookFinalResult.endPaperResult.MakereadyFolding = ((Sig / Sig) * EndPaperMakereadyFolding); //Need to clarify
                        }
                        catch
                        {
                            caseBookFinalResult.endPaperResult.MakereadyFolding = 0;
                        }
                        caseBookFinalResult.endPaperResult.UnitPrice = Convert.ToDecimal(caseBookFinalData.endPaper.UnitPrice) * Quantity;

                        var PrintingColorWastage = (Quantity / EndPaperCut * 2);
                        var MakereadyPrintingColorWastage = (((Convert.ToDecimal(caseBookFinalData.endPaper.ColorFront) + Convert.ToDecimal(caseBookFinalData.endPaper.ColorBack)) * PrintingWastage / EndPaperCut * 2));
                        var VarnishWastage = (caseBookFinalData.endPaper.Varnish * VarWastage / EndPaperCut * 2);
                        var PrintingSpotVarnishWastage = (caseBookFinalData.endPaper.SpotVarnish * SpotVarnishWastage / EndPaperCut * 2);
                        var FoldingWast = Math.Ceiling((FoldingWastages / EndPaperCut * 2));
                        var PaperTotalWastage = PrintingColorWastage + MakereadyPrintingColorWastage + VarnishWastage + PrintingSpotVarnishWastage + FoldingWast;

                        var Height = caseBookFinalData.endPaper.PaperSize.Split('X').FirstOrDefault();
                        var Width = caseBookFinalData.endPaper.PaperSize.Split('X').LastOrDefault();

                        var PriceReam = (((Convert.ToDecimal(caseBookFinalData.endPaper.GSM) / 1000 * Convert.ToDecimal(Height) / 1000 * Convert.ToDecimal(Width)) / 2) * Convert.ToDecimal(2.2046) * Convert.ToDecimal(caseBookFinalData.endPaper.PaperCost));
                        var PriceSheet = PriceReam / 500;

                        caseBookFinalResult.endPaperResult.PaperTotal = (PaperTotalWastage * PriceSheet);

                        if (!string.IsNullOrEmpty(caseBookFinalData.endPaper.UnitPrice))
                        {
                            caseBookFinalResult.endPaperResult.UnitPrice = Convert.ToDecimal(caseBookFinalData.endPaper.UnitPrice) * Quantity;
                        }



                    }

                    //Arlin (Col)

                    if (caseBookFinalData.IsChipBoardCover)
                    {
                        //Grey Chipboard
                        var PriceSheet = 0;
                        var MinimumPaperSizeHeight = OpenedBookHeight + 6;
                        var MinimumPaperSizeWidth = OpenedBookWidth + caseBookFinalData.SpineWidth + 5;

                        var Result = CommonCalculations.GetCommonCalculation((Convert.ToString(MinimumPaperSizeHeight) + "X" + Convert.ToString(MinimumPaperSizeWidth)), "");
                        var ChipBoardCoverCut = Result.Cut;
                        caseBookFinalResult.greyChipboardResult.GreyChipboardTotal = Convert.ToDecimal((Quantity / ChipBoardCoverCut) * 1.02 * PriceSheet);
                    }

                    if (caseBookFinalData.IsJacketFrench || caseBookFinalData.IsJacketNormal)
                    {

                        if (!caseBookFinalData.IsJacketNormal)
                        {
                            caseBookFinalData.jacketNormal.PPValue = "0";
                            caseBookFinalData.jacketNormal.ColorFront = "0";
                            caseBookFinalData.jacketNormal.ColorBack = "0";
                            caseBookFinalData.jacketNormal.GSM = "0";
                            caseBookFinalData.jacketNormal.PaperSize = "0";
                            caseBookFinalData.jacketNormal.PaperCost = "0";
                        }
                        var JacketPrintingColour = CaseBookRates.Where(x => x.CaseBookRatesName == "JacketPrintingColour").Select(x => x.Price).FirstOrDefault();
                        caseBookFinalResult.jacketResult.PrintingColour = ((Quantity / UPs / 1000) * (Convert.ToDecimal(caseBookFinalData.jacketNormal.ColorFront) + Convert.ToDecimal(caseBookFinalData.jacketNormal.ColorBack)) * Sig * JacketPrintingColour);

                        var JacketMakereadyPrintingColour = CaseBookRates.Where(x => x.CaseBookRatesName == "JacketMakereadyPrintingColour").Select(x => x.Price).FirstOrDefault();
                        caseBookFinalResult.jacketResult.MakereadyPrintingColour = ((Convert.ToDecimal(caseBookFinalData.jacketNormal.ColorFront) + Convert.ToDecimal(caseBookFinalData.jacketNormal.ColorBack)) * Sig * JacketMakereadyPrintingColour);


                        if (Quantity <= 2000)
                        {
                            var CTPPlatesColoursSmall = CaseBookRates.Where(x => x.CaseBookRatesName == "CTPPlatesColoursSmall").Select(x => x.Price).FirstOrDefault();
                            caseBookFinalResult.jacketResult.CTPPlatesColours = (((Convert.ToDecimal(caseBookFinalData.jacketNormal.ColorFront) + Convert.ToDecimal(caseBookFinalData.jacketNormal.ColorBack)) * Sig * CTPPlatesColoursSmall) / SW);
                        }
                        else
                        {
                            var CTPPlatesColoursBig = CaseBookRates.Where(x => x.CaseBookRatesName == "CTPPlatesColoursBig").Select(x => x.Price).FirstOrDefault();
                            caseBookFinalResult.jacketResult.CTPPlatesColours = (((Convert.ToDecimal(caseBookFinalData.jacketNormal.ColorFront) + Convert.ToDecimal(caseBookFinalData.jacketNormal.ColorBack)) * Sig * CTPPlatesColoursBig) / SW);
                        }

                        var MinimumPaperSizeHeightNormal = OpenedBookHeight + 6;
                        var MinimumPaperSizeWidthNormal = OpenedBookWidth + Convert.ToDecimal(caseBookFinalData.SpineWidth) + (Convert.ToDecimal(caseBookFinalData.JacketFlap) * 2);

                        var MinimumPaperSizeHeightFrench = OpenedBookHeight + 140 + 6;
                        var MinimumPaperSizeWidthFrench = OpenedBookWidth + Convert.ToDecimal(caseBookFinalData.SpineWidth) + (Convert.ToDecimal(caseBookFinalData.JacketFlap) * 2);

                        var Result = CommonCalculations.GetCommonCalculation((Convert.ToString(MinimumPaperSizeHeightNormal) + "X" + Convert.ToString(MinimumPaperSizeWidthNormal)), "");
                        var JacketCut = Result.Cut;

                        var JacketPlotter = CaseBookRates.Where(x => x.CaseBookRatesName == "JacketPlotter").Select(x => x.Price).FirstOrDefault();
                        caseBookFinalResult.jacketResult.Plotter = (((MinimumPaperSizeHeightNormal * MinimumPaperSizeWidthNormal) * JacketPlotter) * caseBookFinalData.jacketData.Proofs); //1=yes and 0=no proofs value and need to clarify on 211* 411 sizes



                        if (caseBookFinalData.IsJacketNormal)
                        {
                            if (caseBookFinalData.jacketNormal.GlossLam != 0)
                            {
                                var LaminateGloss = CaseBookRates.Where(x => x.CaseBookRatesName == "JacketNormalLaminateGloss").Select(x => x.Price).FirstOrDefault();
                                var LamGloss = (MinimumPaperSizeHeightNormal * MinimumPaperSizeWidthNormal * caseBookFinalData.jacketNormal.GlossLam * LaminateGloss * Quantity); //need to clarify doubts
                                if (LamGloss <= 50)
                                {
                                    caseBookFinalResult.jacketResult.NormalLaminateGloss = 50;
                                }
                                else
                                {
                                    caseBookFinalResult.jacketResult.NormalLaminateGloss = LamGloss;
                                }
                            }

                            if (caseBookFinalData.jacketNormal.MattLam != 0)
                            {
                                var LaminateMatt = CaseBookRates.Where(x => x.CaseBookRatesName == "JacketNormalLaminateMatt").Select(x => x.Price).FirstOrDefault();
                                var LamMatt = (MinimumPaperSizeHeightNormal * MinimumPaperSizeWidthNormal * caseBookFinalData.jacketNormal.MattLam * LaminateMatt * Quantity); //need to clarify doubts
                                if (LamMatt <= 50)
                                {
                                    caseBookFinalResult.jacketResult.NormalLaminateMatt = 50;
                                }
                                else
                                {
                                    caseBookFinalResult.jacketResult.NormalLaminateMatt = LamMatt;
                                }
                            }

                            if (caseBookFinalData.jacketNormal.UVVarnish != 0)
                            {
                                var UVVarnish = CaseBookRates.Where(x => x.CaseBookRatesName == "JacketNormalUVVarnish").Select(x => x.Price).FirstOrDefault();
                                var UVVarn = (MinimumPaperSizeHeightNormal * MinimumPaperSizeWidthNormal * caseBookFinalData.jacketNormal.UVVarnish * UVVarnish * Quantity); //need to clarify doubts
                                if (UVVarn <= 50)
                                {
                                    caseBookFinalResult.jacketResult.NormalUVVarnish = 50;
                                }
                                else
                                {
                                    caseBookFinalResult.jacketResult.NormalUVVarnish = UVVarn;
                                }
                            }
                        }

                        if (caseBookFinalData.IsJacketFrench)
                        {
                            if (caseBookFinalData.jacketFrench.GlossLam != 0)
                            {
                                var LaminateGloss = CaseBookRates.Where(x => x.CaseBookRatesName == "JacketFrenchLaminateGloss").Select(x => x.Price).FirstOrDefault();
                                var LamGloss = (MinimumPaperSizeHeightFrench * MinimumPaperSizeWidthFrench * caseBookFinalData.jacketFrench.GlossLam * LaminateGloss * Quantity); //need to clarify doubts
                                if (LamGloss <= 50)
                                {
                                    caseBookFinalResult.jacketResult.FrenchLaminateGloss = 50;
                                }
                                else
                                {
                                    caseBookFinalResult.jacketResult.FrenchLaminateGloss = LamGloss;
                                }
                            }

                            if (caseBookFinalData.jacketFrench.MattLam != 0)
                            {
                                var LaminateMatt = CaseBookRates.Where(x => x.CaseBookRatesName == "JacketFrenchLaminateMatt").Select(x => x.Price).FirstOrDefault();
                                var LamMatt = (MinimumPaperSizeHeightFrench * MinimumPaperSizeWidthFrench * caseBookFinalData.jacketFrench.MattLam * LaminateMatt * Quantity); //need to clarify doubts
                                if (LamMatt <= 50)
                                {
                                    caseBookFinalResult.jacketResult.FrenchLaminateMatt = 50;
                                }
                                else
                                {
                                    caseBookFinalResult.jacketResult.FrenchLaminateMatt = LamMatt;
                                }
                            }

                            if (caseBookFinalData.jacketFrench.UVVarnish != 0)
                            {
                                var UVVarnish = CaseBookRates.Where(x => x.CaseBookRatesName == "JacketFrenchUVVarnish").Select(x => x.Price).FirstOrDefault();
                                var UVVarn = (MinimumPaperSizeHeightFrench * MinimumPaperSizeWidthFrench * caseBookFinalData.jacketFrench.UVVarnish * UVVarnish * Quantity); //need to clarify doubts
                                if (UVVarn <= 50)
                                {
                                    caseBookFinalResult.jacketResult.FrenchUVVarnish = 50;
                                }
                                else
                                {
                                    caseBookFinalResult.jacketResult.FrenchUVVarnish = UVVarn;
                                }
                            }
                        }


                        if (!string.IsNullOrEmpty(caseBookFinalData.jacketData.SpotUVVarnish))
                        {
                            caseBookFinalResult.jacketResult.SpotUVVarnish = Convert.ToDecimal(caseBookFinalData.jacketData.SpotUVVarnish) * Quantity;
                        }
                        if (!string.IsNullOrEmpty(caseBookFinalData.jacketData.FoilStampProcess))
                        {
                            caseBookFinalResult.jacketResult.FoilStampProcess = Convert.ToDecimal(caseBookFinalData.jacketData.FoilStampProcess) * Quantity;
                        }

                        //caseBookFinalResult.jacketResult.FoilStampBlock
                        if (!string.IsNullOrEmpty(caseBookFinalData.jacketData.BlindEmbossProcess))
                        {
                            caseBookFinalResult.jacketResult.BlindEmbossProcess = Convert.ToDecimal(caseBookFinalData.jacketData.BlindEmbossProcess) * Quantity;
                        }

                        //caseBookFinalResult.jacketResult.BlindEmbossBlock
                        if (!string.IsNullOrEmpty(caseBookFinalData.jacketData.UnitPrice))
                        {
                            caseBookFinalResult.jacketResult.UnitPrice = Convert.ToDecimal(caseBookFinalData.jacketData.UnitPrice) * Quantity;
                        }


                        var JacketFolding = CaseBookRates.Where(x => x.CaseBookRatesName == "JacketFolding").Select(x => x.Price).FirstOrDefault();
                        var JFolding = (Quantity / 1000 * JacketFolding * Sig);
                        if (JFolding >= 20)
                        {
                            caseBookFinalResult.jacketResult.Folding = JFolding;
                        }
                        else
                        {
                            caseBookFinalResult.jacketResult.Folding = 20;
                        }

                        var JacketMakereadyFolding = CaseBookRates.Where(x => x.CaseBookRatesName == "JacketMakereadyFolding").Select(x => x.Price).FirstOrDefault();
                        caseBookFinalResult.jacketResult.MakereadyFolding = (JacketMakereadyFolding * Sig);

                        var Jacketing = CaseBookRates.Where(x => x.CaseBookRatesName == "Jacketing").Select(x => x.Price).FirstOrDefault();
                        caseBookFinalResult.jacketResult.Jacketing = (Jacketing * Quantity * Sig);

                        var JacketPrintingColorWastage = (Quantity / JacketCut);
                        var JacketMakereadyPrintingColorWastage = ((Convert.ToDecimal(caseBookFinalData.jacketNormal.ColorFront) + Convert.ToDecimal(caseBookFinalData.jacketNormal.ColorBack)) * PrintingWastage / JacketCut);
                        var JacketNormalLaminateGlossWastage = (caseBookFinalData.jacketNormal.GlossLam * GlossLaminateWastage / JacketCut);
                        var JacketNormalLaminateMattWastage = (caseBookFinalData.jacketNormal.MattLam * MattLaminateWastage / JacketCut);
                        var JacketNormalUVVarnishWastage = (caseBookFinalData.jacketNormal.UVVarnish * UVVarnishWas / JacketCut);
                        var JacketFrenchLaminateGlossWastage = (caseBookFinalData.jacketFrench.GlossLam * GlossLaminateWastage / JacketCut);
                        var JacketFrenchLaminateMattWastage = (caseBookFinalData.jacketFrench.MattLam * MattLaminateWastage / JacketCut);
                        var JacketFrenchUVVarnishWastage = (caseBookFinalData.jacketFrench.UVVarnish * UVVarnishWas / JacketCut);
                        var JacketSpotUVWastage = (caseBookFinalData.jacketNormal.SpotUV * SpotUVWastage / JacketCut);
                        var JacketFoilStampProcessWastage = (caseBookFinalData.jacketNormal.FoilStamp * FoilStampWastage / JacketCut);
                        var JacketBlindEmbossProcessWastage = (caseBookFinalData.jacketNormal.BlindEmboss * BlindEmbossWastage / JacketCut);
                        var JacketMakereadyFoldingWastage = (FoldingWastages * Sig / JacketCut);
                        var JacketPaperTotalWastage = JacketPrintingColorWastage + JacketMakereadyPrintingColorWastage + JacketNormalLaminateGlossWastage + JacketNormalLaminateMattWastage + JacketNormalUVVarnishWastage + JacketFrenchLaminateGlossWastage + JacketFrenchLaminateMattWastage + JacketFrenchUVVarnishWastage + JacketSpotUVWastage + JacketFoilStampProcessWastage + JacketBlindEmbossProcessWastage + JacketMakereadyFoldingWastage;

                        var Height = caseBookFinalData.jacketNormal.PaperSize.Split('X').FirstOrDefault();
                        var Width = caseBookFinalData.jacketNormal.PaperSize.Split('X').LastOrDefault();


                        var PriceReam = (((Convert.ToDecimal(caseBookFinalData.jacketNormal.GSM) / 1000 * Convert.ToDecimal(Height) / 1000 * Convert.ToDecimal(Width)) / 2) * Convert.ToDecimal(2.2046) * Convert.ToDecimal(caseBookFinalData.jacketNormal.PaperCost));
                        var PriceSheet = PriceReam / 500;

                        caseBookFinalResult.jacketResult.PaperTotal = (JacketPaperTotalWastage * PriceSheet);
                        //caseBookFinalResult.jacketResult.UnitPrice =

                    }

                    if (caseBookFinalData.IsText)
                    {

                        var MinimumPaperSizeHeight = (FoldedBookHeight * 2) + 38;
                        var MinimumPaperSizeWidth = (FoldedBookWidth * 4) + 26;

                        var Result = CommonCalculations.GetCommonCalculation((Convert.ToString(MinimumPaperSizeHeight) + "X" + Convert.ToString(MinimumPaperSizeWidth)), "");
                        var TextCut = Result.Cut;

                        var Text32PrintingColour = CaseBookRates.Where(x => x.CaseBookRatesName == "Text32PrintingColour").Select(x => x.Price).FirstOrDefault();
                        var Text16PrintingColour = CaseBookRates.Where(x => x.CaseBookRatesName == "Text16PrintingColour").Select(x => x.Price).FirstOrDefault();
                        var Text8PrintingColour = CaseBookRates.Where(x => x.CaseBookRatesName == "Text8PrintingColour").Select(x => x.Price).FirstOrDefault();
                        var Text4PrintingColour = CaseBookRates.Where(x => x.CaseBookRatesName == "Text4PrintingColour").Select(x => x.Price).FirstOrDefault();

                        var Text32MakereadyPrintingColour = CaseBookRates.Where(x => x.CaseBookRatesName == "Text32MakereadyPrintingColour").Select(x => x.Price).FirstOrDefault();
                        var Text16MakereadyPrintingColour = CaseBookRates.Where(x => x.CaseBookRatesName == "Text16MakereadyPrintingColour").Select(x => x.Price).FirstOrDefault();
                        var Text8MakereadyPrintingColour = CaseBookRates.Where(x => x.CaseBookRatesName == "Text8MakereadyPrintingColour").Select(x => x.Price).FirstOrDefault();
                        var Text4MakereadyPrintingColour = CaseBookRates.Where(x => x.CaseBookRatesName == "Text4MakereadyPrintingColour").Select(x => x.Price).FirstOrDefault();

                        var Text32Plotter = CaseBookRates.Where(x => x.CaseBookRatesName == "Text32Plotter").Select(x => x.Price).FirstOrDefault();
                        var Text16Plotter = CaseBookRates.Where(x => x.CaseBookRatesName == "Text16Plotter").Select(x => x.Price).FirstOrDefault();
                        var Text8Plotter = CaseBookRates.Where(x => x.CaseBookRatesName == "Text8Plotter").Select(x => x.Price).FirstOrDefault();
                        var Text4Plotter = CaseBookRates.Where(x => x.CaseBookRatesName == "Text4Plotter").Select(x => x.Price).FirstOrDefault();

                        var Text32Varnish = CaseBookRates.Where(x => x.CaseBookRatesName == "Text32Varnish").Select(x => x.Price).FirstOrDefault();
                        var Text16Varnish = CaseBookRates.Where(x => x.CaseBookRatesName == "Text16Varnish").Select(x => x.Price).FirstOrDefault();
                        var Text8Varnish = CaseBookRates.Where(x => x.CaseBookRatesName == "Text8Varnish").Select(x => x.Price).FirstOrDefault();
                        var Text4Varnish = CaseBookRates.Where(x => x.CaseBookRatesName == "Text4Varnish").Select(x => x.Price).FirstOrDefault();

                        var Text32PrintingSpotVarnish = CaseBookRates.Where(x => x.CaseBookRatesName == "Text32PrintingSpotVarnish").Select(x => x.Price).FirstOrDefault();
                        var Text16PrintingSpotVarnish = CaseBookRates.Where(x => x.CaseBookRatesName == "Text16PrintingSpotVarnish").Select(x => x.Price).FirstOrDefault();
                        var Text8PrintingSpotVarnish = CaseBookRates.Where(x => x.CaseBookRatesName == "Text8PrintingSpotVarnish").Select(x => x.Price).FirstOrDefault();
                        var Text4PrintingSpotVarnish = CaseBookRates.Where(x => x.CaseBookRatesName == "Text4PrintingSpotVarnish").Select(x => x.Price).FirstOrDefault();

                        var Text32MakereadySpotVarnish = CaseBookRates.Where(x => x.CaseBookRatesName == "Text32MakereadySpotVarnish").Select(x => x.Price).FirstOrDefault();
                        var Text16MakereadySpotVarnish = CaseBookRates.Where(x => x.CaseBookRatesName == "Text16MakereadySpotVarnish").Select(x => x.Price).FirstOrDefault();
                        var Text8MakereadySpotVarnish = CaseBookRates.Where(x => x.CaseBookRatesName == "Text8MakereadySpotVarnish").Select(x => x.Price).FirstOrDefault();
                        var Text4MakereadySpotVarnish = CaseBookRates.Where(x => x.CaseBookRatesName == "Text4MakereadySpotVarnish").Select(x => x.Price).FirstOrDefault();

                        var Text32CTPPlatesSpotVarnish = CaseBookRates.Where(x => x.CaseBookRatesName == "Text32CTPPlatesSpotVarnish").Select(x => x.Price).FirstOrDefault();// need to clarify on ctpplates spot varnish because ctp plates spot varnish is different in text but same in remaining items
                        var Text16CTPPlatesSpotVarnish = CaseBookRates.Where(x => x.CaseBookRatesName == "Text16CTPPlatesSpotVarnish").Select(x => x.Price).FirstOrDefault();
                        var Text8CTPPlatesSpotVarnish = CaseBookRates.Where(x => x.CaseBookRatesName == "Text8CTPPlatesSpotVarnish").Select(x => x.Price).FirstOrDefault();
                        var Text4CTPPlatesSpotVarnish = CaseBookRates.Where(x => x.CaseBookRatesName == "Text4CTPPlatesSpotVarnish").Select(x => x.Price).FirstOrDefault();

                        var Text32Folding = CaseBookRates.Where(x => x.CaseBookRatesName == "Text32Folding").Select(x => x.Price).FirstOrDefault();
                        var Text16Folding = CaseBookRates.Where(x => x.CaseBookRatesName == "Text16Folding").Select(x => x.Price).FirstOrDefault();
                        var Text8Folding = CaseBookRates.Where(x => x.CaseBookRatesName == "Text8Folding").Select(x => x.Price).FirstOrDefault();
                        var Text4Folding = CaseBookRates.Where(x => x.CaseBookRatesName == "Text4Folding").Select(x => x.Price).FirstOrDefault();

                        var Text32MakereadyFolding = CaseBookRates.Where(x => x.CaseBookRatesName == "Text32MakereadyFolding").Select(x => x.Price).FirstOrDefault();
                        var Text16MakereadyFolding = CaseBookRates.Where(x => x.CaseBookRatesName == "Text16MakereadyFolding").Select(x => x.Price).FirstOrDefault();
                        var Text8MakereadyFolding = CaseBookRates.Where(x => x.CaseBookRatesName == "Text8MakereadyFolding").Select(x => x.Price).FirstOrDefault();
                        var Text4MakereadyFolding = CaseBookRates.Where(x => x.CaseBookRatesName == "Text4MakereadyFolding").Select(x => x.Price).FirstOrDefault();

                        decimal FXProofsValue = 0;
                        if ((Convert.ToDecimal(caseBookFinalData.jacketNormal.ColorFront) + Convert.ToDecimal(caseBookFinalData.jacketNormal.ColorBack)) <= 1)
                        {
                            FXProofsValue = Convert.ToDecimal(0.50);
                        }
                        else
                        {
                            FXProofsValue = 1;
                        }


                        if (Result.PPValue == 32)
                        {
                            caseBookFinalResult.textResult.PrintingColour = ((Quantity / 1000) * Text32PrintingColour * (Convert.ToDecimal(caseBookFinalData.text.ColorFront) + Convert.ToDecimal(caseBookFinalData.text.ColorBack)) * Text32Sig);

                            caseBookFinalResult.textResult.MakereadyPrintingColour = ((Convert.ToDecimal(caseBookFinalData.text.ColorFront) + Convert.ToDecimal(caseBookFinalData.text.ColorBack)) * Text32Sig * Text32MakereadyPrintingColour);

                            //var TextCTPPlatesColor = CaseBookRates.Where(x => x.CaseBookRatesName == "TextCTPPlatesColor").Select(x => x.Price).FirstOrDefault(); //need to clarify on CTP plate colors

                            if (Quantity <= 2000)
                            {
                                var CTPPlatesColoursSmall = CaseBookRates.Where(x => x.CaseBookRatesName == "CTPPlatesColoursSmall").Select(x => x.Price).FirstOrDefault();
                                caseBookFinalResult.textResult.CTPPlatesColours = (((Convert.ToDecimal(caseBookFinalData.text.ColorFront) + Convert.ToDecimal(caseBookFinalData.text.ColorBack)) * Text32Sig * CTPPlatesColoursSmall) / SW); //need to know how to select sw value 
                            }
                            else
                            {
                                var CTPPlatesColoursBig = CaseBookRates.Where(x => x.CaseBookRatesName == "CTPPlatesColoursBig").Select(x => x.Price).FirstOrDefault();
                                caseBookFinalResult.textResult.CTPPlatesColours = (((Convert.ToDecimal(caseBookFinalData.text.ColorFront) + Convert.ToDecimal(caseBookFinalData.text.ColorBack)) * Text32Sig * CTPPlatesColoursBig) / SW); //need to know how to select sw value 
                            }


                            caseBookFinalResult.textResult.Plotter = (((FoldedBookHeight * FoldedBookWidth) * Result.PPValue * Text32Sig * Text32Plotter) * caseBookFinalData.text.Proofs); //Need to get value for Text(32 or 16 or 8 or 4) and an in last 1=yes and 0=no proofs value;

                            caseBookFinalResult.textResult.Varnish = ((Quantity / UPs / 1000) * Text32Sig * caseBookFinalData.text.Varnish * Text32Varnish);

                            caseBookFinalResult.textResult.PrintingSpotVarnish = ((Quantity / UPs / 1000) * Text32Sig * caseBookFinalData.text.SpotVarnish * Text32PrintingSpotVarnish);

                            caseBookFinalResult.textResult.MakereadySpotVarnish = ((caseBookFinalData.text.SpotVarnish * Text32Sig) * Text32MakereadySpotVarnish);

                            caseBookFinalResult.textResult.CTPPlatesSpotVarnish = ((caseBookFinalData.text.SpotVarnish * Text32Sig) * Text32CTPPlatesSpotVarnish);

                            caseBookFinalResult.textResult.Folding = ((Quantity / 1000) * Text32Sig * Text32Folding); //need to clarify on how to set folding value (32 or 16, 8 , 4)

                            try
                            {
                                caseBookFinalResult.textResult.MakereadyFolding = ((Text32Sig / Text32Sig) * Text32MakereadyFolding); //need clarification on sig/sig calculation
                            }
                            catch
                            {
                                caseBookFinalResult.textResult.MakereadyFolding = 0;
                            }

                            var TextPrintingColorWastage = ((Quantity / TextCut) * Text32Sig);
                            var TextMakereadyPrintingColorWastage = ((Convert.ToDecimal(caseBookFinalData.text.ColorFront) + Convert.ToDecimal(caseBookFinalData.text.ColorBack)) * Text32Sig * PrintingWastage);
                            var TextVarnishWastage = (caseBookFinalData.text.Varnish * VarWastage / TextCut * Text32Sig);
                            var TextPrintingSpotVarnishWastage = ((SpotVarnishWastage * caseBookFinalData.text.SpotVarnish * Text32Sig) / TextCut);
                            var TextFoldingWastage = ((FoldingWastages * Text32Sig / TextCut));
                            var TextPaperTotalWastage = TextPrintingColorWastage + TextMakereadyPrintingColorWastage + TextVarnishWastage + TextPrintingSpotVarnishWastage + TextFoldingWastage;

                            var Height = caseBookFinalData.text.PaperSize.Split('X').FirstOrDefault();
                            var Width = caseBookFinalData.text.PaperSize.Split('X').LastOrDefault();

                            var PriceReam = (((Convert.ToDecimal(caseBookFinalData.text.GSM) / 1000 * Convert.ToDecimal(Height) / 1000 * Convert.ToDecimal(Width)) / 2) * Convert.ToDecimal(2.2046) * Convert.ToDecimal(caseBookFinalData.text.PaperCost));

                            var PriceSheet = PriceReam / 500;

                            caseBookFinalResult.textResult.PaperTotal = (PriceSheet * TextPaperTotalWastage);

                            var FxData = Result.PPValue * Text32Sig; // value 32 is based on text
                            caseBookFinalResult.textResult.FXProofs = FxData * FXProofsValue; // if 4c 1.00,else if 1c 0.50 need clarification on this
                                                                                              //others
                        }
                        else if (Result.PPValue == 16)
                        {
                            caseBookFinalResult.textResult.PrintingColour = ((Quantity / 1000) * Text16PrintingColour * (Convert.ToDecimal(caseBookFinalData.text.ColorFront) + Convert.ToDecimal(caseBookFinalData.text.ColorBack)) * Text16Sig);

                            caseBookFinalResult.textResult.MakereadyPrintingColour = ((Convert.ToDecimal(caseBookFinalData.text.ColorFront) + Convert.ToDecimal(caseBookFinalData.text.ColorBack)) * Text16Sig * Text16MakereadyPrintingColour);

                            //var TextCTPPlatesColor = CaseBookRates.Where(x => x.CaseBookRatesName == "TextCTPPlatesColor").Select(x => x.Price).FirstOrDefault(); //need to clarify on CTP plate colors
                            if (Quantity <= 2000)
                            {
                                var CTPPlatesColoursSmall = CaseBookRates.Where(x => x.CaseBookRatesName == "CTPPlatesColoursSmall").Select(x => x.Price).FirstOrDefault();
                                caseBookFinalResult.textResult.CTPPlatesColours = (((Convert.ToDecimal(caseBookFinalData.text.ColorFront) + Convert.ToDecimal(caseBookFinalData.text.ColorBack)) * Text16Sig * CTPPlatesColoursSmall) / SW); //need to know how to select sw value 
                            }
                            else
                            {
                                var CTPPlatesColoursBig = CaseBookRates.Where(x => x.CaseBookRatesName == "CTPPlatesColoursBig").Select(x => x.Price).FirstOrDefault();
                                caseBookFinalResult.textResult.CTPPlatesColours = (((Convert.ToDecimal(caseBookFinalData.text.ColorFront) + Convert.ToDecimal(caseBookFinalData.text.ColorBack)) * Text16Sig * CTPPlatesColoursBig) / SW); //need to know how to select sw value 
                            }

                            caseBookFinalResult.textResult.Plotter = (((FoldedBookHeight * FoldedBookWidth) * Result.PPValue * Text16Sig * Text16Plotter) * caseBookFinalData.text.Proofs); //Need to get value for Text(32 or 16 or 8 or 4) and an in last 1=yes and 0=no proofs value;

                            caseBookFinalResult.textResult.Varnish = ((Quantity / UPs / 1000) * Text16Sig * caseBookFinalData.text.Varnish * Text16Varnish);

                            caseBookFinalResult.textResult.PrintingSpotVarnish = ((Quantity / UPs / 1000) * Text16Sig * caseBookFinalData.text.SpotVarnish * Text16PrintingSpotVarnish);

                            caseBookFinalResult.textResult.MakereadySpotVarnish = ((caseBookFinalData.text.SpotVarnish * Text16Sig) * Text16MakereadySpotVarnish);

                            caseBookFinalResult.textResult.CTPPlatesSpotVarnish = ((caseBookFinalData.text.SpotVarnish * Text16Sig) * Text16CTPPlatesSpotVarnish);

                            caseBookFinalResult.textResult.Folding = ((Quantity / 1000) * Text16Sig * Text16Folding); //need to clarify on how to set folding value (32 or 16, 8 , 4)

                            try
                            {
                                caseBookFinalResult.textResult.MakereadyFolding = ((Text16Sig / Text16Sig) * Text16MakereadyFolding); //need clarification on sig/sig calculation
                            }
                            catch
                            {
                                caseBookFinalResult.textResult.MakereadyFolding = 0;
                            }

                            var TextPrintingColorWastage = ((Quantity / TextCut) * Text16Sig);
                            var TextMakereadyPrintingColorWastage = ((Convert.ToDecimal(caseBookFinalData.text.ColorFront) + Convert.ToDecimal(caseBookFinalData.text.ColorBack)) * Text16Sig * PrintingWastage);
                            var TextVarnishWastage = (caseBookFinalData.text.Varnish * VarWastage / TextCut * Text16Sig);
                            var TextPrintingSpotVarnishWastage = ((SpotVarnishWastage * caseBookFinalData.text.SpotVarnish * Text16Sig) / TextCut);
                            var TextFoldingWastage = ((FoldingWastages * Text16Sig / TextCut));
                            var TextPaperTotalWastage = TextPrintingColorWastage + TextMakereadyPrintingColorWastage + TextVarnishWastage + TextPrintingSpotVarnishWastage + TextFoldingWastage;

                            var Height = caseBookFinalData.text.PaperSize.Split('X').FirstOrDefault();
                            var Width = caseBookFinalData.text.PaperSize.Split('X').LastOrDefault();

                            var PriceReam = (((Convert.ToDecimal(caseBookFinalData.text.GSM) / 1000 * Convert.ToDecimal(Height) / 1000 * Convert.ToDecimal(Width)) / 2) * Convert.ToDecimal(2.2046) * Convert.ToDecimal(caseBookFinalData.text.PaperCost));

                            var PriceSheet = PriceReam / 500;

                            caseBookFinalResult.textResult.PaperTotal = (PriceSheet * TextPaperTotalWastage);

                            var FxData = Result.PPValue * Text16Sig; // value 32 is based on text
                            caseBookFinalResult.textResult.FXProofs = FxData * FXProofsValue; // if 4c 1.00,else if 1c 0.50 need clarification on this
                                                                                              //others
                        }
                        else if (Result.PPValue == 8)
                        {
                            caseBookFinalResult.textResult.PrintingColour = ((Quantity / 1000) * Text8PrintingColour * (Convert.ToDecimal(caseBookFinalData.text.ColorFront) + Convert.ToDecimal(caseBookFinalData.text.ColorBack)) * Text8Sig);

                            caseBookFinalResult.textResult.MakereadyPrintingColour = ((Convert.ToDecimal(caseBookFinalData.text.ColorFront) + Convert.ToDecimal(caseBookFinalData.text.ColorBack)) * Text8Sig * Text8MakereadyPrintingColour);

                            //var TextCTPPlatesColor = CaseBookRates.Where(x => x.CaseBookRatesName == "TextCTPPlatesColor").Select(x => x.Price).FirstOrDefault(); //need to clarify on CTP plate colors

                            if (Quantity <= 2000)
                            {
                                var CTPPlatesColoursSmall = CaseBookRates.Where(x => x.CaseBookRatesName == "CTPPlatesColoursSmall").Select(x => x.Price).FirstOrDefault();
                                caseBookFinalResult.textResult.CTPPlatesColours = (((Convert.ToDecimal(caseBookFinalData.text.ColorFront) + Convert.ToDecimal(caseBookFinalData.text.ColorBack)) * Text8Sig * CTPPlatesColoursSmall) / SW); //need to know how to select sw value 
                            }
                            else
                            {
                                var CTPPlatesColoursBig = CaseBookRates.Where(x => x.CaseBookRatesName == "CTPPlatesColoursBig").Select(x => x.Price).FirstOrDefault();
                                caseBookFinalResult.textResult.CTPPlatesColours = (((Convert.ToDecimal(caseBookFinalData.text.ColorFront) + Convert.ToDecimal(caseBookFinalData.text.ColorBack)) * Text8Sig * CTPPlatesColoursBig) / SW); //need to know how to select sw value 
                            }

                            caseBookFinalResult.textResult.Plotter = (((FoldedBookHeight * FoldedBookWidth) * Result.PPValue * Text8Sig * Text8Plotter) * caseBookFinalData.text.Proofs); //Need to get value for Text(32 or 16 or 8 or 4) and an in last 1=yes and 0=no proofs value;

                            caseBookFinalResult.textResult.Varnish = ((Quantity / UPs / 1000) * Text8Sig * caseBookFinalData.text.Varnish * Text8Varnish);

                            caseBookFinalResult.textResult.PrintingSpotVarnish = ((Quantity / UPs / 1000) * Text8Sig * caseBookFinalData.text.SpotVarnish * Text8PrintingSpotVarnish);

                            caseBookFinalResult.textResult.MakereadySpotVarnish = ((caseBookFinalData.text.SpotVarnish * Text8Sig) * Text8MakereadySpotVarnish);

                            caseBookFinalResult.textResult.CTPPlatesSpotVarnish = ((caseBookFinalData.text.SpotVarnish * Text8Sig) * Text8CTPPlatesSpotVarnish);

                            caseBookFinalResult.textResult.Folding = ((Quantity / 1000) * Text8Sig * Text8Folding); //need to clarify on how to set folding value (32 or 16, 8 , 4)

                            try
                            {
                                caseBookFinalResult.textResult.MakereadyFolding = ((Text8Sig / Text8Sig) * Text8MakereadyFolding); //need clarification on sig/sig calculation
                            }
                            catch
                            {
                                caseBookFinalResult.textResult.MakereadyFolding = 0;
                            }

                            var TextPrintingColorWastage = ((Quantity / TextCut) * Text8Sig);
                            var TextMakereadyPrintingColorWastage = ((Convert.ToDecimal(caseBookFinalData.text.ColorFront) + Convert.ToDecimal(caseBookFinalData.text.ColorBack)) * Text8Sig * PrintingWastage);
                            var TextVarnishWastage = (caseBookFinalData.text.Varnish * VarWastage / TextCut * Text8Sig);
                            var TextPrintingSpotVarnishWastage = ((SpotVarnishWastage * caseBookFinalData.text.SpotVarnish * Text8Sig) / TextCut);
                            var TextFoldingWastage = ((FoldingWastages * Text8Sig / TextCut));
                            var TextPaperTotalWastage = TextPrintingColorWastage + TextMakereadyPrintingColorWastage + TextVarnishWastage + TextPrintingSpotVarnishWastage + TextFoldingWastage;

                            var Height = caseBookFinalData.text.PaperSize.Split('X').FirstOrDefault();
                            var Width = caseBookFinalData.text.PaperSize.Split('X').LastOrDefault();

                            var PriceReam = (((Convert.ToDecimal(caseBookFinalData.text.GSM) / 1000 * Convert.ToDecimal(Height) / 1000 * Convert.ToDecimal(Width)) / 2) * Convert.ToDecimal(2.2046) * Convert.ToDecimal(caseBookFinalData.text.PaperCost));

                            var PriceSheet = PriceReam / 500;

                            caseBookFinalResult.textResult.PaperTotal = (PriceSheet * TextPaperTotalWastage);

                            var FxData = Result.PPValue * Text8Sig; // value 32 is based on text
                            caseBookFinalResult.textResult.FXProofs = FxData * FXProofsValue; // if 4c 1.00,else if 1c 0.50 need clarification on this
                                                                                              //others
                        }
                        else if (Result.PPValue == 4)
                        {
                            caseBookFinalResult.textResult.PrintingColour = ((Quantity / 1000) * Text4PrintingColour * (Convert.ToDecimal(caseBookFinalData.text.ColorFront) + Convert.ToDecimal(caseBookFinalData.text.ColorBack)) * Text4Sig);

                            caseBookFinalResult.textResult.MakereadyPrintingColour = ((Convert.ToDecimal(caseBookFinalData.text.ColorFront) + Convert.ToDecimal(caseBookFinalData.text.ColorBack)) * Text4Sig * Text4MakereadyPrintingColour);

                            //var TextCTPPlatesColor = CaseBookRates.Where(x => x.CaseBookRatesName == "TextCTPPlatesColor").Select(x => x.Price).FirstOrDefault(); //need to clarify on CTP plate colors

                            if (Quantity <= 2000)
                            {
                                var CTPPlatesColoursSmall = CaseBookRates.Where(x => x.CaseBookRatesName == "CTPPlatesColoursSmall").Select(x => x.Price).FirstOrDefault();
                                caseBookFinalResult.textResult.CTPPlatesColours = (((Convert.ToDecimal(caseBookFinalData.text.ColorFront) + Convert.ToDecimal(caseBookFinalData.text.ColorBack)) * Text4Sig * CTPPlatesColoursSmall) / SW); //need to know how to select sw value 
                            }
                            else
                            {
                                var CTPPlatesColoursBig = CaseBookRates.Where(x => x.CaseBookRatesName == "CTPPlatesColoursBig").Select(x => x.Price).FirstOrDefault();
                                caseBookFinalResult.textResult.CTPPlatesColours = (((Convert.ToDecimal(caseBookFinalData.text.ColorFront) + Convert.ToDecimal(caseBookFinalData.text.ColorBack)) * Text4Sig * CTPPlatesColoursBig) / SW); //need to know how to select sw value 
                            }

                            caseBookFinalResult.textResult.Plotter = (((FoldedBookHeight * FoldedBookWidth) * Result.PPValue * Text4Sig * Text4Plotter) * caseBookFinalData.text.Proofs); //Need to get value for Text(32 or 16 or 8 or 4) and an in last 1=yes and 0=no proofs value;

                            caseBookFinalResult.textResult.Varnish = ((Quantity / UPs / 1000) * Text4Sig * caseBookFinalData.text.Varnish * Text4Varnish);

                            caseBookFinalResult.textResult.PrintingSpotVarnish = ((Quantity / UPs / 1000) * Text4Sig * caseBookFinalData.text.SpotVarnish * Text4PrintingSpotVarnish);

                            caseBookFinalResult.textResult.MakereadySpotVarnish = ((caseBookFinalData.text.SpotVarnish * Text4Sig) * Text4MakereadySpotVarnish);

                            caseBookFinalResult.textResult.CTPPlatesSpotVarnish = ((caseBookFinalData.text.SpotVarnish * Text4Sig) * Text4CTPPlatesSpotVarnish);

                            caseBookFinalResult.textResult.Folding = ((Quantity / 1000) * Text4Sig * Text4Folding); //need to clarify on how to set folding value (32 or 16, 8 , 4)

                            try
                            {
                                caseBookFinalResult.textResult.MakereadyFolding = ((Text4Sig / Text4Sig) * Text4MakereadyFolding); //need clarification on sig/sig calculation
                            }
                            catch
                            {
                                caseBookFinalResult.textResult.MakereadyFolding = 0;
                            }

                            var TextPrintingColorWastage = ((Quantity / TextCut) * Text4Sig);
                            var TextMakereadyPrintingColorWastage = ((Convert.ToDecimal(caseBookFinalData.text.ColorFront) + Convert.ToDecimal(caseBookFinalData.text.ColorBack)) * Text4Sig * PrintingWastage);
                            var TextVarnishWastage = (caseBookFinalData.text.Varnish * VarWastage / TextCut * Text4Sig);
                            var TextPrintingSpotVarnishWastage = ((SpotVarnishWastage * caseBookFinalData.text.SpotVarnish * Text4Sig) / TextCut);
                            var TextFoldingWastage = ((FoldingWastages * Text4Sig / TextCut));
                            var TextPaperTotalWastage = TextPrintingColorWastage + TextMakereadyPrintingColorWastage + TextVarnishWastage + TextPrintingSpotVarnishWastage + TextFoldingWastage;

                            var Height = caseBookFinalData.text.PaperSize.Split('X').FirstOrDefault();
                            var Width = caseBookFinalData.text.PaperSize.Split('X').LastOrDefault();

                            var PriceReam = (((Convert.ToDecimal(caseBookFinalData.text.GSM) / 1000 * Convert.ToDecimal(Height) / 1000 * Convert.ToDecimal(Width)) / 2) * Convert.ToDecimal(2.2046) * Convert.ToDecimal(caseBookFinalData.text.PaperCost));

                            var PriceSheet = PriceReam / 500;

                            caseBookFinalResult.textResult.PaperTotal = (PriceSheet * TextPaperTotalWastage);

                            var FxData = Result.PPValue * Text4Sig; // value 32 is based on text
                            caseBookFinalResult.textResult.FXProofs = FxData * FXProofsValue; // if 4c 1.00,else if 1c 0.50 need clarification on this
                                                                                              //others
                        }

                    }


                    //Binding
                    var CaseMake = CaseBookRates.Where(x => x.CaseBookRatesName == "CaseMake").Select(x => x.Price).FirstOrDefault();
                    caseBookFinalResult.binding.CaseMake = (CaseMake * Quantity);

                    var EndPapering = CaseBookRates.Where(x => x.CaseBookRatesName == "EndPapering").Select(x => x.Price).FirstOrDefault();
                    caseBookFinalResult.binding.EndPapering = (EndPapering * Quantity);

                    var Collate = CaseBookRates.Where(x => x.CaseBookRatesName == "Collate").Select(x => x.Price).FirstOrDefault();
                    var TextSigTotal = Text32Sig + Text16Sig + Text8Sig + Text4Sig;
                    caseBookFinalResult.binding.Collate = ((Quantity * TextSigTotal) * Collate);

                    var Sewing = CaseBookRates.Where(x => x.CaseBookRatesName == "Sewing").Select(x => x.Price).FirstOrDefault();
                    caseBookFinalResult.binding.Sewing = ((Quantity * TextSigTotal) * Sewing);

                    var HeadTailBand = CaseBookRates.Where(x => x.CaseBookRatesName == "HeadTailBand").Select(x => x.Price).FirstOrDefault();
                    caseBookFinalResult.binding.HeadAndTailBand = (Quantity * HeadTailBand * 1); //need to clarify on 1 =yes , 0 = no;

                    var CaseIn = CaseBookRates.Where(x => x.CaseBookRatesName == "CaseIn").Select(x => x.Price).FirstOrDefault();
                    caseBookFinalResult.binding.CaseIn = (Quantity * CaseIn);

                    if (!string.IsNullOrEmpty(caseBookFinalData.bindingForCaseBook.UnitPrice))
                    {
                        caseBookFinalResult.binding.UnitPrice = Convert.ToDecimal(caseBookFinalData.bindingForCaseBook.UnitPrice) * Quantity;
                    }


                    //PackingAndDelivery
                    var PaperWrapIn = CaseBookRates.Where(x => x.CaseBookRatesName == "PaperWrapIn").Select(x => x.Price).FirstOrDefault();
                    var PaperWrapInWastage = caseBookFinalData.packingAndDeliveryForCaseBook.PaperWrapIn; //need to clarify on value 0
                    int PaperWrapInSig = 0;
                    try
                    {
                        PaperWrapInSig = (Quantity / PaperWrapInWastage);
                    }
                    catch
                    {
                        PaperWrapInSig = 0;
                    }

                    caseBookFinalResult.packingAndDelivery.PaperWrapIn = (PaperWrapInSig * PaperWrapIn);

                    var CartonizeIn = CaseBookRates.Where(x => x.CaseBookRatesName == "CartonizeIn").Select(x => x.Price).FirstOrDefault();
                    var CartonizeInWastage = caseBookFinalData.packingAndDeliveryForCaseBook.CartonizeIn; //need to clarify on value 0
                    int CartonizeInSig = 0;
                    try
                    {
                        CartonizeInSig = (Quantity / CartonizeInWastage);

                    }
                    catch
                    {
                        CartonizeInSig = 0;
                    }

                    caseBookFinalResult.packingAndDelivery.CartonizeIn = (CartonizeInSig * CartonizeIn);

                    var Pallet = CaseBookRates.Where(x => x.CaseBookRatesName == "Pallet").Select(x => x.Price).FirstOrDefault();
                    var PalletWastage = caseBookFinalData.packingAndDeliveryForCaseBook.Pallet; //need to clarify on value 0
                    int PalletSig = 0;
                    try
                    {
                        PalletSig = (CartonizeInSig / PalletWastage);
                    }
                    catch
                    {
                        PalletSig = 0;
                    }
                    caseBookFinalResult.packingAndDelivery.Pallet = (PalletSig * Pallet);

                    var Shrinkwrap = CaseBookRates.Where(x => x.CaseBookRatesName == "Shrinkwrap").Select(x => x.Price).FirstOrDefault();
                    var ShrinkwrapWastage = caseBookFinalData.packingAndDeliveryForCaseBook.ShrinkWrap; //need to clarify on value 0
                    caseBookFinalResult.packingAndDelivery.ShrinkWrap = (Quantity * Shrinkwrap * ShrinkwrapWastage);

                    if (!string.IsNullOrEmpty(caseBookFinalData.packingAndDeliveryForCaseBook.UnitPrice))
                    {
                        caseBookFinalResult.packingAndDelivery.UnitPrice = Convert.ToDecimal(caseBookFinalData.packingAndDeliveryForCaseBook.UnitPrice) * Quantity;
                    }

                    var Location = CaseBookRates.Where(x => x.CaseBookRatesName == "Location").Select(x => x.Price).FirstOrDefault();
                    //var LocationWastage = 1; //need to clarify on value 0
                    caseBookFinalResult.packingAndDelivery.Location = (Location * caseBookFinalData.packingAndDeliveryForCaseBook.Location);

                    var Courier = CaseBookRates.Where(x => x.CaseBookRatesName == "Courier").Select(x => x.Price).FirstOrDefault();
                    var CourierWastage = caseBookFinalData.packingAndDeliveryForCaseBook.Courier; //need to clarify on value 0
                    caseBookFinalResult.packingAndDelivery.Courier = (Courier * CourierWastage);

                    caseBookFinalResult.SubTotal = caseBookFinalResult.caseWrapResult.CaseWrapTotal + caseBookFinalResult.endPaperResult.EndPaperTotal + caseBookFinalResult.arlinResult.ArlinTotal + caseBookFinalResult.greyChipboardResult.GreyChipboardTotal + caseBookFinalResult.jacketResult.JacketTotal + caseBookFinalResult.textResult.TextTotal + caseBookFinalResult.binding.BindingTotal + caseBookFinalResult.packingAndDelivery.PackingDeliveryTotal;

                    var MarkupPercentage = CaseBookRates.Where(x => x.CaseBookRatesName == "MarkupPercentage").Select(x => x.Price).FirstOrDefault();
                    caseBookFinalResult.MarkUp = ((caseBookFinalResult.SubTotal * MarkupPercentage) / 100);
                    caseBookFinalResult.Total = caseBookFinalResult.SubTotal + caseBookFinalResult.MarkUp;
                    caseBookFinalResult.Unit = (caseBookFinalResult.Total / Quantity);
                    caseBookFinalResult.Quoted = caseBookFinalResult.Unit * Quantity;

                    quantity.NoOfQuantity = Quantity;
                    quantity.Price = Convert.ToDouble(caseBookFinalResult.Total);
                    quantities.Add(quantity);

                }


                string DestinationName = DateTime.Now.ToString("ddMMyyyyhhmmss") + ".xlsx";
                string SourceName = SalesCalcConstants.SourceName;
                string sourcePath = SalesCalcConstants.SourcePath;
                string targetPath = SalesCalcConstants.TargetPath;

                // Use Path class to manipulate file and directory paths.
                string sourceFile = System.IO.Path.Combine(sourcePath, SourceName);
                string destFile = System.IO.Path.Combine(targetPath, DestinationName);

                // To copy a folder's contents to a new location:
                // Create a new target folder.
                // If the directory already exists, this method does not create a new directory.....
                System.IO.Directory.CreateDirectory(targetPath);

                // To copy a file to another location and
                // overwrite the destination file if it already exists.
                System.IO.File.Copy(sourceFile, destFile, true);
                FileInfo file = new FileInfo(destFile);

                //create a new Excel package from the file
                using (ExcelPackage excelPackage = new ExcelPackage(file))
                {
                    //create an instance of the the first sheet in the loaded file
                    using (ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets["Sheet1"])
                    {
                        worksheet.Cells[5, 2].Value = DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss");
                        worksheet.Cells[7, 2].Value = "CaseBook Data";
                        worksheet.Cells[8, 2].Value = "";
                        worksheet.Cells[12, 2].Value = "Case Book";
                        worksheet.Cells[14, 2].Value = caseBookFinalData.FoldedSize;
                        worksheet.Cells[15, 2].Value = caseBookFinalData.OpenedSize;
                        string Extent = string.Empty;
                        if (caseBookFinalData.IsCaseWrap)
                        {
                            Extent = "Case Wrap";
                        }
                        if (caseBookFinalData.IsEndPaper)
                        {
                            if (string.IsNullOrEmpty(Extent))
                            {
                                Extent = "End Paper";
                            }
                            else
                            {
                                Extent = Extent + ", End Paper";
                            }

                        }
                        if (caseBookFinalData.IsChipBoardCover)
                        {
                            if (string.IsNullOrEmpty(Extent))
                            {
                                Extent = "Chip Board (Cover)";
                            }
                            else
                            {
                                Extent = Extent + ", Chip Board (Cover)";
                            }

                        }
                        if (caseBookFinalData.IsJacketNormal)
                        {
                            if (string.IsNullOrEmpty(Extent))
                            {
                                Extent = "Jacket Normal";
                            }
                            else
                            {
                                Extent = Extent + ", Jacket Normal";
                            }
                        }
                        if (caseBookFinalData.IsJacketFrench)
                        {
                            if (string.IsNullOrEmpty(Extent))
                            {
                                Extent = "Jacket French";
                            }
                            else
                            {
                                Extent = Extent + ", Jacket French";
                            }
                        }
                        if (caseBookFinalData.IsText)
                        {
                            if (string.IsNullOrEmpty(Extent))
                            {
                                Extent = "Text";
                            }
                            else
                            {
                                Extent = Extent + ", Text";
                            }
                        }
                        if (caseBookFinalData.IsSlipCase)
                        {
                            if (string.IsNullOrEmpty(Extent))
                            {
                                Extent = "Slip Case";
                            }
                            else
                            {
                                Extent = Extent + ", Slip Case";
                            }
                        }
                        if (caseBookFinalData.IsChipBoardSlip)
                        {
                            if (string.IsNullOrEmpty(Extent))
                            {
                                Extent = "Chip Board (Slip)";
                            }
                            else
                            {
                                Extent = Extent + ", Chip Board (Slip)";
                            }
                        }
                        worksheet.Cells[16, 2].Value = Extent;
                        worksheet.Cells[17, 2].Value = "Varnish";
                        worksheet.Cells[19, 2].Value = "";
                        worksheet.Cells[22, 2].Value = "";
                        worksheet.Cells[23, 2].Value = "Case Book";
                        worksheet.Cells[24, 2].Value = "-";

                        int n = 1;
                        foreach (var items in quantities)
                        {
                            if (n == 1)
                            {
                                worksheet.Cells[27, 2].Value = items.NoOfQuantity;
                                worksheet.Cells[28, 2].Value = items.Price;
                            }
                            if (n == 2)
                            {
                                worksheet.Cells[27, 3].Value = items.NoOfQuantity;
                                worksheet.Cells[28, 3].Value = items.Price;
                            }
                            if (n == 3)
                            {
                                worksheet.Cells[27, 4].Value = items.NoOfQuantity;
                                worksheet.Cells[28, 4].Value = items.Price;
                            }
                            n++;
                        }
                        worksheet.Cells[41, 4].Value = "";

                        //save the changes
                        excelPackage.Save();
                    }

                }
                return SalesCalcConstants.TargetPathIIS + DestinationName;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }

        }

        public static List<CaseBookRates> GetCaseBookRates()
        {
            List<CaseBookRates> caseBookRates = new List<CaseBookRates>();
            using (MySqlConnection connection = new MySqlConnection(SalesCalcConstants.SalesCalcConnection))
            {
                using (MySqlCommand command = new MySqlCommand(SalesCalcConstants.sp_GetCaseBookRates, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Connection.Open();
                    command.CommandTimeout = 180000;
                    MySqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        CaseBookRates caseBookRate = new CaseBookRates();
                        caseBookRate.CaseBookRateId = Convert.ToInt32(reader["CaseBookRateId"]);
                        caseBookRate.CaseBookRatesName = Convert.ToString(reader["CaseBookRatesName"]);
                        caseBookRate.To1000 = Convert.ToDecimal(reader["1-1000"]);
                        caseBookRate.To10000 = Convert.ToDecimal(reader["1001-10000"]);
                        caseBookRate.To50000 = Convert.ToDecimal(reader["10001-50000"]);
                        caseBookRate.Above50000 = Convert.ToDecimal(reader["50001-Above"]);
                        caseBookRates.Add(caseBookRate);

                    }
                    command.Connection.Close();
                }
            }
            return caseBookRates;
        }

        public static List<CaseBookWastage> GetWastageDataForCaseBook()
        {
            List<CaseBookWastage> caseBookWastages = new List<CaseBookWastage>();
            using (MySqlConnection connection = new MySqlConnection(SalesCalcConstants.SalesCalcConnection))
            {
                using (MySqlCommand command = new MySqlCommand(SalesCalcConstants.sp_GetWastageDataForCaseBook, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Connection.Open();
                    command.CommandTimeout = 180000;
                    MySqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        CaseBookWastage caseBookWastage = new CaseBookWastage();
                        caseBookWastage.CaseBookWastageId = Convert.ToInt32(reader["CaseBookWastageId"]);
                        caseBookWastage.CaseBookWastageName = Convert.ToString(reader["CaseBookWastageName"]);
                        caseBookWastage.CaseBookWastageCode = Convert.ToString(reader["CaseBookWastageCode"]);
                        caseBookWastage.CaseBookWastageCost = Convert.ToDouble(reader["CaseBookWastageCost"]);
                        caseBookWastages.Add(caseBookWastage);

                    }
                    command.Connection.Close();
                }
            }
            return caseBookWastages;
        }

        //public static int GetCutValueForCaseWrap()
        //{
        //    return 4;
        //}

        //public static int GetCutValueForEndPaper()
        //{
        //    return 8;
        //}

        //public static int GetCutValueForGreyChipboard()
        //{
        //    return 1;
        //}

        //public static int GetCutValueForJacket()
        //{
        //    return 1;
        //}
        //public static int GetCutValueForText()
        //{
        //    return 1;
        //}

        public static int GetUps()
        {
            return 1;
        }
        public static int GetSig()
        {
            return 1;
        }

        public static int GetText32Sig()
        {
            return 1;
        }

        public static int GetText16Sig()
        {
            return 1;
        }

        public static int GetText8Sig()
        {
            return 1;
        }

        public static int GetText4Sig()
        {
            return 1;
        }

        public static int GetSW()
        {
            return 1;
        }

        public static int GetWT()
        {
            return 2;
        }


        public static List<LimpBoundWastage> GetWastageDataForLimpBound()
        {
            List<LimpBoundWastage> limpBoundWastages = new List<LimpBoundWastage>();
            using (MySqlConnection connection = new MySqlConnection(SalesCalcConstants.SalesCalcConnection))
            {
                using (MySqlCommand command = new MySqlCommand(SalesCalcConstants.sp_GetWastageDataForLimpBound, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Connection.Open();
                    command.CommandTimeout = 180000;
                    MySqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        LimpBoundWastage limpBoundWastage = new LimpBoundWastage();
                        limpBoundWastage.LimpBoundWastageId = Convert.ToInt32(reader["LimpBoundWastageId"]);
                        limpBoundWastage.LimpBoundWastageName = Convert.ToString(reader["LimpBoundWastageName"]);
                        limpBoundWastage.LimpBoundWastageCode = Convert.ToString(reader["LimpBoundWastageCode"]);
                        limpBoundWastage.LimpBoundWastageCost = Convert.ToDouble(reader["LimpBoundWastageCost"]);
                        limpBoundWastages.Add(limpBoundWastage);

                    }
                    command.Connection.Close();
                }
            }
            return limpBoundWastages;
        }


        public static List<LimpBoundRates> GetLimpBoundRates()
        {
            List<LimpBoundRates> limpBoundRates = new List<LimpBoundRates>();
            using (MySqlConnection connection = new MySqlConnection(SalesCalcConstants.SalesCalcConnection))
            {
                using (MySqlCommand command = new MySqlCommand(SalesCalcConstants.sp_GetLimpBoundRates, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Connection.Open();
                    command.CommandTimeout = 180000;
                    MySqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        LimpBoundRates limpBoundRate = new LimpBoundRates();
                        limpBoundRate.LimpBoundRatesId = Convert.ToInt32(reader["LimpBoundRatesId"]);
                        limpBoundRate.LimpBoundRatesName = Convert.ToString(reader["LimpBoundRatesName"]);
                        limpBoundRate.To1000 = Convert.ToDecimal(reader["1-1000"]);
                        limpBoundRate.To10000 = Convert.ToDecimal(reader["1001-10000"]);
                        limpBoundRate.To50000 = Convert.ToDecimal(reader["10001-50000"]);
                        limpBoundRate.Above50000 = Convert.ToDecimal(reader["50001-Above"]);
                        limpBoundRates.Add(limpBoundRate);

                    }
                    command.Connection.Close();
                }
            }
            return limpBoundRates;
        }


        public static string GetExcelReportForLimpBound(LimpBoundFinalData limpBoundFinalData)
        {
            try
            {
                var LimpBoundRates = GetLimpBoundRates();
                var WastageDataForLimpBound = GetWastageDataForLimpBound();
                LimpBoundFinalResult limpBoundFinalResult = new LimpBoundFinalResult();
                limpBoundFinalResult.bindingForLimpBound = new BindingForLimpBound();
                limpBoundFinalResult.coverResultForLimpbound = new CoverResultForLimpbound();
                limpBoundFinalResult.textResultForLimpBound = new TextResultForLimpBound();
                limpBoundFinalResult.deliveryForLimpBound = new DeliveryForLimpBound();
                limpBoundFinalResult.packingForLimpBound = new PackingForLimpBound();


                var PrintingWastage = Convert.ToDecimal(WastageDataForLimpBound.Where(x => x.LimpBoundWastageCode == "Printing").Select(x => x.LimpBoundWastageCost).FirstOrDefault());

                var VarnishWastage = Convert.ToDecimal(WastageDataForLimpBound.Where(x => x.LimpBoundWastageCode == "Varnish").Select(x => x.LimpBoundWastageCost).FirstOrDefault());

                var SpotVarnishWastage = Convert.ToDecimal(WastageDataForLimpBound.Where(x => x.LimpBoundWastageCode == "SpotVarnish").Select(x => x.LimpBoundWastageCost).FirstOrDefault());

                var GlossLaminateWastage = Convert.ToDecimal(WastageDataForLimpBound.Where(x => x.LimpBoundWastageCode == "GlossLaminate").Select(x => x.LimpBoundWastageCost).FirstOrDefault());

                var MattLaminateWastage = Convert.ToDecimal(WastageDataForLimpBound.Where(x => x.LimpBoundWastageCode == "MattLaminate").Select(x => x.LimpBoundWastageCost).FirstOrDefault());

                var UVVarnishWastage = Convert.ToDecimal(WastageDataForLimpBound.Where(x => x.LimpBoundWastageCode == "UVVarnish").Select(x => x.LimpBoundWastageCost).FirstOrDefault());

                var SpotUVWastage = Convert.ToDecimal(WastageDataForLimpBound.Where(x => x.LimpBoundWastageCode == "SpotUV").Select(x => x.LimpBoundWastageCost).FirstOrDefault());

                var FoilStampWastage = Convert.ToDecimal(WastageDataForLimpBound.Where(x => x.LimpBoundWastageCode == "FoilStamp").Select(x => x.LimpBoundWastageCost).FirstOrDefault());

                var BlindEmbossWastage = Convert.ToDecimal(WastageDataForLimpBound.Where(x => x.LimpBoundWastageCode == "BlindEmboss").Select(x => x.LimpBoundWastageCost).FirstOrDefault());

                var DieCutWastage = Convert.ToDecimal(WastageDataForLimpBound.Where(x => x.LimpBoundWastageCode == "DieCut").Select(x => x.LimpBoundWastageCost).FirstOrDefault());
                var HandFoldWastage = Convert.ToDecimal(WastageDataForLimpBound.Where(x => x.LimpBoundWastageCode == "HandFold").Select(x => x.LimpBoundWastageCost).FirstOrDefault());

                var FoldingWastage = Convert.ToDecimal(WastageDataForLimpBound.Where(x => x.LimpBoundWastageCode == "Folding").Select(x => x.LimpBoundWastageCost).FirstOrDefault());

                List<Quantity> quantities = new List<Quantity>();
                foreach (var Quantity in limpBoundFinalData.QuantityRequired)
                {
                    Quantity quantity = new Quantity();
                    if (Enumerable.Range(1, 1000).Contains(Quantity))
                    {
                        foreach (var items in LimpBoundRates)
                        {
                            items.Price = items.To1000;
                        }
                    }
                    else if (Enumerable.Range(1001, 10000).Contains(Quantity))
                    {
                        foreach (var items in LimpBoundRates)
                        {
                            items.Price = items.To10000;
                        }
                    }
                    else if (Enumerable.Range(10001, 50000).Contains(Quantity))
                    {
                        foreach (var items in LimpBoundRates)
                        {
                            items.Price = items.To50000;
                        }
                    }
                    else
                    {
                        foreach (var items in LimpBoundRates)
                        {
                            items.Price = items.Above50000;
                        }
                    }

                    var CoverSizeHeight = Convert.ToDecimal(limpBoundFinalData.limpBoundCover.CoverSize.Split('X').FirstOrDefault());
                    var CoverSizeWidth = Convert.ToDecimal(limpBoundFinalData.limpBoundCover.CoverSize.Split('X').LastOrDefault());
                    var CoverSizeFoldedBookHeight = Convert.ToDecimal(limpBoundFinalData.limpBoundCover.FoldedSize.Split('X').FirstOrDefault());
                    var CoverSizeFoldedBookWidth = Convert.ToDecimal(limpBoundFinalData.limpBoundCover.FoldedSize.Split('X').LastOrDefault());
                    var CoverSizeOpenedBookHeight = Convert.ToDecimal(limpBoundFinalData.limpBoundCover.OpenedSize.Split('X').FirstOrDefault());
                    var CoverSizeOpenedBookWidth = Convert.ToDecimal(limpBoundFinalData.limpBoundCover.OpenedSize.Split('X').LastOrDefault());
                    var CoverSizeFoilStampHeight = Convert.ToDecimal(limpBoundFinalData.limpBoundCover.FoilStamp.Split('X').FirstOrDefault());
                    var CoverSizeFoilStampWidth = Convert.ToDecimal(limpBoundFinalData.limpBoundCover.FoilStamp.Split('X').LastOrDefault());
                    var CoverSizeBlindEmbossHeight = Convert.ToDecimal(limpBoundFinalData.limpBoundCover.BlindEmboss.Split('X').FirstOrDefault());
                    var CoverSizeBlindEmbossWidth = Convert.ToDecimal(limpBoundFinalData.limpBoundCover.BlindEmboss.Split('X').LastOrDefault());


                    var TextSizeHeight = Convert.ToDecimal(limpBoundFinalData.limpBoundText.TextSize.Split('X').FirstOrDefault());
                    var TextSizeWidth = Convert.ToDecimal(limpBoundFinalData.limpBoundText.TextSize.Split('X').LastOrDefault());
                    var TextSizeFoldedBookHeight = Convert.ToDecimal(limpBoundFinalData.limpBoundText.FoldedSize.Split('X').FirstOrDefault());
                    var TextSizeFoldedBookWidth = Convert.ToDecimal(limpBoundFinalData.limpBoundText.FoldedSize.Split('X').LastOrDefault());
                    var TextSizeOpenedBookHeight = Convert.ToDecimal(limpBoundFinalData.limpBoundText.OpenedSize.Split('X').FirstOrDefault());
                    var TextSizeOpenedBookWidth = Convert.ToDecimal(limpBoundFinalData.limpBoundText.OpenedSize.Split('X').LastOrDefault());

                    var MinimumPaperSizeHeightForText = (TextSizeFoldedBookHeight * 2) + 38;
                    var MinimumPaperSizeWidthForText = (TextSizeFoldedBookWidth * 4) + 26;
                    var ResultForText = CommonCalculations.GetCommonCalculation((Convert.ToString(MinimumPaperSizeHeightForText) + "X" + Convert.ToString(MinimumPaperSizeWidthForText)), "");


                    var SpineWidth = Convert.ToDecimal((((ResultForText.PPValue / 2) * 0) / 1000) + 0.5); // need to get clarification on value 0 => bulkiness value

                    var MinimumPaperSizeHeightForCover = CoverSizeOpenedBookHeight + 28;
                    var MinimumPaperSizeWidthForCover = CoverSizeOpenedBookWidth + SpineWidth + 10;
                    var ResultForCover = CommonCalculations.GetCommonCalculation((Convert.ToString(MinimumPaperSizeHeightForCover) + "X" + Convert.ToString(MinimumPaperSizeWidthForCover)), "");

                    limpBoundFinalData.limpBoundExtent.Cover = ResultForCover.PPValue;
                    limpBoundFinalData.limpBoundExtent.Text = ResultForText.PPValue;

                    var CutForLimpBoundCover = ResultForCover.Cut;
                    var UpsForLimpBoundCover = GetUpsForLimpBoundCover();
                    var SigForLimpBoundCover = GetSigForLimpBoundCover();

                    var SW = GetSW();
                    var WT = GetWT();

                    var BindingSig = GetBindingSigForLimpBound();
                    var TextCut = ResultForText.Cut;

                    //32
                    //var Text32CutForLimpBoundText = GetText32CutForLimpBoundText();
                    var Text32UpsForLimpBoundText = GetText32UpsForLimpBoundText();
                    var Text32SigForLimpBoundText = GetText32SigForLimpBoundText();

                    //16
                    //var Text16CutForLimpBoundText = GetText16CutForLimpBoundText();
                    var Text16UpsForLimpBoundText = GetText16UpsForLimpBoundText();
                    var Text16SigForLimpBoundText = GetText16SigForLimpBoundText();

                    //8
                    //var Text8CutForLimpBoundText = GetText8CutForLimpBoundText();
                    var Text8UpsForLimpBoundText = GetText8UpsForLimpBoundText();
                    var Text8SigForLimpBoundText = GetText8SigForLimpBoundText();

                    //4
                    //var Text4CutForLimpBoundText = GetText4CutForLimpBoundText();
                    var Text4UpsForLimpBoundText = GetText4UpsForLimpBoundText();
                    var Text4SigForLimpBoundText = GetText4SigForLimpBoundText();

                    //Cover Calculations
                    var CoverPrintingColour = LimpBoundRates.Where(x => x.LimpBoundRatesName == "CoverPrintingColour").Select(x => x.Price).FirstOrDefault();
                    limpBoundFinalResult.coverResultForLimpbound.PrintingColour = ((Quantity / UpsForLimpBoundCover / 1000) * (Convert.ToDecimal(limpBoundFinalData.limpBoundCover.ColorFront) + Convert.ToDecimal(limpBoundFinalData.limpBoundCover.ColorBack)) * CoverPrintingColour);

                    var CoverMakereadyPrintingColour = LimpBoundRates.Where(x => x.LimpBoundRatesName == "CoverMakereadyPrintingColour").Select(x => x.Price).FirstOrDefault();
                    limpBoundFinalResult.coverResultForLimpbound.MakereadyPrintingColour = ((Convert.ToDecimal(limpBoundFinalData.limpBoundCover.ColorFront) + Convert.ToDecimal(limpBoundFinalData.limpBoundCover.ColorBack)) * SigForLimpBoundCover * CoverMakereadyPrintingColour);

                    //var CoverCTPPlatesColours = LimpBoundRates.Where(x => x.LimpBoundRatesName == "CoverCTPPlatesColours").Select(x => x.Price).FirstOrDefault();
                    if (Quantity <= 2000)
                    {
                        var CTPPlatesColoursSmall = LimpBoundRates.Where(x => x.LimpBoundRatesName == "CTPPlatesColoursSmall").Select(x => x.Price).FirstOrDefault();
                        limpBoundFinalResult.coverResultForLimpbound.CTPPlatesColours = ((Convert.ToDecimal(limpBoundFinalData.limpBoundCover.ColorFront) + Convert.ToDecimal(limpBoundFinalData.limpBoundCover.ColorBack)) * SigForLimpBoundCover * CTPPlatesColoursSmall);
                    }
                    else
                    {
                        var CTPPlatesColoursBig = LimpBoundRates.Where(x => x.LimpBoundRatesName == "CTPPlatesColoursBig").Select(x => x.Price).FirstOrDefault();
                        limpBoundFinalResult.coverResultForLimpbound.CTPPlatesColours = ((Convert.ToDecimal(limpBoundFinalData.limpBoundCover.ColorFront) + Convert.ToDecimal(limpBoundFinalData.limpBoundCover.ColorBack)) * SigForLimpBoundCover * CTPPlatesColoursBig);
                    }



                    var CoverPlotter = LimpBoundRates.Where(x => x.LimpBoundRatesName == "CoverPlotter").Select(x => x.Price).FirstOrDefault();
                    limpBoundFinalResult.coverResultForLimpbound.Plotter = ((CoverSizeFoldedBookHeight * CoverSizeFoldedBookWidth) * limpBoundFinalData.limpBoundExtent.Cover * CoverPlotter * limpBoundFinalData.limpBoundCover.Proofs); //1= yes; 0=no need to clarify on proofs value

                    //var CoverVarnish = LimpBoundRates.Where(x => x.LimpBoundRatesName == "CoverVarnish").Select(x => x.Price).FirstOrDefault();
                    //limpBoundFinalResult.coverResultForLimpbound.Varnish = ((Quantity / 1000 * CoverVarnish) / UpsForLimpBoundCover * ((Convert.ToDecimal(limpBoundFinalData.limpBoundCover.VarnishFront) + Convert.ToDecimal(limpBoundFinalData.limpBoundCover.VarnishBack))));

                    var CoverVarnish = LimpBoundRates.Where(x => x.LimpBoundRatesName == "CoverVarnish").Select(x => x.Price).FirstOrDefault();
                    limpBoundFinalResult.coverResultForLimpbound.Varnish = ((Quantity / 1000 * CoverVarnish) / UpsForLimpBoundCover * (Convert.ToDecimal(limpBoundFinalData.limpBoundCover.Varnish)));

                    var CoverPrintingSpotVarnish = LimpBoundRates.Where(x => x.LimpBoundRatesName == "CoverPrintingSpotVarnish").Select(x => x.Price).FirstOrDefault();
                    limpBoundFinalResult.coverResultForLimpbound.PrintingSpotVarnish = ((Quantity / UpsForLimpBoundCover / 1000) * (Convert.ToDecimal(limpBoundFinalData.limpBoundCover.SpotVarnish)) * CoverPrintingSpotVarnish);

                    var CoverMakereadySpotVarnish = LimpBoundRates.Where(x => x.LimpBoundRatesName == "CoverMakereadySpotVarnish").Select(x => x.Price).FirstOrDefault();
                    limpBoundFinalResult.coverResultForLimpbound.MakereadySpotVarnish = ((Convert.ToDecimal(limpBoundFinalData.limpBoundCover.SpotVarnish)) * SigForLimpBoundCover * CoverMakereadySpotVarnish);

                    var CoverCTPPlatesSpotVarnish = LimpBoundRates.Where(x => x.LimpBoundRatesName == "CoverCTPPlatesSpotVarnish").Select(x => x.Price).FirstOrDefault();
                    limpBoundFinalResult.coverResultForLimpbound.CTPPlatesSpotVarnish = ((Convert.ToDecimal(limpBoundFinalData.limpBoundCover.SpotVarnish)) * SigForLimpBoundCover * CoverCTPPlatesSpotVarnish);

                    if (limpBoundFinalData.limpBoundCover.LaminateGloss != 0)
                    {
                        var LaminateGloss = LimpBoundRates.Where(x => x.LimpBoundRatesName == "CoverLaminateGloss").Select(x => x.Price).FirstOrDefault();
                        var LamGloss = ((CoverSizeOpenedBookHeight * CoverSizeOpenedBookWidth * limpBoundFinalData.limpBoundCover.LaminateGloss) * LaminateGloss * Quantity);
                        if (LamGloss <= 50)
                        {
                            limpBoundFinalResult.coverResultForLimpbound.LaminateGloss = 50;
                        }
                        else
                        {
                            limpBoundFinalResult.coverResultForLimpbound.LaminateGloss = LamGloss;
                        }

                    }

                    if (limpBoundFinalData.limpBoundCover.LaminateMatt != 0)
                    {
                        var LaminateMatt = LimpBoundRates.Where(x => x.LimpBoundRatesName == "CoverLaminateMatt").Select(x => x.Price).FirstOrDefault();
                        var LamMatt = ((CoverSizeOpenedBookHeight * CoverSizeOpenedBookWidth * limpBoundFinalData.limpBoundCover.LaminateMatt) * LaminateMatt * Quantity);
                        if (LamMatt <= 50)
                        {
                            limpBoundFinalResult.coverResultForLimpbound.LaminateMatt = 50;
                        }
                        else
                        {
                            limpBoundFinalResult.coverResultForLimpbound.LaminateMatt = LamMatt;
                        }

                    }

                    if (limpBoundFinalData.limpBoundCover.UVVarnish != 0)
                    {
                        var UVVarnish = LimpBoundRates.Where(x => x.LimpBoundRatesName == "CoverUVVarnish").Select(x => x.Price).FirstOrDefault();
                        var UVVar = ((CoverSizeOpenedBookHeight * CoverSizeOpenedBookWidth * limpBoundFinalData.limpBoundCover.UVVarnish) * UVVarnish * Quantity);
                        if (UVVar <= 70)
                        {
                            limpBoundFinalResult.coverResultForLimpbound.UVVarnish = 70;
                        }
                        else
                        {
                            limpBoundFinalResult.coverResultForLimpbound.UVVarnish = UVVar;
                        }

                    }


                    if (!string.IsNullOrEmpty(limpBoundFinalData.limpBoundCover.SpotUVVarnish))
                    {
                        limpBoundFinalResult.coverResultForLimpbound.SpotUVVarnish = Convert.ToDecimal(limpBoundFinalData.limpBoundCover.SpotUVVarnish) * Quantity;
                    }
                    if (!string.IsNullOrEmpty(limpBoundFinalData.limpBoundCover.FoilStampProcess))
                    {
                        limpBoundFinalResult.coverResultForLimpbound.FoilStampProcess = Convert.ToDecimal(limpBoundFinalData.limpBoundCover.FoilStampProcess) * Quantity;
                    }
                    if (!string.IsNullOrEmpty(limpBoundFinalData.limpBoundCover.BlindEmbossProcess))
                    {
                        limpBoundFinalResult.coverResultForLimpbound.BlindEmbossProcess = Convert.ToDecimal(limpBoundFinalData.limpBoundCover.BlindEmbossProcess) * Quantity;
                    }

                    var CoverFoilStampBlock = LimpBoundRates.Where(x => x.LimpBoundRatesName == "CoverFoilStampBlock").Select(x => x.Price).FirstOrDefault();
                    limpBoundFinalResult.coverResultForLimpbound.FoilStampBlock = ((CoverSizeFoilStampHeight * CoverSizeFoilStampWidth) * CoverFoilStampBlock);

                    var CoverBlindEmbossBlock = LimpBoundRates.Where(x => x.LimpBoundRatesName == "CoverBlindEmbossBlock").Select(x => x.Price).FirstOrDefault();
                    limpBoundFinalResult.coverResultForLimpbound.BlindEmbossBlock = ((CoverSizeBlindEmbossHeight * CoverSizeBlindEmbossWidth) * CoverBlindEmbossBlock);



                    var CoverPriceReam = ((CoverSizeHeight / 1000 * CoverSizeWidth / 1000 * Convert.ToDecimal(limpBoundFinalData.limpBoundCover.GSM)) / 2 * Convert.ToDecimal(2.2046) * Convert.ToDecimal(limpBoundFinalData.limpBoundCover.PaperCost));
                    var CoverPriceSheet = CoverPriceReam / 500;

                    var CoverPrintingColorWastage = Quantity / CutForLimpBoundCover;
                    var CoverMakeReadyPrintingColorWastage = ((Convert.ToDecimal(limpBoundFinalData.limpBoundCover.ColorFront) + Convert.ToDecimal(limpBoundFinalData.limpBoundCover.ColorBack)) * PrintingWastage / (CutForLimpBoundCover / UpsForLimpBoundCover));
                    var CoverVarnishWastage = ((Convert.ToDecimal(limpBoundFinalData.limpBoundCover.Varnish)) * VarnishWastage / (CutForLimpBoundCover / UpsForLimpBoundCover));
                    var CoverMakeReadySpotVarnishWastage = ((Convert.ToDecimal(limpBoundFinalData.limpBoundCover.SpotVarnish)) * SpotVarnishWastage / (CutForLimpBoundCover / UpsForLimpBoundCover));
                    var CoverLaminateGlossWastage = ((limpBoundFinalData.limpBoundCover.LaminateGloss * GlossLaminateWastage) / (CutForLimpBoundCover / UpsForLimpBoundCover));
                    var CoverLaminateMattWastage = ((limpBoundFinalData.limpBoundCover.LaminateMatt * MattLaminateWastage) / (CutForLimpBoundCover / UpsForLimpBoundCover));
                    var CoverUVVarnishWastage = ((limpBoundFinalData.limpBoundCover.UVVarnish * UVVarnishWastage) / (CutForLimpBoundCover / UpsForLimpBoundCover));
                    var CoverSpotUVVarnishWastage = ((0 * SpotUVWastage) / (CutForLimpBoundCover / UpsForLimpBoundCover)); // need to clarify on value "0"
                    var CoverFoilStampProcessWastage = ((0 * FoilStampWastage) / (CutForLimpBoundCover / UpsForLimpBoundCover)); // need to clarify on value "0"
                    var CoverBlindEmbossProcessWastage = ((0 * BlindEmbossWastage) / (CutForLimpBoundCover / UpsForLimpBoundCover)); // need to clarify on value "0"
                    var CoverPaperTotalWastage = CoverPrintingColorWastage + CoverMakeReadyPrintingColorWastage + CoverVarnishWastage + CoverMakeReadySpotVarnishWastage + CoverLaminateGlossWastage + CoverLaminateMattWastage + CoverUVVarnishWastage + CoverSpotUVVarnishWastage + CoverFoilStampProcessWastage + CoverBlindEmbossProcessWastage;

                    limpBoundFinalResult.coverResultForLimpbound.PaperTotal = (CoverPaperTotalWastage * CoverPriceSheet);

                    decimal FXProofsValueForCover = 0;
                    if ((Convert.ToDecimal(limpBoundFinalData.limpBoundCover.ColorFront) + Convert.ToDecimal(limpBoundFinalData.limpBoundCover.ColorBack)) <= 1)
                    {
                        FXProofsValueForCover = Convert.ToDecimal(0.50);
                    }
                    else
                    {
                        FXProofsValueForCover = 1;
                    }


                    //var CoverFXProofs = LimpBoundRates.Where(x => x.LimpBoundRatesName == "CoverFXProofs").Select(x => x.Price).FirstOrDefault();
                    limpBoundFinalResult.coverResultForLimpbound.FXProofs = (FXProofsValueForCover * 4); //4c-$1.00;  1c-$0.50 need clarification on this

                    var CoverScoreline = LimpBoundRates.Where(x => x.LimpBoundRatesName == "CoverScoreline").Select(x => x.Price).FirstOrDefault();
                    limpBoundFinalResult.coverResultForLimpbound.ScoreLine = (CoverScoreline * Quantity) * 0; //1= yes; 0=no need to clarify

                    var CoverFoldandPasteflaps = LimpBoundRates.Where(x => x.LimpBoundRatesName == "CoverFoldandPasteflaps").Select(x => x.Price).FirstOrDefault();
                    limpBoundFinalResult.coverResultForLimpbound.FoldAndPasteFlaps = (CoverFoldandPasteflaps * Quantity) * 0; //1= yes; 0=no need to clarify

                    //Text Calculations

                    if (ResultForText.PPValue == 32)
                    {
                        var Text32PrintingColour = LimpBoundRates.Where(x => x.LimpBoundRatesName == "Text32PrintingColour").Select(x => x.Price).FirstOrDefault();
                        limpBoundFinalResult.textResultForLimpBound.PrintingColour = ((Quantity / 1000) * Text32PrintingColour * (Convert.ToDecimal(limpBoundFinalData.limpBoundText.ColorFront) + Convert.ToDecimal(limpBoundFinalData.limpBoundText.ColorBack)) * Text32SigForLimpBoundText);

                        var Text32MakereadyPrintingcolour = LimpBoundRates.Where(x => x.LimpBoundRatesName == "Text32MakereadyPrintingcolour").Select(x => x.Price).FirstOrDefault();
                        limpBoundFinalResult.textResultForLimpBound.MakereadyPrintingColour = ((Convert.ToDecimal(limpBoundFinalData.limpBoundText.ColorFront) + Convert.ToDecimal(limpBoundFinalData.limpBoundText.ColorBack)) * Text32SigForLimpBoundText * Text32MakereadyPrintingcolour);

                        //var Text32CTPPlatesColours = LimpBoundRates.Where(x => x.LimpBoundRatesName == "Text32CTPPlatesColours").Select(x => x.Price).FirstOrDefault();
                        if (Quantity <= 2000)
                        {
                            var CTPPlatesColoursSmall = LimpBoundRates.Where(x => x.LimpBoundRatesName == "CTPPlatesColoursSmall").Select(x => x.Price).FirstOrDefault();
                            limpBoundFinalResult.textResultForLimpBound.CTPPlatesColours = (((Convert.ToDecimal(limpBoundFinalData.limpBoundText.ColorFront) + Convert.ToDecimal(limpBoundFinalData.limpBoundText.ColorBack)) * Text32SigForLimpBoundText * CTPPlatesColoursSmall) / SW);
                        }
                        else
                        {
                            var CTPPlatesColoursBig = LimpBoundRates.Where(x => x.LimpBoundRatesName == "CTPPlatesColoursBig").Select(x => x.Price).FirstOrDefault();
                            limpBoundFinalResult.textResultForLimpBound.CTPPlatesColours = (((Convert.ToDecimal(limpBoundFinalData.limpBoundText.ColorFront) + Convert.ToDecimal(limpBoundFinalData.limpBoundText.ColorBack)) * Text32SigForLimpBoundText * CTPPlatesColoursBig) / SW);
                        }

                        var Text32Plotter = LimpBoundRates.Where(x => x.LimpBoundRatesName == "Text32Plotter").Select(x => x.Price).FirstOrDefault();
                        limpBoundFinalResult.textResultForLimpBound.Plotter = ((TextSizeFoldedBookHeight * TextSizeFoldedBookWidth * limpBoundFinalData.limpBoundExtent.Text * Text32SigForLimpBoundText) * Text32Plotter * limpBoundFinalData.limpBoundText.Proofs); //1=yes, 0=no need to clarify on this proofs value

                        var Text32Varnish = LimpBoundRates.Where(x => x.LimpBoundRatesName == "Text32Varnish").Select(x => x.Price).FirstOrDefault();
                        limpBoundFinalResult.textResultForLimpBound.Varnish = ((Text32SigForLimpBoundText * Quantity) * (Convert.ToDecimal(limpBoundFinalData.limpBoundText.Varnish)) / 1000 * Text32Varnish);

                        var Text32PrintingSpotVarnish = LimpBoundRates.Where(x => x.LimpBoundRatesName == "Text32PrintingSpotVarnish").Select(x => x.Price).FirstOrDefault();
                        limpBoundFinalResult.textResultForLimpBound.PrintingSpotVarnish = ((Quantity * Text32SigForLimpBoundText / 1000) * (Convert.ToDecimal(limpBoundFinalData.limpBoundText.SpotVarnish)) * Text32PrintingSpotVarnish);

                        var Text32MakereadySpotVarnish = LimpBoundRates.Where(x => x.LimpBoundRatesName == "Text32MakereadySpotVarnish").Select(x => x.Price).FirstOrDefault();
                        limpBoundFinalResult.textResultForLimpBound.MakereadySpotVarnish = ((Convert.ToDecimal(limpBoundFinalData.limpBoundText.SpotVarnish)) * Text32SigForLimpBoundText * Text32MakereadySpotVarnish);//need to clarify on spotvarnish front and back value to be taken from cover or text???

                        var Text32CTPPlatesSpotVarnish = LimpBoundRates.Where(x => x.LimpBoundRatesName == "Text32CTPPlatesSpotVarnish").Select(x => x.Price).FirstOrDefault();
                        limpBoundFinalResult.textResultForLimpBound.CTPPlatesSpotVarnish = ((Convert.ToDecimal(limpBoundFinalData.limpBoundText.SpotVarnish)) * Text32SigForLimpBoundText * Text32CTPPlatesSpotVarnish);//need to clarify on spotvarnish front and back value to be taken from cover or text???

                        var Text32Folding = LimpBoundRates.Where(x => x.LimpBoundRatesName == "Text32Folding").Select(x => x.Price).FirstOrDefault();
                        limpBoundFinalResult.textResultForLimpBound.Folding = ((Quantity * Text32SigForLimpBoundText) / 1000 * Text32Folding);

                        var Text32MakereadyFolding = LimpBoundRates.Where(x => x.LimpBoundRatesName == "Text32MakereadyFolding").Select(x => x.Price).FirstOrDefault();
                        try
                        {
                            limpBoundFinalResult.textResultForLimpBound.MakereadyFolding = ((Text32SigForLimpBoundText / Text32SigForLimpBoundText) * Text32MakereadyFolding);
                        }
                        catch
                        {
                            limpBoundFinalResult.textResultForLimpBound.MakereadyFolding = 0;
                        }

                        var Text32PrintingColorWastage = (Quantity / TextCut) * Text32SigForLimpBoundText;
                        var Text32MakeReadyPrintingColorWastage = (Convert.ToDecimal(limpBoundFinalData.limpBoundText.ColorFront) + Convert.ToDecimal(limpBoundFinalData.limpBoundText.ColorBack)) * PrintingWastage / (TextCut / Text32UpsForLimpBoundText) * Text32SigForLimpBoundText;
                        var Text32VarnishWastage = (Convert.ToDecimal(limpBoundFinalData.limpBoundText.Varnish)) * VarnishWastage / (TextCut / Text32UpsForLimpBoundText) * Text32SigForLimpBoundText;
                        var Text32PrintingSpotVarnishWastage = (Convert.ToDecimal(limpBoundFinalData.limpBoundText.SpotVarnish)) * SpotVarnishWastage / (TextCut / Text32UpsForLimpBoundText) * Text32SigForLimpBoundText;
                        var Text32FoldingWastage = (FoldingWastage / TextCut) * Text32SigForLimpBoundText;

                        var Text32PaperTotalWastage = Text32PrintingColorWastage + Text32MakeReadyPrintingColorWastage + Text32VarnishWastage + Text32PrintingSpotVarnishWastage + Text32FoldingWastage;

                        var TextPriceReam = ((TextSizeHeight / 1000 * TextSizeWidth / 1000 * Convert.ToDecimal(limpBoundFinalData.limpBoundText.GSM)) / 2 * Convert.ToDecimal(2.2046) * Convert.ToDecimal(limpBoundFinalData.limpBoundText.PaperCost));
                        var TextPriceSheet = TextPriceReam / 500;

                        limpBoundFinalResult.textResultForLimpBound.PaperTotal = Text32PaperTotalWastage * TextPriceSheet;

                        decimal FXProofsValueForText = 0;
                        if ((Convert.ToDecimal(limpBoundFinalData.limpBoundText.ColorFront) + Convert.ToDecimal(limpBoundFinalData.limpBoundText.ColorBack)) <= 1)
                        {
                            FXProofsValueForText = Convert.ToDecimal(0.50);
                        }
                        else
                        {
                            FXProofsValueForText = 1;
                        }

                        //var Text32FXProofs = LimpBoundRates.Where(x => x.LimpBoundRatesName == "Text32FXProofs").Select(x => x.Price).FirstOrDefault();
                        limpBoundFinalResult.textResultForLimpBound.FXProofs = ((FXProofsValueForText * 0));//4c-$1.00;  1c-$0.50 need clarification on this

                    }
                    else if (ResultForText.PPValue == 16)
                    {
                        var Text16PrintingColour = LimpBoundRates.Where(x => x.LimpBoundRatesName == "Text16PrintingColour").Select(x => x.Price).FirstOrDefault();
                        limpBoundFinalResult.textResultForLimpBound.PrintingColour = ((Quantity / 1000) * Text16PrintingColour * (Convert.ToDecimal(limpBoundFinalData.limpBoundText.ColorFront) + Convert.ToDecimal(limpBoundFinalData.limpBoundText.ColorBack)) * Text16SigForLimpBoundText);

                        var Text16MakereadyPrintingcolour = LimpBoundRates.Where(x => x.LimpBoundRatesName == "Text16MakereadyPrintingcolour").Select(x => x.Price).FirstOrDefault();
                        limpBoundFinalResult.textResultForLimpBound.MakereadyPrintingColour = ((Convert.ToDecimal(limpBoundFinalData.limpBoundText.ColorFront) + Convert.ToDecimal(limpBoundFinalData.limpBoundText.ColorBack)) * Text16SigForLimpBoundText * Text16MakereadyPrintingcolour);

                        //var Text32CTPPlatesColours = LimpBoundRates.Where(x => x.LimpBoundRatesName == "Text32CTPPlatesColours").Select(x => x.Price).FirstOrDefault();
                        if (Quantity <= 2000)
                        {
                            var CTPPlatesColoursSmall = LimpBoundRates.Where(x => x.LimpBoundRatesName == "CTPPlatesColoursSmall").Select(x => x.Price).FirstOrDefault();
                            limpBoundFinalResult.textResultForLimpBound.CTPPlatesColours = (((Convert.ToDecimal(limpBoundFinalData.limpBoundText.ColorFront) + Convert.ToDecimal(limpBoundFinalData.limpBoundText.ColorBack)) * Text16SigForLimpBoundText * CTPPlatesColoursSmall) / SW);
                        }
                        else
                        {
                            var CTPPlatesColoursBig = LimpBoundRates.Where(x => x.LimpBoundRatesName == "CTPPlatesColoursBig").Select(x => x.Price).FirstOrDefault();
                            limpBoundFinalResult.textResultForLimpBound.CTPPlatesColours = (((Convert.ToDecimal(limpBoundFinalData.limpBoundText.ColorFront) + Convert.ToDecimal(limpBoundFinalData.limpBoundText.ColorBack)) * Text16SigForLimpBoundText * CTPPlatesColoursBig) / SW);
                        }

                        var Text16Plotter = LimpBoundRates.Where(x => x.LimpBoundRatesName == "Text16Plotter").Select(x => x.Price).FirstOrDefault();
                        limpBoundFinalResult.textResultForLimpBound.Plotter = ((TextSizeFoldedBookHeight * TextSizeFoldedBookWidth * limpBoundFinalData.limpBoundExtent.Text * Text16SigForLimpBoundText) * Text16Plotter * limpBoundFinalData.limpBoundText.Proofs); //1=yes, 0=no need to clarify on this proofs value

                        var Text16Varnish = LimpBoundRates.Where(x => x.LimpBoundRatesName == "Text16Varnish").Select(x => x.Price).FirstOrDefault();
                        limpBoundFinalResult.textResultForLimpBound.Varnish = ((Text16SigForLimpBoundText * Quantity) * (Convert.ToDecimal(limpBoundFinalData.limpBoundText.Varnish)) / 1000 * Text16Varnish);

                        var Text16PrintingSpotVarnish = LimpBoundRates.Where(x => x.LimpBoundRatesName == "Text16PrintingSpotVarnish").Select(x => x.Price).FirstOrDefault();
                        limpBoundFinalResult.textResultForLimpBound.PrintingSpotVarnish = ((Quantity * Text16SigForLimpBoundText / 1000) * (Convert.ToDecimal(limpBoundFinalData.limpBoundText.SpotVarnish)) * Text16PrintingSpotVarnish);

                        var Text16MakereadySpotVarnish = LimpBoundRates.Where(x => x.LimpBoundRatesName == "Text16MakereadySpotVarnish").Select(x => x.Price).FirstOrDefault();
                        limpBoundFinalResult.textResultForLimpBound.MakereadySpotVarnish = ((Convert.ToDecimal(limpBoundFinalData.limpBoundText.SpotVarnish)) * Text16SigForLimpBoundText * Text16MakereadySpotVarnish);//need to clarify on spotvarnish front and back value to be taken from cover or text???

                        var Text16CTPPlatesSpotVarnish = LimpBoundRates.Where(x => x.LimpBoundRatesName == "Text16CTPPlatesSpotVarnish").Select(x => x.Price).FirstOrDefault();
                        limpBoundFinalResult.textResultForLimpBound.CTPPlatesSpotVarnish = ((Convert.ToDecimal(limpBoundFinalData.limpBoundText.SpotVarnish)) * Text16SigForLimpBoundText * Text16CTPPlatesSpotVarnish);//need to clarify on spotvarnish front and back value to be taken from cover or text???

                        var Text16Folding = LimpBoundRates.Where(x => x.LimpBoundRatesName == "Text16Folding").Select(x => x.Price).FirstOrDefault();
                        limpBoundFinalResult.textResultForLimpBound.Folding = ((Quantity * Text16SigForLimpBoundText) / 1000 * Text16Folding);

                        var Text16MakereadyFolding = LimpBoundRates.Where(x => x.LimpBoundRatesName == "Text16MakereadyFolding").Select(x => x.Price).FirstOrDefault();
                        try
                        {
                            limpBoundFinalResult.textResultForLimpBound.MakereadyFolding = ((Text16SigForLimpBoundText / Text16SigForLimpBoundText) * Text16MakereadyFolding);
                        }
                        catch
                        {
                            limpBoundFinalResult.textResultForLimpBound.MakereadyFolding = 0;
                        }

                        var Text16PrintingColorWastage = (Quantity / TextCut) * Text16SigForLimpBoundText;
                        var Text16MakeReadyPrintingColorWastage = (Convert.ToDecimal(limpBoundFinalData.limpBoundText.ColorFront) + Convert.ToDecimal(limpBoundFinalData.limpBoundText.ColorBack)) * PrintingWastage / (TextCut / Text16UpsForLimpBoundText) * Text16SigForLimpBoundText;
                        var Text16VarnishWastage = (Convert.ToDecimal(limpBoundFinalData.limpBoundText.Varnish)) * VarnishWastage / (TextCut / Text16UpsForLimpBoundText) * Text16SigForLimpBoundText;
                        var Text16PrintingSpotVarnishWastage = (Convert.ToDecimal(limpBoundFinalData.limpBoundText.SpotVarnish)) * SpotVarnishWastage / (TextCut / Text16UpsForLimpBoundText) * Text16SigForLimpBoundText;
                        var Text16FoldingWastage = (FoldingWastage / TextCut) * Text16SigForLimpBoundText;

                        var Text16PaperTotalWastage = Text16PrintingColorWastage + Text16MakeReadyPrintingColorWastage + Text16VarnishWastage + Text16PrintingSpotVarnishWastage + Text16FoldingWastage;

                        var TextPriceReam = ((TextSizeHeight / 1000 * TextSizeWidth / 1000 * Convert.ToDecimal(limpBoundFinalData.limpBoundText.GSM)) / 2 * Convert.ToDecimal(2.2046) * Convert.ToDecimal(limpBoundFinalData.limpBoundText.PaperCost));
                        var TextPriceSheet = TextPriceReam / 500;

                        limpBoundFinalResult.textResultForLimpBound.PaperTotal = Text16PaperTotalWastage * TextPriceSheet;

                        decimal FXProofsValueForText = 0;
                        if ((Convert.ToDecimal(limpBoundFinalData.limpBoundText.ColorFront) + Convert.ToDecimal(limpBoundFinalData.limpBoundText.ColorBack)) <= 1)
                        {
                            FXProofsValueForText = Convert.ToDecimal(0.50);
                        }
                        else
                        {
                            FXProofsValueForText = 1;
                        }

                        //var Text32FXProofs = LimpBoundRates.Where(x => x.LimpBoundRatesName == "Text32FXProofs").Select(x => x.Price).FirstOrDefault();
                        limpBoundFinalResult.textResultForLimpBound.FXProofs = ((FXProofsValueForText * 0));//4c-$1.00;  1c-$0.50 need clarification on this


                    }
                    else if (ResultForText.PPValue == 8)
                    {
                        var Text8PrintingColour = LimpBoundRates.Where(x => x.LimpBoundRatesName == "Text8PrintingColour").Select(x => x.Price).FirstOrDefault();
                        limpBoundFinalResult.textResultForLimpBound.PrintingColour = ((Quantity / 1000) * Text8PrintingColour * (Convert.ToDecimal(limpBoundFinalData.limpBoundText.ColorFront) + Convert.ToDecimal(limpBoundFinalData.limpBoundText.ColorBack)) * Text8SigForLimpBoundText);

                        var Text8MakereadyPrintingcolour = LimpBoundRates.Where(x => x.LimpBoundRatesName == "Text8MakereadyPrintingcolour").Select(x => x.Price).FirstOrDefault();
                        limpBoundFinalResult.textResultForLimpBound.MakereadyPrintingColour = ((Convert.ToDecimal(limpBoundFinalData.limpBoundText.ColorFront) + Convert.ToDecimal(limpBoundFinalData.limpBoundText.ColorBack)) * Text8SigForLimpBoundText * Text8MakereadyPrintingcolour);

                        //var Text32CTPPlatesColours = LimpBoundRates.Where(x => x.LimpBoundRatesName == "Text32CTPPlatesColours").Select(x => x.Price).FirstOrDefault();
                        if (Quantity <= 2000)
                        {
                            var CTPPlatesColoursSmall = LimpBoundRates.Where(x => x.LimpBoundRatesName == "CTPPlatesColoursSmall").Select(x => x.Price).FirstOrDefault();
                            limpBoundFinalResult.textResultForLimpBound.CTPPlatesColours = (((Convert.ToDecimal(limpBoundFinalData.limpBoundText.ColorFront) + Convert.ToDecimal(limpBoundFinalData.limpBoundText.ColorBack)) * Text8SigForLimpBoundText * CTPPlatesColoursSmall) / SW);
                        }
                        else
                        {
                            var CTPPlatesColoursBig = LimpBoundRates.Where(x => x.LimpBoundRatesName == "CTPPlatesColoursBig").Select(x => x.Price).FirstOrDefault();
                            limpBoundFinalResult.textResultForLimpBound.CTPPlatesColours = (((Convert.ToDecimal(limpBoundFinalData.limpBoundText.ColorFront) + Convert.ToDecimal(limpBoundFinalData.limpBoundText.ColorBack)) * Text8SigForLimpBoundText * CTPPlatesColoursBig) / SW);
                        }

                        var Text8Plotter = LimpBoundRates.Where(x => x.LimpBoundRatesName == "Text8Plotter").Select(x => x.Price).FirstOrDefault();
                        limpBoundFinalResult.textResultForLimpBound.Plotter = ((TextSizeFoldedBookHeight * TextSizeFoldedBookWidth * limpBoundFinalData.limpBoundExtent.Text * Text8SigForLimpBoundText) * Text8Plotter * limpBoundFinalData.limpBoundText.Proofs); //1=yes, 0=no need to clarify on this proofs value

                        var Text8Varnish = LimpBoundRates.Where(x => x.LimpBoundRatesName == "Text8Varnish").Select(x => x.Price).FirstOrDefault();
                        limpBoundFinalResult.textResultForLimpBound.Varnish = ((Text8SigForLimpBoundText * Quantity) * (Convert.ToDecimal(limpBoundFinalData.limpBoundText.Varnish)) / 1000 * Text8Varnish);

                        var Text8PrintingSpotVarnish = LimpBoundRates.Where(x => x.LimpBoundRatesName == "Text8PrintingSpotVarnish").Select(x => x.Price).FirstOrDefault();
                        limpBoundFinalResult.textResultForLimpBound.PrintingSpotVarnish = ((Quantity * Text8SigForLimpBoundText / 1000) * (Convert.ToDecimal(limpBoundFinalData.limpBoundText.SpotVarnish)) * Text8PrintingSpotVarnish);

                        var Text8MakereadySpotVarnish = LimpBoundRates.Where(x => x.LimpBoundRatesName == "Text8MakereadySpotVarnish").Select(x => x.Price).FirstOrDefault();
                        limpBoundFinalResult.textResultForLimpBound.MakereadySpotVarnish = ((Convert.ToDecimal(limpBoundFinalData.limpBoundText.SpotVarnish)) * Text8SigForLimpBoundText * Text8MakereadySpotVarnish);//need to clarify on spotvarnish front and back value to be taken from cover or text???

                        var Text8CTPPlatesSpotVarnish = LimpBoundRates.Where(x => x.LimpBoundRatesName == "Text8CTPPlatesSpotVarnish").Select(x => x.Price).FirstOrDefault();
                        limpBoundFinalResult.textResultForLimpBound.CTPPlatesSpotVarnish = ((Convert.ToDecimal(limpBoundFinalData.limpBoundText.SpotVarnish)) * Text8SigForLimpBoundText * Text8CTPPlatesSpotVarnish);//need to clarify on spotvarnish front and back value to be taken from cover or text???

                        var Text8Folding = LimpBoundRates.Where(x => x.LimpBoundRatesName == "Text8Folding").Select(x => x.Price).FirstOrDefault();
                        limpBoundFinalResult.textResultForLimpBound.Folding = ((Quantity * Text8SigForLimpBoundText) / 1000 * Text8Folding);

                        var Text8MakereadyFolding = LimpBoundRates.Where(x => x.LimpBoundRatesName == "Text8MakereadyFolding").Select(x => x.Price).FirstOrDefault();
                        try
                        {
                            limpBoundFinalResult.textResultForLimpBound.MakereadyFolding = ((Text8SigForLimpBoundText / Text8SigForLimpBoundText) * Text8MakereadyFolding);
                        }
                        catch
                        {
                            limpBoundFinalResult.textResultForLimpBound.MakereadyFolding = 0;
                        }

                        var Text8PrintingColorWastage = (Quantity / TextCut) * Text8SigForLimpBoundText;
                        var Text8MakeReadyPrintingColorWastage = (Convert.ToDecimal(limpBoundFinalData.limpBoundText.ColorFront) + Convert.ToDecimal(limpBoundFinalData.limpBoundText.ColorBack)) * PrintingWastage / (TextCut / Text8UpsForLimpBoundText) * Text8SigForLimpBoundText;
                        var Text8VarnishWastage = (Convert.ToDecimal(limpBoundFinalData.limpBoundText.Varnish)) * VarnishWastage / (TextCut / Text8UpsForLimpBoundText) * Text8SigForLimpBoundText;
                        var Text8PrintingSpotVarnishWastage = (Convert.ToDecimal(limpBoundFinalData.limpBoundText.SpotVarnish)) * SpotVarnishWastage / (TextCut / Text8UpsForLimpBoundText) * Text8SigForLimpBoundText;
                        var Text8FoldingWastage = (FoldingWastage / TextCut) * Text8SigForLimpBoundText;

                        var Text8PaperTotalWastage = Text8PrintingColorWastage + Text8MakeReadyPrintingColorWastage + Text8VarnishWastage + Text8PrintingSpotVarnishWastage + Text8FoldingWastage;

                        var TextPriceReam = ((TextSizeHeight / 1000 * TextSizeWidth / 1000 * Convert.ToDecimal(limpBoundFinalData.limpBoundText.GSM)) / 2 * Convert.ToDecimal(2.2046) * Convert.ToDecimal(limpBoundFinalData.limpBoundText.PaperCost));
                        var TextPriceSheet = TextPriceReam / 500;

                        limpBoundFinalResult.textResultForLimpBound.PaperTotal = Text8PaperTotalWastage * TextPriceSheet;

                        decimal FXProofsValueForText = 0;
                        if ((Convert.ToDecimal(limpBoundFinalData.limpBoundText.ColorFront) + Convert.ToDecimal(limpBoundFinalData.limpBoundText.ColorBack)) <= 1)
                        {
                            FXProofsValueForText = Convert.ToDecimal(0.50);
                        }
                        else
                        {
                            FXProofsValueForText = 1;
                        }

                        //var Text32FXProofs = LimpBoundRates.Where(x => x.LimpBoundRatesName == "Text32FXProofs").Select(x => x.Price).FirstOrDefault();
                        limpBoundFinalResult.textResultForLimpBound.FXProofs = ((FXProofsValueForText * 0));//4c-$1.00;  1c-$0.50 need clarification on this


                    }
                    else if (ResultForText.PPValue == 4)
                    {
                        var Text4PrintingColour = LimpBoundRates.Where(x => x.LimpBoundRatesName == "Text4PrintingColour").Select(x => x.Price).FirstOrDefault();
                        limpBoundFinalResult.textResultForLimpBound.PrintingColour = ((Quantity / 1000) * Text4PrintingColour * (Convert.ToDecimal(limpBoundFinalData.limpBoundText.ColorFront) + Convert.ToDecimal(limpBoundFinalData.limpBoundText.ColorBack)) * Text4SigForLimpBoundText);

                        var Text4MakereadyPrintingcolour = LimpBoundRates.Where(x => x.LimpBoundRatesName == "Text4MakereadyPrintingcolour").Select(x => x.Price).FirstOrDefault();
                        limpBoundFinalResult.textResultForLimpBound.MakereadyPrintingColour = ((Convert.ToDecimal(limpBoundFinalData.limpBoundText.ColorFront) + Convert.ToDecimal(limpBoundFinalData.limpBoundText.ColorBack)) * Text4SigForLimpBoundText * Text4MakereadyPrintingcolour);

                        //var Text32CTPPlatesColours = LimpBoundRates.Where(x => x.LimpBoundRatesName == "Text32CTPPlatesColours").Select(x => x.Price).FirstOrDefault();
                        if (Quantity <= 2000)
                        {
                            var CTPPlatesColoursSmall = LimpBoundRates.Where(x => x.LimpBoundRatesName == "CTPPlatesColoursSmall").Select(x => x.Price).FirstOrDefault();
                            limpBoundFinalResult.textResultForLimpBound.CTPPlatesColours = (((Convert.ToDecimal(limpBoundFinalData.limpBoundText.ColorFront) + Convert.ToDecimal(limpBoundFinalData.limpBoundText.ColorBack)) * Text4SigForLimpBoundText * CTPPlatesColoursSmall) / SW);
                        }
                        else
                        {
                            var CTPPlatesColoursBig = LimpBoundRates.Where(x => x.LimpBoundRatesName == "CTPPlatesColoursBig").Select(x => x.Price).FirstOrDefault();
                            limpBoundFinalResult.textResultForLimpBound.CTPPlatesColours = (((Convert.ToDecimal(limpBoundFinalData.limpBoundText.ColorFront) + Convert.ToDecimal(limpBoundFinalData.limpBoundText.ColorBack)) * Text4SigForLimpBoundText * CTPPlatesColoursBig) / SW);
                        }

                        var Text4Plotter = LimpBoundRates.Where(x => x.LimpBoundRatesName == "Text4Plotter").Select(x => x.Price).FirstOrDefault();
                        limpBoundFinalResult.textResultForLimpBound.Plotter = ((TextSizeFoldedBookHeight * TextSizeFoldedBookWidth * limpBoundFinalData.limpBoundExtent.Text * Text4SigForLimpBoundText) * Text4Plotter * limpBoundFinalData.limpBoundText.Proofs); //1=yes, 0=no need to clarify on this proofs value

                        var Text4Varnish = LimpBoundRates.Where(x => x.LimpBoundRatesName == "Text4Varnish").Select(x => x.Price).FirstOrDefault();
                        limpBoundFinalResult.textResultForLimpBound.Varnish = ((Text4SigForLimpBoundText * Quantity) * (Convert.ToDecimal(limpBoundFinalData.limpBoundText.Varnish)) / 1000 * Text4Varnish);

                        var Text4PrintingSpotVarnish = LimpBoundRates.Where(x => x.LimpBoundRatesName == "Text4PrintingSpotVarnish").Select(x => x.Price).FirstOrDefault();
                        limpBoundFinalResult.textResultForLimpBound.PrintingSpotVarnish = ((Quantity * Text4SigForLimpBoundText / 1000) * (Convert.ToDecimal(limpBoundFinalData.limpBoundText.SpotVarnish)) * Text4PrintingSpotVarnish);

                        var Text4MakereadySpotVarnish = LimpBoundRates.Where(x => x.LimpBoundRatesName == "Text4MakereadySpotVarnish").Select(x => x.Price).FirstOrDefault();
                        limpBoundFinalResult.textResultForLimpBound.MakereadySpotVarnish = ((Convert.ToDecimal(limpBoundFinalData.limpBoundText.SpotVarnish)) * Text4SigForLimpBoundText * Text4MakereadySpotVarnish);//need to clarify on spotvarnish front and back value to be taken from cover or text???

                        var Text4CTPPlatesSpotVarnish = LimpBoundRates.Where(x => x.LimpBoundRatesName == "Text4CTPPlatesSpotVarnish").Select(x => x.Price).FirstOrDefault();
                        limpBoundFinalResult.textResultForLimpBound.CTPPlatesSpotVarnish = ((Convert.ToDecimal(limpBoundFinalData.limpBoundText.SpotVarnish)) * Text4SigForLimpBoundText * Text4CTPPlatesSpotVarnish);//need to clarify on spotvarnish front and back value to be taken from cover or text???

                        var Text4Folding = LimpBoundRates.Where(x => x.LimpBoundRatesName == "Text4Folding").Select(x => x.Price).FirstOrDefault();
                        limpBoundFinalResult.textResultForLimpBound.Folding = ((Quantity * Text4SigForLimpBoundText) / 1000 * Text4Folding);

                        var Text4MakereadyFolding = LimpBoundRates.Where(x => x.LimpBoundRatesName == "Text4MakereadyFolding").Select(x => x.Price).FirstOrDefault();
                        try
                        {
                            limpBoundFinalResult.textResultForLimpBound.MakereadyFolding = ((Text4SigForLimpBoundText / Text4SigForLimpBoundText) * Text4MakereadyFolding);
                        }
                        catch
                        {
                            limpBoundFinalResult.textResultForLimpBound.MakereadyFolding = 0;
                        }

                        var Text4PrintingColorWastage = (Quantity / TextCut) * Text4SigForLimpBoundText;
                        var Text4MakeReadyPrintingColorWastage = (Convert.ToDecimal(limpBoundFinalData.limpBoundText.ColorFront) + Convert.ToDecimal(limpBoundFinalData.limpBoundText.ColorBack)) * PrintingWastage / (TextCut / Text4UpsForLimpBoundText) * Text4SigForLimpBoundText;
                        var Text4VarnishWastage = (Convert.ToDecimal(limpBoundFinalData.limpBoundText.Varnish)) * VarnishWastage / (TextCut / Text4UpsForLimpBoundText) * Text4SigForLimpBoundText;
                        var Text4PrintingSpotVarnishWastage = (Convert.ToDecimal(limpBoundFinalData.limpBoundText.SpotVarnish)) * SpotVarnishWastage / (TextCut / Text4UpsForLimpBoundText) * Text4SigForLimpBoundText;
                        var Text4FoldingWastage = (FoldingWastage / TextCut) * Text4SigForLimpBoundText;

                        var Text4PaperTotalWastage = Text4PrintingColorWastage + Text4MakeReadyPrintingColorWastage + Text4VarnishWastage + Text4PrintingSpotVarnishWastage + Text4FoldingWastage;

                        var TextPriceReam = ((TextSizeHeight / 1000 * TextSizeWidth / 1000 * Convert.ToDecimal(limpBoundFinalData.limpBoundText.GSM)) / 2 * Convert.ToDecimal(2.2046) * Convert.ToDecimal(limpBoundFinalData.limpBoundText.PaperCost));
                        var TextPriceSheet = TextPriceReam / 500;

                        limpBoundFinalResult.textResultForLimpBound.PaperTotal = Text4PaperTotalWastage * TextPriceSheet;

                        decimal FXProofsValueForText = 0;
                        if ((Convert.ToDecimal(limpBoundFinalData.limpBoundText.ColorFront) + Convert.ToDecimal(limpBoundFinalData.limpBoundText.ColorBack)) <= 1)
                        {
                            FXProofsValueForText = Convert.ToDecimal(0.50);
                        }
                        else
                        {
                            FXProofsValueForText = 1;
                        }

                        //var Text32FXProofs = LimpBoundRates.Where(x => x.LimpBoundRatesName == "Text32FXProofs").Select(x => x.Price).FirstOrDefault();
                        limpBoundFinalResult.textResultForLimpBound.FXProofs = ((FXProofsValueForText * 0));//4c-$1.00;  1c-$0.50 need clarification on this
                    }

                    //Binding
                    var Collate = LimpBoundRates.Where(x => x.LimpBoundRatesName == "Collate").Select(x => x.Price).FirstOrDefault();
                    limpBoundFinalResult.bindingForLimpBound.Collate = Quantity * BindingSig * Collate / 1000;

                    var Sewing = LimpBoundRates.Where(x => x.LimpBoundRatesName == "Sewing").Select(x => x.Price).FirstOrDefault();
                    limpBoundFinalResult.bindingForLimpBound.Sewing = (Quantity * BindingSig * Sewing / 1000) * 1; //1=yes 0=no need to clarify on this

                    var MakeReadySewing = LimpBoundRates.Where(x => x.LimpBoundRatesName == "MakeReadySewing").Select(x => x.Price).FirstOrDefault();
                    try
                    {
                        limpBoundFinalResult.bindingForLimpBound.MakeReadySewing = ((BindingSig / BindingSig) * MakeReadySewing) * 1;//1=yes 0=no need to clarify on this
                    }
                    catch
                    {
                        limpBoundFinalResult.bindingForLimpBound.MakeReadySewing = 0;
                    }

                    var DrawnOnCover = LimpBoundRates.Where(x => x.LimpBoundRatesName == "DrawnOnCover").Select(x => x.Price).FirstOrDefault();
                    limpBoundFinalResult.bindingForLimpBound.DrawOnCover = (Quantity / 1000) * DrawnOnCover;

                    limpBoundFinalResult.bindingForLimpBound.SetUpPerfectBlind = 30;

                    //Packing
                    var PaperWrap = LimpBoundRates.Where(x => x.LimpBoundRatesName == "PaperWrap").Select(x => x.Price).FirstOrDefault();
                    try
                    {
                        limpBoundFinalResult.packingForLimpBound.PaperWrap = (Quantity / limpBoundFinalData.PaperWrap) * PaperWrap;
                    }
                    catch
                    {
                        limpBoundFinalResult.packingForLimpBound.PaperWrap = 0;
                    }

                    var Cartonize = LimpBoundRates.Where(x => x.LimpBoundRatesName == "Cartonize").Select(x => x.Price).FirstOrDefault();
                    try
                    {
                        limpBoundFinalResult.packingForLimpBound.Cartonize = (Quantity / limpBoundFinalData.Carton) * Cartonize;
                    }
                    catch
                    {
                        limpBoundFinalResult.packingForLimpBound.Cartonize = 0;
                    }

                    var Pallet = LimpBoundRates.Where(x => x.LimpBoundRatesName == "Pallet").Select(x => x.Price).FirstOrDefault();
                    try
                    {
                        limpBoundFinalResult.packingForLimpBound.Pallet = (Quantity / limpBoundFinalData.Pallet) * Pallet;
                    }
                    catch
                    {
                        limpBoundFinalResult.packingForLimpBound.Pallet = 0;
                    }

                    var ShrinkWrap = LimpBoundRates.Where(x => x.LimpBoundRatesName == "ShrinkWrap").Select(x => x.Price).FirstOrDefault();
                    try
                    {
                        limpBoundFinalResult.packingForLimpBound.ShrinkWrap = (Quantity / limpBoundFinalData.ShrinkWrap) * ShrinkWrap;
                    }
                    catch
                    {
                        limpBoundFinalResult.packingForLimpBound.ShrinkWrap = 0;
                    }

                    //Delivery
                    var Location = LimpBoundRates.Where(x => x.LimpBoundRatesName == "Location").Select(x => x.Price).FirstOrDefault();
                    limpBoundFinalResult.deliveryForLimpBound.Location = Location * limpBoundFinalData.Delivery;

                    var Courier = LimpBoundRates.Where(x => x.LimpBoundRatesName == "Courier").Select(x => x.Price).FirstOrDefault();
                    limpBoundFinalResult.deliveryForLimpBound.Courier = Courier * limpBoundFinalData.Courier;

                    limpBoundFinalResult.SubTotal = limpBoundFinalResult.coverResultForLimpbound.CoverTotal + limpBoundFinalResult.textResultForLimpBound.TextTotal + limpBoundFinalResult.bindingForLimpBound.BindingTotal + limpBoundFinalResult.packingForLimpBound.PackingTotal + limpBoundFinalResult.deliveryForLimpBound.DeliveryTotal;

                    var MarkupPercentage = LimpBoundRates.Where(x => x.LimpBoundRatesName == "MarkupPercentage").Select(x => x.Price).FirstOrDefault();
                    limpBoundFinalResult.MarkUp = ((limpBoundFinalResult.SubTotal * MarkupPercentage) / 100);
                    limpBoundFinalResult.Total = limpBoundFinalResult.SubTotal + limpBoundFinalResult.MarkUp;
                    limpBoundFinalResult.Unit = (limpBoundFinalResult.Total / Quantity);
                    limpBoundFinalResult.Quoted = limpBoundFinalResult.Unit * Quantity;

                    quantity.NoOfQuantity = Quantity;
                    quantity.Price = Convert.ToDouble(limpBoundFinalResult.Total);
                    quantities.Add(quantity);
                }

                string DestinationName = DateTime.Now.ToString("ddMMyyyyhhmmss") + ".xlsx";
                string SourceName = SalesCalcConstants.SourceName;
                string sourcePath = SalesCalcConstants.SourcePath;
                string targetPath = SalesCalcConstants.TargetPath;

                // Use Path class to manipulate file and directory paths.
                string sourceFile = System.IO.Path.Combine(sourcePath, SourceName);
                string destFile = System.IO.Path.Combine(targetPath, DestinationName);

                // To copy a folder's contents to a new location:
                // Create a new target folder.
                // If the directory already exists, this method does not create a new directory.....
                System.IO.Directory.CreateDirectory(targetPath);

                // To copy a file to another location and
                // overwrite the destination file if it already exists.
                System.IO.File.Copy(sourceFile, destFile, true);
                FileInfo file = new FileInfo(destFile);

                //create a new Excel package from the file
                using (ExcelPackage excelPackage = new ExcelPackage(file))
                {
                    //create an instance of the the first sheet in the loaded file
                    using (ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets["Sheet1"])
                    {
                        worksheet.Cells[5, 2].Value = DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss");
                        worksheet.Cells[7, 2].Value = "LimpBound Data";
                        worksheet.Cells[8, 2].Value = "";
                        worksheet.Cells[12, 2].Value = "LimpBound Book";
                        worksheet.Cells[14, 2].Value = "Cover -" + limpBoundFinalData.limpBoundCover.FoldedSize;
                        worksheet.Cells[15, 2].Value = "Cover -" + limpBoundFinalData.limpBoundCover.OpenedSize;
                        worksheet.Cells[14, 3].Value = "Text -" + limpBoundFinalData.limpBoundText.FoldedSize;
                        worksheet.Cells[15, 3].Value = "Text -" + limpBoundFinalData.limpBoundText.OpenedSize;
                        worksheet.Cells[16, 2].Value = "Cover -" + limpBoundFinalData.limpBoundExtent.Cover;
                        worksheet.Cells[16, 3].Value = "Text -" + limpBoundFinalData.limpBoundExtent.Text;
                        worksheet.Cells[17, 2].Value = "Cover- " + limpBoundFinalData.limpBoundCover.PaperType;
                        worksheet.Cells[18, 2].Value = "Text- " + limpBoundFinalData.limpBoundText.PaperType;
                        worksheet.Cells[19, 2].Value = "Cover- " + limpBoundFinalData.limpBoundCover.ColorFront + "X" + limpBoundFinalData.limpBoundCover.ColorBack;
                        worksheet.Cells[20, 2].Value = "Text- " + limpBoundFinalData.limpBoundText.ColorFront + "X" + limpBoundFinalData.limpBoundText.ColorBack;
                        worksheet.Cells[22, 2].Value = "";
                        worksheet.Cells[23, 2].Value = "LimpBound";
                        worksheet.Cells[24, 2].Value = "-";

                        int n = 1;
                        foreach (var items in quantities)
                        {
                            if (n == 1)
                            {
                                worksheet.Cells[27, 2].Value = items.NoOfQuantity;
                                worksheet.Cells[28, 2].Value = items.Price;
                            }
                            if (n == 2)
                            {
                                worksheet.Cells[27, 3].Value = items.NoOfQuantity;
                                worksheet.Cells[28, 3].Value = items.Price;
                            }
                            if (n == 3)
                            {
                                worksheet.Cells[27, 4].Value = items.NoOfQuantity;
                                worksheet.Cells[28, 4].Value = items.Price;
                            }
                            n++;
                        }
                        worksheet.Cells[41, 4].Value = "";

                        //save the changes
                        excelPackage.Save();
                    }

                }
                return SalesCalcConstants.TargetPathIIS + DestinationName;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }

        }

        public static int GetCutForLimpBoundCover()
        {
            return 4;
        }
        public static int GetUpsForLimpBoundCover()
        {
            return 1;
        }
        public static int GetSigForLimpBoundCover()
        {
            return 1;
        }
        //public static int GetText32CutForLimpBoundText()
        //{
        //    return 1;
        //}

        //public static int GetText16CutForLimpBoundText()
        //{
        //    return 1;
        //}
        //public static int GetText8CutForLimpBoundText()
        //{
        //    return 2;
        //}
        //public static int GetText4CutForLimpBoundText()
        //{
        //    return 1;
        //}

        public static int GetText32UpsForLimpBoundText()
        {
            return 1;
        }
        public static int GetText16UpsForLimpBoundText()
        {
            return 1;
        }
        public static int GetText8UpsForLimpBoundText()
        {
            return 1;
        }
        public static int GetText4UpsForLimpBoundText()
        {
            return 1;
        }

        public static int GetText32SigForLimpBoundText()
        {
            return 1;
        }

        public static int GetText16SigForLimpBoundText()
        {
            return 1;
        }

        public static int GetText8SigForLimpBoundText()
        {
            return 1;
        }

        public static int GetText4SigForLimpBoundText()
        {
            return 1;
        }

        public static int GetBindingSigForLimpBound()
        {
            return 0; //need to clarify on this (they are adding all text sigs)
        }




        public static List<SaddleStitchedWastage> GetWastageDataForSaddleStitched()
        {
            List<SaddleStitchedWastage> saddleStitchedWastages = new List<SaddleStitchedWastage>();
            using (MySqlConnection connection = new MySqlConnection(SalesCalcConstants.SalesCalcConnection))
            {
                using (MySqlCommand command = new MySqlCommand(SalesCalcConstants.sp_GetWastageDataForSaddleStitched, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Connection.Open();
                    command.CommandTimeout = 180000;
                    MySqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        SaddleStitchedWastage saddleStitchedWastage = new SaddleStitchedWastage();
                        saddleStitchedWastage.SaddleStitchedWastageId = Convert.ToInt32(reader["SaddleStitchedWastageId"]);
                        saddleStitchedWastage.SaddleStitchedWastageName = Convert.ToString(reader["SaddleStitchedWastageName"]);
                        saddleStitchedWastage.SaddleStitchedWastageCode = Convert.ToString(reader["SaddleStitchedWastageCode"]);
                        saddleStitchedWastage.SaddleStitchedWastageCost = Convert.ToDouble(reader["SaddleStitchedWastageCost"]);
                        saddleStitchedWastages.Add(saddleStitchedWastage);

                    }
                    command.Connection.Close();
                }
            }
            return saddleStitchedWastages;
        }


        public static List<SaddleStitchedRates> GetSaddleStitchedRates()
        {
            List<SaddleStitchedRates> saddleStitchedRates = new List<SaddleStitchedRates>();
            using (MySqlConnection connection = new MySqlConnection(SalesCalcConstants.SalesCalcConnection))
            {
                using (MySqlCommand command = new MySqlCommand(SalesCalcConstants.sp_GetSaddleStitchedRates, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Connection.Open();
                    command.CommandTimeout = 180000;
                    MySqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        SaddleStitchedRates saddleStitchedRate = new SaddleStitchedRates();
                        saddleStitchedRate.SaddleStitchedRatesId = Convert.ToInt32(reader["SaddleStitchedRatesId"]);
                        saddleStitchedRate.SaddleStitchedRatesName = Convert.ToString(reader["SaddleStitchedRatesName"]);
                        saddleStitchedRate.To1000 = Convert.ToDecimal(reader["1-1000"]);
                        saddleStitchedRate.To10000 = Convert.ToDecimal(reader["1001-10000"]);
                        saddleStitchedRate.To50000 = Convert.ToDecimal(reader["10001-50000"]);
                        saddleStitchedRate.Above50000 = Convert.ToDecimal(reader["50001-Above"]);
                        saddleStitchedRates.Add(saddleStitchedRate);

                    }
                    command.Connection.Close();
                }
            }
            return saddleStitchedRates;
        }





        public static string GetExcelReportForSaddleStitched(SaddleStitchedFinalData saddleStitchedFinalData)
        {
            var SaddleStitchedRates = GetSaddleStitchedRates();
            var WastageDataForSaddleStitched = GetWastageDataForSaddleStitched();
            SaddleStitchedFinalResult saddleStitchedFinalResult = new SaddleStitchedFinalResult();
            saddleStitchedFinalResult.bindingForSaddleStitched = new BindingForSaddleStitched();
            saddleStitchedFinalResult.coverResultForSaddleStitched = new CoverResultForSaddleStitched();
            saddleStitchedFinalResult.textResultForSaddleStitched = new TextResultForSaddleStitched();
            saddleStitchedFinalResult.deliveryForSaddleStitched = new DeliveryForSaddleStitched();
            saddleStitchedFinalResult.packingForSaddleStitched = new PackingForSaddleStitched();

            var PrintingWastage = Convert.ToDecimal(WastageDataForSaddleStitched.Where(x => x.SaddleStitchedWastageCode == "Printing").Select(x => x.SaddleStitchedWastageCost).FirstOrDefault());

            var VarnishWastage = Convert.ToDecimal(WastageDataForSaddleStitched.Where(x => x.SaddleStitchedWastageCode == "Varnish").Select(x => x.SaddleStitchedWastageCost).FirstOrDefault());

            var SpotVarnishWastage = Convert.ToDecimal(WastageDataForSaddleStitched.Where(x => x.SaddleStitchedWastageCode == "SpotVarnish").Select(x => x.SaddleStitchedWastageCost).FirstOrDefault());

            var GlossLaminateWastage = Convert.ToDecimal(WastageDataForSaddleStitched.Where(x => x.SaddleStitchedWastageCode == "GlossLaminate").Select(x => x.SaddleStitchedWastageCost).FirstOrDefault());

            var MattLaminateWastage = Convert.ToDecimal(WastageDataForSaddleStitched.Where(x => x.SaddleStitchedWastageCode == "MattLaminate").Select(x => x.SaddleStitchedWastageCost).FirstOrDefault());

            var UVVarnishWastage = Convert.ToDecimal(WastageDataForSaddleStitched.Where(x => x.SaddleStitchedWastageCode == "UVVarnish").Select(x => x.SaddleStitchedWastageCost).FirstOrDefault());

            var SpotUVWastage = Convert.ToDecimal(WastageDataForSaddleStitched.Where(x => x.SaddleStitchedWastageCode == "SpotUV").Select(x => x.SaddleStitchedWastageCost).FirstOrDefault());

            var FoilStampWastage = Convert.ToDecimal(WastageDataForSaddleStitched.Where(x => x.SaddleStitchedWastageCode == "FoilStamp").Select(x => x.SaddleStitchedWastageCost).FirstOrDefault());

            var BlindEmbossWastage = Convert.ToDecimal(WastageDataForSaddleStitched.Where(x => x.SaddleStitchedWastageCode == "BlindEmboss").Select(x => x.SaddleStitchedWastageCost).FirstOrDefault());

            var DieCutWastage = Convert.ToDecimal(WastageDataForSaddleStitched.Where(x => x.SaddleStitchedWastageCode == "DieCut").Select(x => x.SaddleStitchedWastageCost).FirstOrDefault());
            var HandFoldWastage = Convert.ToDecimal(WastageDataForSaddleStitched.Where(x => x.SaddleStitchedWastageCode == "HandFold").Select(x => x.SaddleStitchedWastageCost).FirstOrDefault());

            var FoldingWastage = Convert.ToDecimal(WastageDataForSaddleStitched.Where(x => x.SaddleStitchedWastageCode == "Folding").Select(x => x.SaddleStitchedWastageCost).FirstOrDefault());

            List<Quantity> quantities = new List<Quantity>();
            foreach (var Quantity in saddleStitchedFinalData.QuantityRequired)
            {
                Quantity quantity = new Quantity();
                if (Enumerable.Range(1, 1000).Contains(Quantity))
                {
                    foreach (var items in SaddleStitchedRates)
                    {
                        items.Price = items.To1000;
                    }
                }
                else if (Enumerable.Range(1001, 10000).Contains(Quantity))
                {
                    foreach (var items in SaddleStitchedRates)
                    {
                        items.Price = items.To10000;
                    }
                }
                else if (Enumerable.Range(10001, 50000).Contains(Quantity))
                {
                    foreach (var items in SaddleStitchedRates)
                    {
                        items.Price = items.To50000;
                    }
                }
                else
                {
                    foreach (var items in SaddleStitchedRates)
                    {
                        items.Price = items.Above50000;
                    }
                }

                var CoverSizeHeight = Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedCover.CoverSize.Split('X').FirstOrDefault());
                var CoverSizeWidth = Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedCover.CoverSize.Split('X').LastOrDefault());
                var CoverSizeFoldedBookHeight = Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedCover.FoldedSize.Split('X').FirstOrDefault());
                var CoverSizeFoldedBookWidth = Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedCover.FoldedSize.Split('X').LastOrDefault());
                var CoverSizeOpenedBookHeight = Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedCover.OpenedSize.Split('X').FirstOrDefault());
                var CoverSizeOpenedBookWidth = Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedCover.OpenedSize.Split('X').LastOrDefault());
                var CoverSizeFoilStampHeight = Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedCover.FoilStamp.Split('X').FirstOrDefault());
                var CoverSizeFoilStampWidth = Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedCover.FoilStamp.Split('X').LastOrDefault());
                var CoverSizeBlindEmbossHeight = Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedCover.BlindEmboss.Split('X').FirstOrDefault());
                var CoverSizeBlindEmbossWidth = Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedCover.BlindEmboss.Split('X').LastOrDefault());

                var TextSizeHeight = Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.TextSize.Split('X').FirstOrDefault());
                var TextSizeWidth = Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.TextSize.Split('X').LastOrDefault());
                var TextSizeFoldedBookHeight = Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.FoldedSize.Split('X').FirstOrDefault());
                var TextSizeFoldedBookWidth = Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.FoldedSize.Split('X').LastOrDefault());
                var TextSizeOpenedBookHeight = Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.OpenedSize.Split('X').FirstOrDefault());
                var TextSizeOpenedBookWidth = Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.OpenedSize.Split('X').LastOrDefault());


                var MinimumPaperSizeHeightForText = (TextSizeFoldedBookHeight * 2) + 38;
                var MinimumPaperSizeWidthForText = (TextSizeFoldedBookWidth * 4) + 26;
                var ResultForText = CommonCalculations.GetCommonCalculation((Convert.ToString(MinimumPaperSizeHeightForText) + "X" + Convert.ToString(MinimumPaperSizeWidthForText)), "");


                var SpineWidth = Convert.ToDecimal((((ResultForText.PPValue / 2) * 0) / 1000) + 0.5); // need to get clarification on value 0 => bulkiness value

                var MinimumPaperSizeHeightForCover = CoverSizeOpenedBookHeight + 28;
                var MinimumPaperSizeWidthForCover = CoverSizeOpenedBookWidth + SpineWidth + 10;
                var ResultForCover = CommonCalculations.GetCommonCalculation((Convert.ToString(MinimumPaperSizeHeightForCover) + "X" + Convert.ToString(MinimumPaperSizeWidthForCover)), "");

                saddleStitchedFinalData.saddleStitchedExtent.Cover = ResultForCover.PPValue;
                saddleStitchedFinalData.saddleStitchedExtent.Text = ResultForText.PPValue;

                var CutForSaddleStitchedCover = ResultForCover.Cut;
                var UpsForSaddleStitchedCover = GetUpsForSaddleStitchedCover();
                var SigForSaddleStitchedCover = GetSigForSaddleStitchedCover();

                var SW = GetSW();
                var WT = GetWT();

                var BindingSig = GetBindingSigForSaddleStitched();
                var TextCut = ResultForText.Cut;

                //32
                //var Text32CutForSaddleStitchedText = GetText32CutForSaddleStitchedText();
                var Text32UpsForSaddleStitchedText = GetText32UpsForSaddleStitchedText();
                var Text32SigForSaddleStitchedText = GetText32SigForSaddleStitchedText();

                //16
                //var Text16CutForSaddleStitchedText = GetText16CutForSaddleStitchedText();
                var Text16UpsForSaddleStitchedText = GetText16UpsForSaddleStitchedText();
                var Text16SigForSaddleStitchedText = GetText16SigForSaddleStitchedText();

                //8
                //var Text8CutForSaddleStitchedText = GetText8CutForSaddleStitchedText();
                var Text8UpsForSaddleStitchedText = GetText8UpsForSaddleStitchedText();
                var Text8SigForSaddleStitchedText = GetText8SigForSaddleStitchedText();

                //4
                //var Text4CutForSaddleStitchedText = GetText4CutForSaddleStitchedText();
                var Text4UpsForSaddleStitchedText = GetText4UpsForSaddleStitchedText();
                var Text4SigForSaddleStitchedText = GetText4SigForSaddleStitchedText();

                //Cover Calculations
                var CoverPrintingColour = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "CoverPrintingColour").Select(x => x.Price).FirstOrDefault();
                saddleStitchedFinalResult.coverResultForSaddleStitched.PrintingColour = ((Quantity / UpsForSaddleStitchedCover / 1000) * (Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedCover.ColorFront) + Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedCover.ColorBack)) * CoverPrintingColour);

                var CoverMakereadyPrintingColour = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "CoverMakereadyPrintingColour").Select(x => x.Price).FirstOrDefault();
                saddleStitchedFinalResult.coverResultForSaddleStitched.MakereadyPrintingColour = ((Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedCover.ColorFront) + Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedCover.ColorBack)) * SigForSaddleStitchedCover * CoverMakereadyPrintingColour);

                //var CoverCTPPlatesColours = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "CoverCTPPlatesColours").Select(x => x.Price).FirstOrDefault();
                if (Quantity <= 2000)
                {
                    var CTPPlatesColoursSmall = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "CTPPlatesColoursSmall").Select(x => x.Price).FirstOrDefault();
                    saddleStitchedFinalResult.coverResultForSaddleStitched.CTPPlatesColours = ((Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedCover.ColorFront) + Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedCover.ColorBack)) * SigForSaddleStitchedCover * CTPPlatesColoursSmall);
                }
                else
                {
                    var CTPPlatesColoursBig = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "CTPPlatesColoursBig").Select(x => x.Price).FirstOrDefault();
                    saddleStitchedFinalResult.coverResultForSaddleStitched.CTPPlatesColours = ((Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedCover.ColorFront) + Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedCover.ColorBack)) * SigForSaddleStitchedCover * CTPPlatesColoursBig);
                }


                var CoverPlotter = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "CoverPlotter").Select(x => x.Price).FirstOrDefault();
                saddleStitchedFinalResult.coverResultForSaddleStitched.Plotter = ((CoverSizeFoldedBookHeight * CoverSizeFoldedBookWidth) * saddleStitchedFinalData.saddleStitchedExtent.Cover * CoverPlotter * saddleStitchedFinalData.saddleStitchedCover.Proofs); //1= yes; 0=no need to clarify proofs value

                var CoverVarnish = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "CoverVarnish").Select(x => x.Price).FirstOrDefault();
                saddleStitchedFinalResult.coverResultForSaddleStitched.Varnish = ((Quantity / 1000 * CoverVarnish) / UpsForSaddleStitchedCover * (Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedCover.Varnish)));

                var CoverPrintingSpotVarnish = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "CoverPrintingSpotVarnish").Select(x => x.Price).FirstOrDefault();
                saddleStitchedFinalResult.coverResultForSaddleStitched.PrintingSpotVarnish = ((Quantity / UpsForSaddleStitchedCover / 1000) * (Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedCover.SpotVarnish)) * CoverPrintingSpotVarnish);

                var CoverMakereadySpotVarnish = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "CoverMakereadySpotVarnish").Select(x => x.Price).FirstOrDefault();
                saddleStitchedFinalResult.coverResultForSaddleStitched.MakereadySpotVarnish = ((Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedCover.SpotVarnish)) * SigForSaddleStitchedCover * CoverMakereadySpotVarnish);

                var CoverCTPPlatesSpotVarnish = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "CoverCTPPlatesSpotVarnish").Select(x => x.Price).FirstOrDefault();
                saddleStitchedFinalResult.coverResultForSaddleStitched.CTPPlatesSpotVarnish = ((Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedCover.SpotVarnish)) * SigForSaddleStitchedCover * CoverCTPPlatesSpotVarnish);

                if (saddleStitchedFinalData.saddleStitchedCover.LaminateGloss != 0)
                {
                    var LaminateGloss = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "CoverLaminateGloss").Select(x => x.Price).FirstOrDefault();
                    var LamGloss = ((CoverSizeOpenedBookHeight * CoverSizeOpenedBookWidth * saddleStitchedFinalData.saddleStitchedCover.LaminateGloss) * LaminateGloss * Quantity);
                    if (LamGloss <= 50)
                    {
                        saddleStitchedFinalResult.coverResultForSaddleStitched.LaminateGloss = 50;
                    }
                    else
                    {
                        saddleStitchedFinalResult.coverResultForSaddleStitched.LaminateGloss = LamGloss;
                    }

                }

                if (saddleStitchedFinalData.saddleStitchedCover.LaminateMatt != 0)
                {
                    var LaminateMatt = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "CoverLaminateMatt").Select(x => x.Price).FirstOrDefault();
                    var LamMatt = ((CoverSizeOpenedBookHeight * CoverSizeOpenedBookWidth * saddleStitchedFinalData.saddleStitchedCover.LaminateMatt) * LaminateMatt * Quantity);
                    if (LamMatt <= 50)
                    {
                        saddleStitchedFinalResult.coverResultForSaddleStitched.LaminateMatt = 50;
                    }
                    else
                    {
                        saddleStitchedFinalResult.coverResultForSaddleStitched.LaminateMatt = LamMatt;
                    }

                }

                if (saddleStitchedFinalData.saddleStitchedCover.UVVarnish != 0)
                {
                    var UVVarnish = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "CoverUVVarnish").Select(x => x.Price).FirstOrDefault();
                    var UVVar = ((CoverSizeOpenedBookHeight * CoverSizeOpenedBookWidth * saddleStitchedFinalData.saddleStitchedCover.UVVarnish) * UVVarnish * Quantity);
                    if (UVVar <= 70)
                    {
                        saddleStitchedFinalResult.coverResultForSaddleStitched.UVVarnish = 70;
                    }
                    else
                    {
                        saddleStitchedFinalResult.coverResultForSaddleStitched.UVVarnish = UVVar;
                    }

                }

                if (!string.IsNullOrEmpty(saddleStitchedFinalData.saddleStitchedCover.SpotUVVarnish))
                {
                    saddleStitchedFinalResult.coverResultForSaddleStitched.SpotUVVarnish = Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedCover.SpotUVVarnish) * Quantity;
                }

                if (!string.IsNullOrEmpty(saddleStitchedFinalData.saddleStitchedCover.FoilStampProcess))
                {
                    saddleStitchedFinalResult.coverResultForSaddleStitched.FoilStampProcess = Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedCover.FoilStampProcess) * Quantity;
                }

                if (!string.IsNullOrEmpty(saddleStitchedFinalData.saddleStitchedCover.BlindEmbossProcess))
                {
                    saddleStitchedFinalResult.coverResultForSaddleStitched.BlindEmbossProcess = Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedCover.BlindEmbossProcess) * Quantity;
                }


                var CoverFoilStampBlock = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "CoverFoilStampBlock").Select(x => x.Price).FirstOrDefault();
                saddleStitchedFinalResult.coverResultForSaddleStitched.FoilStampBlock = ((CoverSizeFoilStampHeight * CoverSizeFoilStampWidth) * CoverFoilStampBlock);

                var CoverBlindEmbossBlock = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "CoverBlindEmbossBlock").Select(x => x.Price).FirstOrDefault();
                saddleStitchedFinalResult.coverResultForSaddleStitched.BlindEmbossBlock = ((CoverSizeBlindEmbossHeight * CoverSizeBlindEmbossWidth) * CoverBlindEmbossBlock);



                var CoverPriceReam = ((CoverSizeHeight / 1000 * CoverSizeWidth / 1000 * Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedCover.GSM)) / 2 * Convert.ToDecimal(2.2046) * Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedCover.PaperCost));
                var CoverPriceSheet = CoverPriceReam / 500;

                var CoverPrintingColorWastage = Quantity / CutForSaddleStitchedCover;
                var CoverMakeReadyPrintingColorWastage = ((Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedCover.ColorFront) + Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedCover.ColorBack)) * PrintingWastage / (CutForSaddleStitchedCover / UpsForSaddleStitchedCover));
                var CoverVarnishWastage = ((Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedCover.Varnish)) * VarnishWastage / (CutForSaddleStitchedCover / UpsForSaddleStitchedCover));
                var CoverMakeReadySpotVarnishWastage = ((Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedCover.SpotVarnish)) * SpotVarnishWastage / (CutForSaddleStitchedCover / UpsForSaddleStitchedCover));
                var CoverLaminateGlossWastage = ((saddleStitchedFinalData.saddleStitchedCover.LaminateGloss * GlossLaminateWastage) / (CutForSaddleStitchedCover / UpsForSaddleStitchedCover));
                var CoverLaminateMattWastage = ((saddleStitchedFinalData.saddleStitchedCover.LaminateMatt * MattLaminateWastage) / (CutForSaddleStitchedCover / UpsForSaddleStitchedCover));
                var CoverUVVarnishWastage = ((saddleStitchedFinalData.saddleStitchedCover.UVVarnish * UVVarnishWastage) / (CutForSaddleStitchedCover / UpsForSaddleStitchedCover));
                var CoverSpotUVVarnishWastage = ((0 * SpotUVWastage) / (CutForSaddleStitchedCover / UpsForSaddleStitchedCover)); // need to clarify on value "0"
                var CoverFoilStampProcessWastage = ((0 * FoilStampWastage) / (CutForSaddleStitchedCover / UpsForSaddleStitchedCover)); // need to clarify on value "0"
                var CoverBlindEmbossProcessWastage = ((0 * BlindEmbossWastage) / (CutForSaddleStitchedCover / UpsForSaddleStitchedCover)); // need to clarify on value "0"
                var CoverPaperTotalWastage = CoverPrintingColorWastage + CoverMakeReadyPrintingColorWastage + CoverVarnishWastage + CoverMakeReadySpotVarnishWastage + CoverLaminateGlossWastage + CoverLaminateMattWastage + CoverUVVarnishWastage + CoverSpotUVVarnishWastage + CoverFoilStampProcessWastage + CoverBlindEmbossProcessWastage;

                saddleStitchedFinalResult.coverResultForSaddleStitched.PaperTotal = (CoverPaperTotalWastage * CoverPriceSheet);

                decimal FXProofsValueForCover = 0;
                if ((Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedCover.ColorFront) + Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedCover.ColorBack)) <= 1)
                {
                    FXProofsValueForCover = Convert.ToDecimal(0.50);
                }
                else
                {
                    FXProofsValueForCover = 1;
                }

                //var CoverFXProofs = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "CoverFXProofs").Select(x => x.Price).FirstOrDefault();
                saddleStitchedFinalResult.coverResultForSaddleStitched.FXProofs = (FXProofsValueForCover * 4); //4c-$1.00;  1c-$0.50 need clarification on this



                //Text Calculations


                if (ResultForText.PPValue == 32)
                {
                    var Text32PrintingColour = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "Text32PrintingColour").Select(x => x.Price).FirstOrDefault();
                    saddleStitchedFinalResult.textResultForSaddleStitched.PrintingColour = ((Quantity / 1000) * Text32PrintingColour * (Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.ColorFront) + Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.ColorBack)) * Text32SigForSaddleStitchedText);

                    var Text32MakereadyPrintingcolour = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "Text32MakereadyPrintingcolour").Select(x => x.Price).FirstOrDefault();
                    saddleStitchedFinalResult.textResultForSaddleStitched.MakereadyPrintingColour = ((Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.ColorFront) + Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.ColorBack)) * Text32SigForSaddleStitchedText * Text32MakereadyPrintingcolour);

                    //var Text32CTPPlatesColours = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "Text32CTPPlatesColours").Select(x => x.Price).FirstOrDefault();
                    if (Quantity <= 2000)
                    {
                        var CTPPlatesColoursSmall = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "CTPPlatesColoursSmall").Select(x => x.Price).FirstOrDefault();
                        saddleStitchedFinalResult.textResultForSaddleStitched.CTPPlatesColours = (((Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.ColorFront) + Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.ColorBack)) * Text32SigForSaddleStitchedText * CTPPlatesColoursSmall) / SW);
                    }
                    else
                    {
                        var CTPPlatesColoursBig = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "CTPPlatesColoursBig").Select(x => x.Price).FirstOrDefault();
                        saddleStitchedFinalResult.textResultForSaddleStitched.CTPPlatesColours = (((Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.ColorFront) + Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.ColorBack)) * Text32SigForSaddleStitchedText * CTPPlatesColoursBig) / SW);
                    }

                    var Text32Plotter = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "Text32Plotter").Select(x => x.Price).FirstOrDefault();
                    saddleStitchedFinalResult.textResultForSaddleStitched.Plotter = ((TextSizeFoldedBookHeight * TextSizeFoldedBookWidth * saddleStitchedFinalData.saddleStitchedExtent.Text * Text32SigForSaddleStitchedText) * Text32Plotter * saddleStitchedFinalData.saddleStitchedText.Proofs); //1=yes, 0=no need to clarify on this proofs value

                    var Text32Varnish = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "Text32Varnish").Select(x => x.Price).FirstOrDefault();
                    saddleStitchedFinalResult.textResultForSaddleStitched.Varnish = ((Text32SigForSaddleStitchedText * Quantity) * (Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.Varnish)) / 1000 * Text32Varnish);

                    var Text32PrintingSpotVarnish = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "Text32PrintingSpotVarnish").Select(x => x.Price).FirstOrDefault();
                    saddleStitchedFinalResult.textResultForSaddleStitched.PrintingSpotVarnish = ((Quantity * Text32SigForSaddleStitchedText / 1000) * (Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.SpotVarnish)) * Text32PrintingSpotVarnish);

                    var Text32MakereadySpotVarnish = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "Text32MakereadySpotVarnish").Select(x => x.Price).FirstOrDefault();
                    saddleStitchedFinalResult.textResultForSaddleStitched.MakereadySpotVarnish = ((Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.SpotVarnish)) * Text32SigForSaddleStitchedText * Text32MakereadySpotVarnish);//need to clarify on spotvarnish front and back value to be taken from cover or text???

                    var Text32CTPPlatesSpotVarnish = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "Text32CTPPlatesSpotVarnish").Select(x => x.Price).FirstOrDefault();
                    saddleStitchedFinalResult.textResultForSaddleStitched.CTPPlatesSpotVarnish = ((Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.SpotVarnish)) * Text32SigForSaddleStitchedText * Text32CTPPlatesSpotVarnish);//need to clarify on spotvarnish front and back value to be taken from cover or text???

                    var Text32Folding = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "Text32Folding").Select(x => x.Price).FirstOrDefault();
                    saddleStitchedFinalResult.textResultForSaddleStitched.Folding = ((Quantity * Text32SigForSaddleStitchedText) / 1000 * Text32Folding);

                    var Text32MakereadyFolding = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "Text32MakereadyFolding").Select(x => x.Price).FirstOrDefault();
                    try
                    {
                        saddleStitchedFinalResult.textResultForSaddleStitched.MakereadyFolding = ((Text32SigForSaddleStitchedText / Text32SigForSaddleStitchedText) * Text32MakereadyFolding);
                    }
                    catch
                    {
                        saddleStitchedFinalResult.textResultForSaddleStitched.MakereadyFolding = 0;
                    }

                    var Text32PrintingColorWastage = (Quantity / TextCut) * Text32SigForSaddleStitchedText;
                    var Text32MakeReadyPrintingColorWastage = (Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.ColorFront) + Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.ColorBack)) * PrintingWastage / (TextCut / Text32UpsForSaddleStitchedText) * Text32SigForSaddleStitchedText;
                    var Text32VarnishWastage = (Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.Varnish)) * VarnishWastage / (TextCut / Text32UpsForSaddleStitchedText) * Text32SigForSaddleStitchedText;
                    var Text32PrintingSpotVarnishWastage = (Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.SpotVarnish)) * SpotVarnishWastage / (TextCut / Text32UpsForSaddleStitchedText) * Text32SigForSaddleStitchedText;
                    var Text32FoldingWastage = (FoldingWastage / TextCut) * Text32SigForSaddleStitchedText;

                    var Text32PaperTotalWastage = Text32PrintingColorWastage + Text32MakeReadyPrintingColorWastage + Text32VarnishWastage + Text32PrintingSpotVarnishWastage + Text32FoldingWastage;

                    var TextPriceReam = ((TextSizeHeight / 1000 * TextSizeWidth / 1000 * Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.GSM)) / 2 * Convert.ToDecimal(2.2046) * Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.PaperCost));
                    var TextPriceSheet = TextPriceReam / 500;

                    saddleStitchedFinalResult.textResultForSaddleStitched.PaperTotal = Text32PaperTotalWastage * TextPriceSheet;

                    decimal FXProofsValueForText = 0;
                    if ((Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.ColorFront) + Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.ColorBack)) <= 1)
                    {
                        FXProofsValueForText = Convert.ToDecimal(0.50);
                    }
                    else
                    {
                        FXProofsValueForText = 1;
                    }

                    //var Text32FXProofs = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "Text32FXProofs").Select(x => x.Price).FirstOrDefault();
                    saddleStitchedFinalResult.textResultForSaddleStitched.FXProofs = ((FXProofsValueForText * 0));//4c-$1.00;  1c-$0.50 need clarification on this
                }
                else if (ResultForText.PPValue == 16)
                {
                    var Text16PrintingColour = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "Text16PrintingColour").Select(x => x.Price).FirstOrDefault();
                    saddleStitchedFinalResult.textResultForSaddleStitched.PrintingColour = ((Quantity / 1000) * Text16PrintingColour * (Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.ColorFront) + Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.ColorBack)) * Text16SigForSaddleStitchedText);

                    var Text16MakereadyPrintingcolour = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "Text16MakereadyPrintingcolour").Select(x => x.Price).FirstOrDefault();
                    saddleStitchedFinalResult.textResultForSaddleStitched.MakereadyPrintingColour = ((Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.ColorFront) + Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.ColorBack)) * Text16SigForSaddleStitchedText * Text16MakereadyPrintingcolour);

                    //var Text32CTPPlatesColours = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "Text32CTPPlatesColours").Select(x => x.Price).FirstOrDefault();
                    if (Quantity <= 2000)
                    {
                        var CTPPlatesColoursSmall = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "CTPPlatesColoursSmall").Select(x => x.Price).FirstOrDefault();
                        saddleStitchedFinalResult.textResultForSaddleStitched.CTPPlatesColours = (((Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.ColorFront) + Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.ColorBack)) * Text16SigForSaddleStitchedText * CTPPlatesColoursSmall) / SW);
                    }
                    else
                    {
                        var CTPPlatesColoursBig = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "CTPPlatesColoursBig").Select(x => x.Price).FirstOrDefault();
                        saddleStitchedFinalResult.textResultForSaddleStitched.CTPPlatesColours = (((Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.ColorFront) + Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.ColorBack)) * Text16SigForSaddleStitchedText * CTPPlatesColoursBig) / SW);
                    }

                    var Text16Plotter = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "Text16Plotter").Select(x => x.Price).FirstOrDefault();
                    saddleStitchedFinalResult.textResultForSaddleStitched.Plotter = ((TextSizeFoldedBookHeight * TextSizeFoldedBookWidth * saddleStitchedFinalData.saddleStitchedExtent.Text * Text16SigForSaddleStitchedText) * Text16Plotter * saddleStitchedFinalData.saddleStitchedText.Proofs); //1=yes, 0=no need to clarify on this proofs value

                    var Text16Varnish = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "Text16Varnish").Select(x => x.Price).FirstOrDefault();
                    saddleStitchedFinalResult.textResultForSaddleStitched.Varnish = ((Text16SigForSaddleStitchedText * Quantity) * (Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.Varnish)) / 1000 * Text16Varnish);

                    var Text16PrintingSpotVarnish = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "Text16PrintingSpotVarnish").Select(x => x.Price).FirstOrDefault();
                    saddleStitchedFinalResult.textResultForSaddleStitched.PrintingSpotVarnish = ((Quantity * Text16SigForSaddleStitchedText / 1000) * (Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.SpotVarnish)) * Text16PrintingSpotVarnish);

                    var Text16MakereadySpotVarnish = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "Text16MakereadySpotVarnish").Select(x => x.Price).FirstOrDefault();
                    saddleStitchedFinalResult.textResultForSaddleStitched.MakereadySpotVarnish = ((Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.SpotVarnish)) * Text16SigForSaddleStitchedText * Text16MakereadySpotVarnish);//need to clarify on spotvarnish front and back value to be taken from cover or text???

                    var Text16CTPPlatesSpotVarnish = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "Text16CTPPlatesSpotVarnish").Select(x => x.Price).FirstOrDefault();
                    saddleStitchedFinalResult.textResultForSaddleStitched.CTPPlatesSpotVarnish = ((Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.SpotVarnish)) * Text16SigForSaddleStitchedText * Text16CTPPlatesSpotVarnish);//need to clarify on spotvarnish front and back value to be taken from cover or text???

                    var Text16Folding = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "Text16Folding").Select(x => x.Price).FirstOrDefault();
                    saddleStitchedFinalResult.textResultForSaddleStitched.Folding = ((Quantity * Text16SigForSaddleStitchedText) / 1000 * Text16Folding);

                    var Text16MakereadyFolding = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "Text16MakereadyFolding").Select(x => x.Price).FirstOrDefault();
                    try
                    {
                        saddleStitchedFinalResult.textResultForSaddleStitched.MakereadyFolding = ((Text16SigForSaddleStitchedText / Text16SigForSaddleStitchedText) * Text16MakereadyFolding);
                    }
                    catch
                    {
                        saddleStitchedFinalResult.textResultForSaddleStitched.MakereadyFolding = 0;
                    }

                    var Text16PrintingColorWastage = (Quantity / TextCut) * Text16SigForSaddleStitchedText;
                    var Text16MakeReadyPrintingColorWastage = (Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.ColorFront) + Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.ColorBack)) * PrintingWastage / (TextCut / Text16UpsForSaddleStitchedText) * Text16SigForSaddleStitchedText;
                    var Text16VarnishWastage = (Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.Varnish)) * VarnishWastage / (TextCut / Text16UpsForSaddleStitchedText) * Text16SigForSaddleStitchedText;
                    var Text16PrintingSpotVarnishWastage = (Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.SpotVarnish)) * SpotVarnishWastage / (TextCut / Text16UpsForSaddleStitchedText) * Text16SigForSaddleStitchedText;
                    var Text16FoldingWastage = (FoldingWastage / TextCut) * Text16SigForSaddleStitchedText;

                    var Text16PaperTotalWastage = Text16PrintingColorWastage + Text16MakeReadyPrintingColorWastage + Text16VarnishWastage + Text16PrintingSpotVarnishWastage + Text16FoldingWastage;

                    var TextPriceReam = ((TextSizeHeight / 1000 * TextSizeWidth / 1000 * Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.GSM)) / 2 * Convert.ToDecimal(2.2046) * Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.PaperCost));
                    var TextPriceSheet = TextPriceReam / 500;

                    saddleStitchedFinalResult.textResultForSaddleStitched.PaperTotal = Text16PaperTotalWastage * TextPriceSheet;

                    decimal FXProofsValueForText = 0;
                    if ((Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.ColorFront) + Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.ColorBack)) <= 1)
                    {
                        FXProofsValueForText = Convert.ToDecimal(0.50);
                    }
                    else
                    {
                        FXProofsValueForText = 1;
                    }

                    //var Text32FXProofs = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "Text32FXProofs").Select(x => x.Price).FirstOrDefault();
                    saddleStitchedFinalResult.textResultForSaddleStitched.FXProofs = ((FXProofsValueForText * 0));//4c-$1.00;  1c-$0.50 need clarification on this
                }
                else if (ResultForText.PPValue == 8)
                {
                    var Text8PrintingColour = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "Text8PrintingColour").Select(x => x.Price).FirstOrDefault();
                    saddleStitchedFinalResult.textResultForSaddleStitched.PrintingColour = ((Quantity / 1000) * Text8PrintingColour * (Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.ColorFront) + Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.ColorBack)) * Text8SigForSaddleStitchedText);

                    var Text8MakereadyPrintingcolour = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "Text8MakereadyPrintingcolour").Select(x => x.Price).FirstOrDefault();
                    saddleStitchedFinalResult.textResultForSaddleStitched.MakereadyPrintingColour = ((Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.ColorFront) + Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.ColorBack)) * Text8SigForSaddleStitchedText * Text8MakereadyPrintingcolour);

                    //var Text32CTPPlatesColours = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "Text32CTPPlatesColours").Select(x => x.Price).FirstOrDefault();
                    if (Quantity <= 2000)
                    {
                        var CTPPlatesColoursSmall = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "CTPPlatesColoursSmall").Select(x => x.Price).FirstOrDefault();
                        saddleStitchedFinalResult.textResultForSaddleStitched.CTPPlatesColours = (((Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.ColorFront) + Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.ColorBack)) * Text8SigForSaddleStitchedText * CTPPlatesColoursSmall) / SW);
                    }
                    else
                    {
                        var CTPPlatesColoursBig = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "CTPPlatesColoursBig").Select(x => x.Price).FirstOrDefault();
                        saddleStitchedFinalResult.textResultForSaddleStitched.CTPPlatesColours = (((Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.ColorFront) + Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.ColorBack)) * Text8SigForSaddleStitchedText * CTPPlatesColoursBig) / SW);
                    }

                    var Text8Plotter = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "Text8Plotter").Select(x => x.Price).FirstOrDefault();
                    saddleStitchedFinalResult.textResultForSaddleStitched.Plotter = ((TextSizeFoldedBookHeight * TextSizeFoldedBookWidth * saddleStitchedFinalData.saddleStitchedExtent.Text * Text8SigForSaddleStitchedText) * Text8Plotter * saddleStitchedFinalData.saddleStitchedText.Proofs); //1=yes, 0=no need to clarify on this proofs value

                    var Text8Varnish = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "Text8Varnish").Select(x => x.Price).FirstOrDefault();
                    saddleStitchedFinalResult.textResultForSaddleStitched.Varnish = ((Text8SigForSaddleStitchedText * Quantity) * (Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.Varnish)) / 1000 * Text8Varnish);

                    var Text8PrintingSpotVarnish = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "Text8PrintingSpotVarnish").Select(x => x.Price).FirstOrDefault();
                    saddleStitchedFinalResult.textResultForSaddleStitched.PrintingSpotVarnish = ((Quantity * Text8SigForSaddleStitchedText / 1000) * (+Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.SpotVarnish)) * Text8PrintingSpotVarnish);

                    var Text8MakereadySpotVarnish = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "Text8MakereadySpotVarnish").Select(x => x.Price).FirstOrDefault();
                    saddleStitchedFinalResult.textResultForSaddleStitched.MakereadySpotVarnish = ((Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.SpotVarnish)) * Text8SigForSaddleStitchedText * Text8MakereadySpotVarnish);//need to clarify on spotvarnish front and back value to be taken from cover or text???

                    var Text8CTPPlatesSpotVarnish = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "Text8CTPPlatesSpotVarnish").Select(x => x.Price).FirstOrDefault();
                    saddleStitchedFinalResult.textResultForSaddleStitched.CTPPlatesSpotVarnish = ((Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.SpotVarnish)) * Text8SigForSaddleStitchedText * Text8CTPPlatesSpotVarnish);//need to clarify on spotvarnish front and back value to be taken from cover or text???

                    var Text8Folding = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "Text8Folding").Select(x => x.Price).FirstOrDefault();
                    saddleStitchedFinalResult.textResultForSaddleStitched.Folding = ((Quantity * Text8SigForSaddleStitchedText) / 1000 * Text8Folding);

                    var Text8MakereadyFolding = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "Text8MakereadyFolding").Select(x => x.Price).FirstOrDefault();
                    try
                    {
                        saddleStitchedFinalResult.textResultForSaddleStitched.MakereadyFolding = ((Text8SigForSaddleStitchedText / Text8SigForSaddleStitchedText) * Text8MakereadyFolding);
                    }
                    catch
                    {
                        saddleStitchedFinalResult.textResultForSaddleStitched.MakereadyFolding = 0;
                    }

                    var Text8PrintingColorWastage = (Quantity / TextCut) * Text8SigForSaddleStitchedText;
                    var Text8MakeReadyPrintingColorWastage = (Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.ColorFront) + Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.ColorBack)) * PrintingWastage / (TextCut / Text8UpsForSaddleStitchedText) * Text8SigForSaddleStitchedText;
                    var Text8VarnishWastage = (Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.Varnish)) * VarnishWastage / (TextCut / Text8UpsForSaddleStitchedText) * Text8SigForSaddleStitchedText;
                    var Text8PrintingSpotVarnishWastage = (Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.SpotVarnish)) * SpotVarnishWastage / (TextCut / Text8UpsForSaddleStitchedText) * Text8SigForSaddleStitchedText;
                    var Text8FoldingWastage = (FoldingWastage / TextCut) * Text8SigForSaddleStitchedText;

                    var Text8PaperTotalWastage = Text8PrintingColorWastage + Text8MakeReadyPrintingColorWastage + Text8VarnishWastage + Text8PrintingSpotVarnishWastage + Text8FoldingWastage;

                    var TextPriceReam = ((TextSizeHeight / 1000 * TextSizeWidth / 1000 * Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.GSM)) / 2 * Convert.ToDecimal(2.2046) * Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.PaperCost));
                    var TextPriceSheet = TextPriceReam / 500;

                    saddleStitchedFinalResult.textResultForSaddleStitched.PaperTotal = Text8PaperTotalWastage * TextPriceSheet;

                    decimal FXProofsValueForText = 0;
                    if ((Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.ColorFront) + Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.ColorBack)) <= 1)
                    {
                        FXProofsValueForText = Convert.ToDecimal(0.50);
                    }
                    else
                    {
                        FXProofsValueForText = 1;
                    }

                    //var Text32FXProofs = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "Text32FXProofs").Select(x => x.Price).FirstOrDefault();
                    saddleStitchedFinalResult.textResultForSaddleStitched.FXProofs = ((FXProofsValueForText * 0));//4c-$1.00;  1c-$0.50 need clarification on this
                }
                else if (ResultForText.PPValue == 4)
                {
                    var Text4PrintingColour = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "Text4PrintingColour").Select(x => x.Price).FirstOrDefault();
                    saddleStitchedFinalResult.textResultForSaddleStitched.PrintingColour = ((Quantity / 1000) * Text4PrintingColour * (Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.ColorFront) + Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.ColorBack)) * Text4SigForSaddleStitchedText);

                    var Text4MakereadyPrintingcolour = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "Text4MakereadyPrintingcolour").Select(x => x.Price).FirstOrDefault();
                    saddleStitchedFinalResult.textResultForSaddleStitched.MakereadyPrintingColour = ((Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.ColorFront) + Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.ColorBack)) * Text4SigForSaddleStitchedText * Text4MakereadyPrintingcolour);

                    //var Text32CTPPlatesColours = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "Text32CTPPlatesColours").Select(x => x.Price).FirstOrDefault();
                    if (Quantity <= 2000)
                    {
                        var CTPPlatesColoursSmall = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "CTPPlatesColoursSmall").Select(x => x.Price).FirstOrDefault();
                        saddleStitchedFinalResult.textResultForSaddleStitched.CTPPlatesColours = (((Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.ColorFront) + Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.ColorBack)) * Text4SigForSaddleStitchedText * CTPPlatesColoursSmall) / SW);
                    }
                    else
                    {
                        var CTPPlatesColoursBig = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "CTPPlatesColoursBig").Select(x => x.Price).FirstOrDefault();
                        saddleStitchedFinalResult.textResultForSaddleStitched.CTPPlatesColours = (((Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.ColorFront) + Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.ColorBack)) * Text4SigForSaddleStitchedText * CTPPlatesColoursBig) / SW);
                    }

                    var Text4Plotter = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "Text4Plotter").Select(x => x.Price).FirstOrDefault();
                    saddleStitchedFinalResult.textResultForSaddleStitched.Plotter = ((TextSizeFoldedBookHeight * TextSizeFoldedBookWidth * saddleStitchedFinalData.saddleStitchedExtent.Text * Text4SigForSaddleStitchedText) * Text4Plotter * saddleStitchedFinalData.saddleStitchedText.Proofs); //1=yes, 0=no need to clarify on this proofs value

                    var Text4Varnish = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "Text4Varnish").Select(x => x.Price).FirstOrDefault();
                    saddleStitchedFinalResult.textResultForSaddleStitched.Varnish = ((Text4SigForSaddleStitchedText * Quantity) * (Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.Varnish)) / 1000 * Text4Varnish);

                    var Text4PrintingSpotVarnish = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "Text4PrintingSpotVarnish").Select(x => x.Price).FirstOrDefault();
                    saddleStitchedFinalResult.textResultForSaddleStitched.PrintingSpotVarnish = ((Quantity * Text4SigForSaddleStitchedText / 1000) * (Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.SpotVarnish)) * Text4PrintingSpotVarnish);

                    var Text4MakereadySpotVarnish = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "Text4MakereadySpotVarnish").Select(x => x.Price).FirstOrDefault();
                    saddleStitchedFinalResult.textResultForSaddleStitched.MakereadySpotVarnish = ((Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.SpotVarnish)) * Text4SigForSaddleStitchedText * Text4MakereadySpotVarnish);//need to clarify on spotvarnish front and back value to be taken from cover or text???

                    var Text4CTPPlatesSpotVarnish = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "Text4CTPPlatesSpotVarnish").Select(x => x.Price).FirstOrDefault();
                    saddleStitchedFinalResult.textResultForSaddleStitched.CTPPlatesSpotVarnish = ((Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.SpotVarnish)) * Text4SigForSaddleStitchedText * Text4CTPPlatesSpotVarnish);//need to clarify on spotvarnish front and back value to be taken from cover or text???

                    var Text4Folding = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "Text4Folding").Select(x => x.Price).FirstOrDefault();
                    saddleStitchedFinalResult.textResultForSaddleStitched.Folding = ((Quantity * Text4SigForSaddleStitchedText) / 1000 * Text4Folding);

                    var Text4MakereadyFolding = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "Text4MakereadyFolding").Select(x => x.Price).FirstOrDefault();
                    try
                    {
                        saddleStitchedFinalResult.textResultForSaddleStitched.MakereadyFolding = ((Text4SigForSaddleStitchedText / Text4SigForSaddleStitchedText) * Text4MakereadyFolding);
                    }
                    catch
                    {
                        saddleStitchedFinalResult.textResultForSaddleStitched.MakereadyFolding = 0;
                    }

                    var Text4PrintingColorWastage = (Quantity / TextCut) * Text4SigForSaddleStitchedText;
                    var Text4MakeReadyPrintingColorWastage = (Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.ColorFront) + Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.ColorBack)) * PrintingWastage / (TextCut / Text4UpsForSaddleStitchedText) * Text4SigForSaddleStitchedText;
                    var Text4VarnishWastage = (Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.Varnish)) * VarnishWastage / (TextCut / Text4UpsForSaddleStitchedText) * Text4SigForSaddleStitchedText;
                    var Text4PrintingSpotVarnishWastage = (Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.SpotVarnish)) * SpotVarnishWastage / (TextCut / Text4UpsForSaddleStitchedText) * Text4SigForSaddleStitchedText;
                    var Text4FoldingWastage = (FoldingWastage / TextCut) * Text4SigForSaddleStitchedText;

                    var Text4PaperTotalWastage = Text4PrintingColorWastage + Text4MakeReadyPrintingColorWastage + Text4VarnishWastage + Text4PrintingSpotVarnishWastage + Text4FoldingWastage;

                    var TextPriceReam = ((TextSizeHeight / 1000 * TextSizeWidth / 1000 * Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.GSM)) / 2 * Convert.ToDecimal(2.2046) * Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.PaperCost));
                    var TextPriceSheet = TextPriceReam / 500;

                    saddleStitchedFinalResult.textResultForSaddleStitched.PaperTotal = Text4PaperTotalWastage * TextPriceSheet;

                    decimal FXProofsValueForText = 0;
                    if ((Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.ColorFront) + Convert.ToDecimal(saddleStitchedFinalData.saddleStitchedText.ColorBack)) <= 1)
                    {
                        FXProofsValueForText = Convert.ToDecimal(0.50);
                    }
                    else
                    {
                        FXProofsValueForText = 1;
                    }

                    //var Text32FXProofs = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "Text32FXProofs").Select(x => x.Price).FirstOrDefault();
                    saddleStitchedFinalResult.textResultForSaddleStitched.FXProofs = ((FXProofsValueForText * 0));//4c-$1.00;  1c-$0.50 need clarification on this
                }



                //Binding
                var Collate = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "Collate").Select(x => x.Price).FirstOrDefault();
                saddleStitchedFinalResult.bindingForSaddleStitched.Collate = Quantity * BindingSig * Collate / 1000;


                var DrawnOnCover = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "DrawnOnCover").Select(x => x.Price).FirstOrDefault();
                saddleStitchedFinalResult.bindingForSaddleStitched.DrawOnCover = (Quantity / 1000) * DrawnOnCover;

                saddleStitchedFinalResult.bindingForSaddleStitched.SetUp = 15;

                //Packing
                var PaperWrap = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "PaperWrap").Select(x => x.Price).FirstOrDefault();
                try
                {
                    saddleStitchedFinalResult.packingForSaddleStitched.PaperWrap = (Quantity / saddleStitchedFinalData.PaperWrap) * PaperWrap;
                }
                catch
                {
                    saddleStitchedFinalResult.packingForSaddleStitched.PaperWrap = 0;
                }

                var Cartonize = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "Cartonize").Select(x => x.Price).FirstOrDefault();
                try
                {
                    saddleStitchedFinalResult.packingForSaddleStitched.Cartonize = (Quantity / saddleStitchedFinalData.Carton) * Cartonize;
                }
                catch
                {
                    saddleStitchedFinalResult.packingForSaddleStitched.Cartonize = 0;
                }

                var Pallet = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "Pallet").Select(x => x.Price).FirstOrDefault();
                try
                {
                    saddleStitchedFinalResult.packingForSaddleStitched.Pallet = ((Quantity / saddleStitchedFinalData.Carton) / saddleStitchedFinalData.Pallet) * Pallet;
                }
                catch
                {
                    saddleStitchedFinalResult.packingForSaddleStitched.Pallet = 0;
                }

                var ShrinkWrap = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "ShrinkWrap").Select(x => x.Price).FirstOrDefault();
                try
                {
                    saddleStitchedFinalResult.packingForSaddleStitched.ShrinkWrap = (Quantity / saddleStitchedFinalData.ShrinkWrap) * ShrinkWrap;
                }
                catch
                {
                    saddleStitchedFinalResult.packingForSaddleStitched.ShrinkWrap = 0;
                }

                //Delivery
                var Location = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "Location").Select(x => x.Price).FirstOrDefault();
                saddleStitchedFinalResult.deliveryForSaddleStitched.Location = Location * saddleStitchedFinalData.Delivery;

                var Courier = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "Courier").Select(x => x.Price).FirstOrDefault();
                saddleStitchedFinalResult.deliveryForSaddleStitched.Courier = Courier * saddleStitchedFinalData.Courier;

                saddleStitchedFinalResult.SubTotal = saddleStitchedFinalResult.coverResultForSaddleStitched.CoverTotal + saddleStitchedFinalResult.textResultForSaddleStitched.TextTotal + saddleStitchedFinalResult.bindingForSaddleStitched.BindingTotal + saddleStitchedFinalResult.packingForSaddleStitched.PackingTotal + saddleStitchedFinalResult.deliveryForSaddleStitched.DeliveryTotal;

                var MarkupPercentage = SaddleStitchedRates.Where(x => x.SaddleStitchedRatesName == "MarkupPercentage").Select(x => x.Price).FirstOrDefault();
                saddleStitchedFinalResult.MarkUp = ((saddleStitchedFinalResult.SubTotal * MarkupPercentage) / 100);
                saddleStitchedFinalResult.Total = saddleStitchedFinalResult.SubTotal + saddleStitchedFinalResult.MarkUp;
                saddleStitchedFinalResult.Unit = (saddleStitchedFinalResult.Total / Quantity);
                saddleStitchedFinalResult.Quoted = saddleStitchedFinalResult.Unit * Quantity;

                quantity.NoOfQuantity = Quantity;
                quantity.Price = Convert.ToDouble(saddleStitchedFinalResult.Total);
                quantities.Add(quantity);
            }

            string DestinationName = DateTime.Now.ToString("ddMMyyyyhhmmss") + ".xlsx";
            string SourceName = SalesCalcConstants.SourceName;
            string sourcePath = SalesCalcConstants.SourcePath;
            string targetPath = SalesCalcConstants.TargetPath;

            // Use Path class to manipulate file and directory paths.
            string sourceFile = System.IO.Path.Combine(sourcePath, SourceName);
            string destFile = System.IO.Path.Combine(targetPath, DestinationName);

            // To copy a folder's contents to a new location:
            // Create a new target folder.
            // If the directory already exists, this method does not create a new directory.....
            System.IO.Directory.CreateDirectory(targetPath);

            // To copy a file to another location and
            // overwrite the destination file if it already exists.
            System.IO.File.Copy(sourceFile, destFile, true);
            FileInfo file = new FileInfo(destFile);

            //create a new Excel package from the file
            using (ExcelPackage excelPackage = new ExcelPackage(file))
            {
                //create an instance of the the first sheet in the loaded file
                using (ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets["Sheet1"])
                {
                    worksheet.Cells[5, 2].Value = DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss");
                    worksheet.Cells[7, 2].Value = "SaddleStitched Data";
                    worksheet.Cells[8, 2].Value = "";
                    worksheet.Cells[12, 2].Value = "SaddleStitched Book";
                    worksheet.Cells[14, 2].Value = "Cover -" + saddleStitchedFinalData.saddleStitchedCover.FoldedSize;
                    worksheet.Cells[15, 2].Value = "Cover -" + saddleStitchedFinalData.saddleStitchedCover.OpenedSize;
                    worksheet.Cells[14, 3].Value = "Text -" + saddleStitchedFinalData.saddleStitchedText.FoldedSize;
                    worksheet.Cells[15, 3].Value = "Text -" + saddleStitchedFinalData.saddleStitchedText.OpenedSize;
                    worksheet.Cells[16, 2].Value = "Cover -" + saddleStitchedFinalData.saddleStitchedExtent.Cover;
                    worksheet.Cells[16, 3].Value = "Text -" + saddleStitchedFinalData.saddleStitchedExtent.Text;
                    worksheet.Cells[17, 2].Value = "Cover- " + saddleStitchedFinalData.saddleStitchedCover.PaperType;
                    worksheet.Cells[18, 2].Value = "Text- " + saddleStitchedFinalData.saddleStitchedText.PaperType;
                    worksheet.Cells[19, 2].Value = "Cover- " + saddleStitchedFinalData.saddleStitchedCover.ColorFront + "X" + saddleStitchedFinalData.saddleStitchedCover.ColorBack;
                    worksheet.Cells[20, 2].Value = "Text- " + saddleStitchedFinalData.saddleStitchedText.ColorFront + "X" + saddleStitchedFinalData.saddleStitchedText.ColorBack;
                    worksheet.Cells[22, 2].Value = "";
                    worksheet.Cells[23, 2].Value = "SaddleStitched";
                    worksheet.Cells[24, 2].Value = "-";

                    int n = 1;
                    foreach (var items in quantities)
                    {
                        if (n == 1)
                        {
                            worksheet.Cells[27, 2].Value = items.NoOfQuantity;
                            worksheet.Cells[28, 2].Value = items.Price;
                        }
                        if (n == 2)
                        {
                            worksheet.Cells[27, 3].Value = items.NoOfQuantity;
                            worksheet.Cells[28, 3].Value = items.Price;
                        }
                        if (n == 3)
                        {
                            worksheet.Cells[27, 4].Value = items.NoOfQuantity;
                            worksheet.Cells[28, 4].Value = items.Price;
                        }
                        n++;
                    }
                    worksheet.Cells[41, 4].Value = "";

                    //save the changes
                    excelPackage.Save();
                }

            }
            return SalesCalcConstants.TargetPathIIS + DestinationName;

        }




        //public static int GetCutForSaddleStitchedCover()
        //{
        //    return 4;
        //}
        public static int GetUpsForSaddleStitchedCover()
        {
            return 1;
        }
        public static int GetSigForSaddleStitchedCover()
        {
            return 1;
        }
        //public static int GetText32CutForSaddleStitchedText()
        //{
        //    return 1;
        //}

        //public static int GetText16CutForSaddleStitchedText()
        //{
        //    return 1;
        //}
        //public static int GetText8CutForSaddleStitchedText()
        //{
        //    return 2;
        //}
        //public static int GetText4CutForSaddleStitchedText()
        //{
        //    return 4;
        //}

        public static int GetText32UpsForSaddleStitchedText()
        {
            return 1;
        }
        public static int GetText16UpsForSaddleStitchedText()
        {
            return 1;
        }
        public static int GetText8UpsForSaddleStitchedText()
        {
            return 1;
        }
        public static int GetText4UpsForSaddleStitchedText()
        {
            return 1;
        }

        public static int GetText32SigForSaddleStitchedText()
        {
            return 1;
        }

        public static int GetText16SigForSaddleStitchedText()
        {
            return 1;
        }

        public static int GetText8SigForSaddleStitchedText()
        {
            return 1;
        }

        public static int GetText4SigForSaddleStitchedText()
        {
            return 1;
        }

        public static int GetBindingSigForSaddleStitched()
        {
            return 0; //need to clarify on this (they are adding all text sigs)
        }
    }
}
