﻿using MySql.Data.MySqlClient;
using SalesCalc.Business.Constants;
using SalesCalc.Business.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;


namespace SalesCalc.Business.Repository
{
    public class PermissionsRepository
    {

        public static List<ParentNodes> GetAllPermissions()
        {
            List<ParentNodes> parentNodes = new List<ParentNodes>();
            var nodeids = GetAllParentNodeIds();
            foreach (var item in nodeids)
            {
                ParentNodes parentNode = new ParentNodes();
                parentNode.Permissions = new List<Permissions>();

                using (MySqlConnection connection = new MySqlConnection(SalesCalcConstants.DBConnectionString))
                {
                    using (MySqlCommand command = new MySqlCommand(SalesCalcConstants.sp_GetAllPermissions, connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Connection.Open();
                        command.CommandTimeout = 180000;
                        command.Parameters.AddWithValue("@Node_ID", item);
                        MySqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            Permissions permission = new Permissions();
                            permission.PermissionId = Convert.ToInt32(reader["PermissionId"]);
                            if (reader["PermissionCode"] != System.DBNull.Value)
                            {
                                permission.Code = Convert.ToString(reader["PermissionCode"]);
                            }
                            permission.PermissionName = Convert.ToString(reader["PermissionName"]);
                            permission.PermissionDescription = Convert.ToString(reader["PermissionDescription"]);
                            permission.NodeId = Convert.ToInt32(reader["ParentNodeId"]);
                            permission.Icon = Convert.ToString(reader["Icon"]);
                            permission.Type = Convert.ToString(reader["Type"]);
                            permission.Path = Convert.ToString(reader["Path"]);
                            permission.State = Convert.ToString(reader["State"]);
                            parentNode.NodeId = Convert.ToInt32(reader["ParentNodeId"]);
                            parentNode.NodeName = Convert.ToString(reader["ParentNodeName"]);
                            parentNode.NodeIcon = Convert.ToString(reader["NodeIcon"]);
                            parentNode.Path = Convert.ToString(reader["ParentPath"]);
                            parentNode.Type = Convert.ToString(reader["Type"]);
                            parentNode.Permissions.Add(permission);

                        }
                        command.Connection.Close();
                        parentNodes.Add(parentNode);
                    }

                }
            }

            return parentNodes;
        }

        public static List<int> GetAllParentNodeIds()
        {
            List<int> data = new List<int>();

            using (MySqlConnection connection = new MySqlConnection(SalesCalcConstants.DBConnectionString))
            {
                using (MySqlCommand command = new MySqlCommand(SalesCalcConstants.sp_GetAllParentNodeIds, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Connection.Open();
                    command.CommandTimeout = 180000;
                    MySqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        data.Add(Convert.ToInt32(reader["ParentNodeId"]));
                    }
                }
            }
            return data;
        }
    }
}

