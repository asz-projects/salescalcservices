﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Microsoft.Extensions.Primitives;
using System.Configuration;
using SalesCalc.Business.Model;
using SalesCalc.Business.Constants;
using SalesCalc.Business.Common;

namespace SalesCalc.Business.Repository
{
    public class UserRepository
    {

        
        //This service is used to get all the users, and data is pulled from users table
        public static object GetAllUsers()
        {
            try
            {
                List<User> Users = new List<User>();
                using (MySqlConnection connection = new MySqlConnection(SalesCalcConstants.DBConnectionString))
                {
                    using (MySqlCommand command = new MySqlCommand(SalesCalcConstants.sp_GetAllUsers, connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Connection.Open();
                        command.CommandTimeout = 180000;
                        MySqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            User user = new User();
                            user.UserGuid = Convert.ToString(reader["UserGuid"]);
                            if (reader["UserId"] != System.DBNull.Value)
                            {
                                user.UserId = Convert.ToInt32(reader["UserId"]);
                            }

                            user.FirstName = Convert.ToString(reader["FirstName"]);
                            user.LastName = Convert.ToString(reader["LastName"]);
                            //if (reader["CityId"] != System.DBNull.Value)
                            //{
                            //    user.CityId = Convert.ToInt32(reader["CityId"]);
                            //}

                            //if (reader["CountryId"] != System.DBNull.Value)
                            //{
                            //    user.CountryId = Convert.ToInt32(reader["CountryId"]);
                            //}

                            user.CreatedBy = Convert.ToString(reader["CreatedBy"]);
                            if (reader["CreatedDate"] != System.DBNull.Value)
                            {
                                user.CreatedDate = Convert.ToDateTime(reader["CreatedDate"]);
                            }
                            user.MobileNo = Convert.ToString(reader["MobileNo"]);
                            user.Email = Convert.ToString(reader["Email"]);
                            user.RoleId = Convert.ToInt32(reader["RoleId"]);
                            user.RoleName = Convert.ToString(reader["RoleName"]);
                           // user.UserTypeCode = Convert.ToString(reader["UserTypeCode"]);
                            user.Address = Convert.ToString(reader["Address"]);
                            //user.GAgentId = Convert.ToInt32(reader["GAgentId"]);
                            //user.OperatorId = Convert.ToInt32(reader["OperatorId"]);
                            Users.Add(user);

                        }
                        command.Connection.Close();
                    }
                }
                var response = new { sucess = true, users = Users };
                return response;
            }
            catch (Exception e)
            {
                var response1 = new { sucess = false, Error = e.ToString() };
                return response1;
            }
        }

        public static object GetAllUserTypes()
        {
            try
            {
                List<UserType> userTypes = new List<UserType>();
                using (MySqlConnection connection = new MySqlConnection(SalesCalcConstants.DBConnectionString))
                {
                    using (MySqlCommand command = new MySqlCommand(SalesCalcConstants.sp_GetAllUserTypes, connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Connection.Open();
                        command.CommandTimeout = 180000;
                        MySqlDataReader reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            UserType user = new UserType();
                            user.UserTypeId=Convert.ToInt32(reader["UserTypeId"]);
                            user.UserTypeName = Convert.ToString(reader["UserTypeName"]);
                            user.UserTypeCode = Convert.ToString(reader["UserTypeCode"]);
                            userTypes.Add(user);
                        }
                        command.Connection.Close();
                    }
                }
                var response = new { sucess = true, userTypes = userTypes };
                return response;
            }
            catch (Exception e)
            {
                var response1 = new { sucess = false, Error = e.ToString() };
                return response1;
            }
        }

        //This service is used to get details of a user by userid, and data is pulled from Users table
        public static object GetUserById(string UserGuid)
        {
            try
            {
                User user = new User();
                using (MySqlConnection connection = new MySqlConnection(SalesCalcConstants.DBConnectionString))
                {
                    using (MySqlCommand command = new MySqlCommand(SalesCalcConstants.sp_GetUserById, connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Connection.Open();
                        command.CommandTimeout = 180000;
                        command.Parameters.AddWithValue("@User_Guid", UserGuid);
                        MySqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {

                            user.UserGuid = Convert.ToString(reader["UserGuid"]);
                            if (reader["UserId"] != System.DBNull.Value)
                            {
                                user.UserId = Convert.ToInt32(reader["UserId"]);
                            }

                            user.FirstName = Convert.ToString(reader["FirstName"]);
                            user.LastName = Convert.ToString(reader["LastName"]);
                            //if (reader["CityId"] != System.DBNull.Value)
                            //{
                            //    user.CityId = Convert.ToInt32(reader["CityId"]);
                            //}

                            //if (reader["CountryId"] != System.DBNull.Value)
                            //{
                            //    user.CountryId = Convert.ToInt32(reader["CountryId"]);
                            //}

                            user.CreatedBy = Convert.ToString(reader["CreatedBy"]);
                            if (reader["CreatedDate"] != System.DBNull.Value)
                            {
                                user.CreatedDate = Convert.ToDateTime(reader["CreatedDate"]);
                            }
                            user.MobileNo = Convert.ToString(reader["MobileNo"]);
                            user.Email = Convert.ToString(reader["Email"]);
                            user.RoleId = Convert.ToInt32(reader["RoleId"]);
                          //  user.UserTypeCode = Convert.ToString(reader["UserTypeCode"]);
                            user.Address = Convert.ToString(reader["Address"]);
                            //user.GAgentId = Convert.ToInt32(reader["GAgentId"]);
                            //user.OperatorId = Convert.ToInt32(reader["OperatorId"]);
                        }
                        command.Connection.Close();
                    }
                }
                var response = new { sucess = true, user = user };
                return response;
            }
            catch (Exception e)
            {
                var response1 = new { sucess = false, Error = e.ToString() };
                return response1;
            }
        }

        //This service is used to insert new user, and data in inserted in Users table
        public static object InsertUser(User user)
        {
            try
            {
                bool Issuccessfull = false;
                using (MySqlConnection connection = new MySqlConnection(SalesCalcConstants.DBConnectionString))
                {
                    using (MySqlCommand command = new MySqlCommand(SalesCalcConstants.sp_SaveUser, connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Connection.Open();
                        command.CommandTimeout = 180000;
                        command.Parameters.AddWithValue("@_Email", user.Email);
                        command.Parameters.AddWithValue("@First_Name", user.FirstName);
                        command.Parameters.AddWithValue("@Last_Name", user.LastName);
                        command.Parameters.AddWithValue("@Mobile_No", user.MobileNo);
                        command.Parameters.AddWithValue("@_Password", Cryptography.EncryptPassword(user.Password));
                        //command.Parameters.AddWithValue("@City_Id", user.CityId);
                        //command.Parameters.AddWithValue("@Country_Id", user.CountryId);
                        command.Parameters.AddWithValue("@Created_By", user.CreatedBy);
                        command.Parameters.AddWithValue("@Role_Id", user.RoleId);
                        user.UserGuid = IdUtils.generateSampleId();
                        command.Parameters.AddWithValue("@User_Guid", user.UserGuid);
                        //command.Parameters.AddWithValue("@UserType_Code", user.UserTypeCode);
                        command.Parameters.AddWithValue("@_Address", user.Address);
                        //command.Parameters.AddWithValue("@GAgent_Id", user.GAgentId);
                        //command.Parameters.AddWithValue("@Operator_Id", user.OperatorId);
                        MySqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            Issuccessfull = Convert.ToBoolean(reader["sucess"]);
                        }
                        command.Connection.Close();
                    }
                }
                var response = new { sucess = true, Issuccessfull = Issuccessfull };
                return response;
            }
            catch (Exception e)
            {
                var response1 = new { sucess = false, Error = e.ToString() };
                return response1;
            }
        }

        //This service is used to delete a user, and data is updated in users table
        public static Object DeleteUser(string UserGuid)
        {
            try
            {
                bool Issuccessfull = false;
                using (MySqlConnection connection = new MySqlConnection(SalesCalcConstants.DBConnectionString))
                {
                    using (MySqlCommand command = new MySqlCommand(SalesCalcConstants.sp_DeleteUser, connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Connection.Open();
                        command.CommandTimeout = 180000;
                        command.Parameters.AddWithValue("@User_Guid", UserGuid);
                        command.ExecuteNonQuery();
                        command.Connection.Close();
                        Issuccessfull = true;
                    }
                }
                var response = new { sucess = true, Issuccessfull = Issuccessfull };
                return response;
            }
            catch (Exception e)
            {
                var response1 = new { sucess = false, Error = e.ToString() };
                return response1;
            }
        }

        //This service is used to update the details of user by userid, and data is updated in users table
        public static object UpdateUserDetails(User user)
        {
            try
            {
                bool Issuccessfull = false;
                using (MySqlConnection connection = new MySqlConnection(SalesCalcConstants.DBConnectionString))
                {
                    using (MySqlCommand command = new MySqlCommand(SalesCalcConstants.sp_UpdateUserDetails, connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Connection.Open();
                        command.CommandTimeout = 180000;
                        command.Parameters.AddWithValue("@User_Guid", user.UserGuid);
                        command.Parameters.AddWithValue("@_Email", user.Email);
                        command.Parameters.AddWithValue("@First_Name", user.FirstName);
                        command.Parameters.AddWithValue("@Last_Name", user.LastName);
                        command.Parameters.AddWithValue("@Mobile_No", user.MobileNo);
                        //command.Parameters.AddWithValue("@City_Id", user.CityId);
                        //command.Parameters.AddWithValue("@Country_Id", user.CountryId);
                        command.Parameters.AddWithValue("@Modified_By", user.ModifiedBy);
                        command.Parameters.AddWithValue("@Role_Id", user.RoleId);
                       // command.Parameters.AddWithValue("@UserType_Code", user.UserTypeCode);
                        command.Parameters.AddWithValue("@_Address", user.Address);
                        //command.Parameters.AddWithValue("@GAgent_Id", user.GAgentId);
                        //command.Parameters.AddWithValue("@Operator_Id", user.OperatorId);
                        MySqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            Issuccessfull = Convert.ToBoolean(reader["sucess"]);
                        }
                        command.Connection.Close();
                    }
                }
                var response = new { sucess = true, Issuccessfull = Issuccessfull };
                return response;
            }
            catch (Exception e)
            {
                var response1 = new { sucess = false, Error = e.ToString() };
                return response1;
            }
        }

        public static UserLogIn AuthenticateUser(User user)
        {

            UserLogIn userlogin = new UserLogIn();
            using (MySqlConnection connection = new MySqlConnection(SalesCalcConstants.DBConnectionString))
            {
                using (MySqlCommand command = new MySqlCommand(SalesCalcConstants.sp_LogInUser, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Connection.Open();
                    command.CommandTimeout = 180000;
                    command.Parameters.AddWithValue("@_Email", user.Email);
                    command.Parameters.AddWithValue("@_Password", Cryptography.EncryptPassword(user.Password));
                    MySqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        userlogin.Success = Convert.ToBoolean(reader["sucess"]);
                        userlogin.UserGuid = Convert.ToString(reader["User_Guid"]);
                        userlogin.RoleId = Convert.ToInt32(reader["Role_Id"]);
                    }
                    command.Connection.Close();

                }
            }

            return userlogin;


        }

        public static object ChangePassword(ChangePassword data)
        {
            try
            {
                bool isSuccessFul = false;

                using (MySqlConnection connection = new MySqlConnection(SalesCalcConstants.DBConnectionString))
                {
                    using (MySqlCommand command = new MySqlCommand(SalesCalcConstants.sp_UserChangePassword, connection))
                    {

                        command.CommandType = CommandType.StoredProcedure;
                        command.Connection.Open();
                        command.CommandTimeout = 180000;
                        command.Parameters.AddWithValue("@OldPassword", Cryptography.EncryptPassword(data.OldPassword));
                        command.Parameters.AddWithValue("@NewPassword", Cryptography.EncryptPassword(data.NewPassword));
                        command.Parameters.AddWithValue("@User_Guid", data.UserGuid);
                        MySqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            isSuccessFul = (Convert.ToBoolean(reader["sucess"]));
                        }
                        command.Connection.Close();
                    }
                }

                var response = new { sucess = true, isSuccessFul = isSuccessFul };
                return response;
            }
            catch (Exception e)
            {
                var response1 = new { sucess = false, Error = e.ToString() };
                return response1;
            }
        }


        public static object ForgotPasswordMail(string EmailId)
        {
            try
            {
                bool Issuccessful = false;
                string test = string.Empty;
                User user = new User();
                using (MySqlConnection connection = new MySqlConnection(SalesCalcConstants.DBConnectionString))
                {
                    using (MySqlCommand command = new MySqlCommand(SalesCalcConstants.sp_UserForgotPassword, connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Connection.Open();
                        command.CommandTimeout = 180000;
                        command.Parameters.AddWithValue("@Email_Id", EmailId);
                        MySqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            user.FirstName = Convert.ToString(reader["FirstName"]);
                            user.LastName = Convert.ToString(reader["LastName"]);
                            user.Email = Convert.ToString(reader["Email"]);
                            user.Password = Convert.ToString(reader["Password"]);
                            user.Password = Cryptography.DecryptPassword(user.Password);
                        }
                        command.Connection.Close();
                    }
                }
                if (!string.IsNullOrEmpty(user.Email))
                {
                    string emailBody = GetEmailBodyForForgotPassword(user);
                    string[] TOEmail = new[] { user.Email };
                    //Email.InsertEmailData(TOEmail, null, null, "Zsoon Password", emailBody, null, true);
                    Email.SendEmail(TOEmail, null, null, "Your Password", emailBody, null, true);
                    Issuccessful = true;
                }
                else
                {
                    Issuccessful = false;
                }
                var response = new { sucess = true, Issuccessful = Issuccessful };
                return response;
            }
            catch (Exception e)
            {
                var response1 = new { sucess = false, Error = e.ToString() };
                return response1;
            }
        }

        public static object ForgotPasswordOTP(string EmailId)
        {
            try
            {
                bool Issuccessful = false;
                string test = string.Empty;
                string OTP = "";
                User user = new User();
                using (MySqlConnection connection = new MySqlConnection(SalesCalcConstants.DBConnectionString))
                {
                    using (MySqlCommand command = new MySqlCommand(SalesCalcConstants.sp_UserForgotPassword, connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Connection.Open();
                        command.CommandTimeout = 180000;
                        command.Parameters.AddWithValue("@Email_Id", EmailId);
                        MySqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            user.FirstName = Convert.ToString(reader["FirstName"]);
                            user.LastName = Convert.ToString(reader["LastName"]);
                            user.Email = Convert.ToString(reader["Email"]);
                            user.Password = Convert.ToString(reader["Password"]);
                            user.Password = Cryptography.DecryptPassword(user.Password);
                        }
                        command.Connection.Close();
                    }
                }
                if (!string.IsNullOrEmpty(user.Email))
                {
                    bool sucess = false;
                    Random rnd = new Random();
                    OTP = (rnd.Next(1000, 9999)).ToString();
                    if (OTP != null)
                    {
                        sucess = InsertOTP(OTP, user.Email);
                    }
                    if (sucess)
                    {
                        string emailBody = GetEmailBodyForForgotPasswordOTP(user, OTP);
                        string[] TOEmail = new[] { user.Email };
                        //Email.InsertEmailData(TOEmail, null, null, "Zsoon Password", emailBody, null, true);
                        Email.SendEmail(TOEmail, null, null, "Your OTP", emailBody, null, true);
                        Issuccessful = true;


                    }

                }
                else
                {
                    Issuccessful = false;
                }
                var response = new { sucess = true, Issuccessful = Issuccessful };
                return response;
            }
            catch (Exception e)
            {
                var response1 = new { sucess = false, Error = e.ToString() };
                return response1;
            }
        }


        public static string GetEmailBodyForForgotPassword(User user)
        {
            var emailBody = "<br/> <h3> Dear " + user.FirstName + " " + user.LastName + ",</h3> <br/>" +
 "Your Password is : <b>" + user.Password + "</b> <br/> " +
 "If you have any further questions, please contact us at <br/> " +
 "<br/> <td><img src = '' height=75px; width: 50px; alt=‘’ style=‘max-height: 80px’/> <br/> " +
 "";
            return emailBody;
        }
        public static string GetEmailBodyForForgotPasswordOTP(User user, string otp)
        {
            var emailBody = "<br/> <h3> Dear " + user.FirstName + " " + user.LastName + ",</h3> <br/>" +
 "Your OTP is : <b>" + otp + "</b> <br/> " +
 "If you have any further questions, please contact us at <br/> " +
 "<br/> <td><img src = '' height=75px; width: 50px; alt=‘’ style=‘max-height: 80px’/> <br/> " +
 "";
            return emailBody;
        }



        public static bool InsertOTP(string OTP, string Email)
        {

            bool Issuccessfull = false;
            using (MySqlConnection connection = new MySqlConnection(SalesCalcConstants.DBConnectionString))
            {
                using (MySqlCommand command = new MySqlCommand(SalesCalcConstants.sp_InsertOTP, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Connection.Open();
                    command.CommandTimeout = 180000;
                    command.Parameters.AddWithValue("@_OTP", OTP);
                    command.Parameters.AddWithValue("@_Email", Email);

                    MySqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        Issuccessfull = Convert.ToBoolean(reader["sucess"]);
                    }
                    command.Connection.Close();
                }
            }

            return Issuccessfull;

        }


        public static object CheckOTP(string Email, string OTP)
        {
            try
            {
                bool Issuccessfull = false;
                using (MySqlConnection connection = new MySqlConnection(SalesCalcConstants.DBConnectionString))
                {
                    using (MySqlCommand command = new MySqlCommand(SalesCalcConstants.sp_CheckOTP, connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Connection.Open();
                        command.CommandTimeout = 180000;
                        command.Parameters.AddWithValue("@_OTP", OTP);
                        command.Parameters.AddWithValue("@_Email", Email);

                        MySqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            Issuccessfull = Convert.ToBoolean(reader["sucess"]);
                        }
                        command.Connection.Close();
                    }
                }
                var response = new { sucess = true, Issuccessfull = Issuccessfull };
                return response;
            }
            catch (Exception e)
            {
                var response1 = new { sucess = false, Error = e.ToString() };
                return response1;
            }
        }


        public static object CreateNewPassword(string Email, string password)
        {
            try
            {
                bool Issuccessfull = false;
                using (MySqlConnection connection = new MySqlConnection(SalesCalcConstants.DBConnectionString))
                {
                    using (MySqlCommand command = new MySqlCommand(SalesCalcConstants.sp_CreateNewPassword, connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Connection.Open();
                        command.CommandTimeout = 180000;
                        command.Parameters.AddWithValue("@_password", Cryptography.EncryptPassword(password));
                        command.Parameters.AddWithValue("@_Email", Email);

                        MySqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            Issuccessfull = Convert.ToBoolean(reader["sucess"]);
                        }
                        command.Connection.Close();
                    }
                }
                var response = new { sucess = true, Issuccessfull = Issuccessfull };
                return response;
            }
            catch (Exception e)
            {
                var response1 = new { sucess = false, Error = e.ToString() };
                return response1;
            }
        }


        public static bool InsertToken(UserLogIn userLogIn)
        {
            bool Issuccessfull = false;
            using (MySqlConnection connection = new MySqlConnection(SalesCalcConstants.DBConnectionString))
            {
                using (MySqlCommand command = new MySqlCommand(SalesCalcConstants.sp_InsertToken, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Connection.Open();
                    command.CommandTimeout = 180000;
                    command.Parameters.AddWithValue("@User_Guid", userLogIn.UserGuid);
                    command.Parameters.AddWithValue("@Token_", userLogIn.Token);

                    MySqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        Issuccessfull = Convert.ToBoolean(reader["sucess"]);
                    }
                    command.Connection.Close();
                }
            }

            return Issuccessfull;


        }

        public static bool DeleteToken(StringValues Authorization, StringValues UserId)
        {
            bool isSuccessFul = false;
            string AuthorizationString = string.Empty;
            string AuthorizationData = string.Empty;
            string UserIdData = string.Empty;
            var AuthorizationArray = Authorization.ToArray();
            if (AuthorizationArray.Length != 0)
            {
                AuthorizationString = AuthorizationArray[0];
                AuthorizationData = AuthorizationString.Replace("Bearer ", "");
            }
            var UserIdArray = UserId.ToArray();
            if (UserIdArray.Length != 0)
            {
                UserIdData = UserIdArray[0];
            }
            using (MySqlConnection con = new MySqlConnection(SalesCalcConstants.DBConnectionString))
            {
                using (MySqlCommand cmd = new MySqlCommand(SalesCalcConstants.sp_DeleteToken, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection.Open();
                    cmd.CommandTimeout = 180000;
                    cmd.Parameters.AddWithValue("@Token_", AuthorizationData);
                    cmd.Parameters.AddWithValue("@User_Id", UserIdData);
                    cmd.ExecuteNonQuery();
                    cmd.Connection.Close();
                    isSuccessFul = true;
                }
            }
            return isSuccessFul;

        }


        public static object CheckPermissionsByUserGuid(string UserGuid, int permissionId)
        {
            try
            {
                bool Issuccessfull = false;
                using (MySqlConnection connection = new MySqlConnection(SalesCalcConstants.DBConnectionString))
                {
                    using (MySqlCommand command = new MySqlCommand(SalesCalcConstants.sp_CheckPermissionsByUserGuid, connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Connection.Open();
                        command.CommandTimeout = 180000;
                        command.Parameters.AddWithValue("@User_Guid", UserGuid);
                        command.Parameters.AddWithValue("@permission_Id", permissionId);

                        MySqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            Issuccessfull = Convert.ToBoolean(reader["sucess"]);
                        }
                        command.Connection.Close();
                    }
                }
                var response = new { sucess = true, PermissionGranted = Issuccessfull };
                return response;
            }
            catch (Exception e)
            {
                var response1 = new { sucess = false, Error = e.ToString() };
                return response1;
            }
        }



        
    }
}
