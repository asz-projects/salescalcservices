﻿using System;
using System.Collections.Generic;
using System.Text;
using SalesCalc.Business.AppConfig;

namespace SalesCalc.Business.Constants
{
    public static class SalesCalcConstants
    {
        public static string DBConnectionString = new AppConfiguration().ConnectionString;
        public static string SalesCalcConnection = new AppConfiguration().SalesCalcConnectionString;


        //Email
        public static string EmailId = new AppConfiguration().EmailId;
        public static string Password = new AppConfiguration().Password;

        public static string SourceName = new AppConfiguration().SourceName;
        public static string FoldingName = new AppConfiguration().FoldingName;
        public static string SourcePath = new AppConfiguration().SourcePath;
        public static string TargetPath = new AppConfiguration().TargetPath;
        public static string TargetPathIIS = new AppConfiguration().TargetPathIIS;

        //Stored Procedures Names 
        public static string sp_GetAllProducts = "sp_GetAllProducts";
        public static string sp_GetAllFoldingsData = "sp_GetAllFoldingsData";
        public static string sp_GetFoldedSizeData = "sp_GetFoldedSizeData";
        public static string sp_GetOpenedSizeData = "sp_GetOpenedSizeData";
        public static string sp_GetFoldingFinishingData = "sp_GetFoldingFinishingData";
        public static string sp_GetPaperWrappedData = "sp_GetPaperWrappedData";
        public static string sp_GetSheetSizeData = "sp_GetSheetSizeData";
        public static string sp_GetPaperData = "sp_GetPaperData"; 
        public static string sp_InsertFinalData = "sp_InsertFinalData"; 
        public static string sp_GetAllExtents = "sp_GetAllExtents"; 
        public static string sp_GetExtentMapping = "sp_GetExtentMapping"; 
        public static string sp_GetWastageDataForFolding = "sp_GetWastageDataForFolding"; 
        public static string sp_GetFoldingRates = "sp_GetFoldingRates"; 
        public static string sp_GetCaseBookRates = "sp_GetCaseBookRates"; 
        public static string sp_GetWastageDataForCaseBook = "sp_GetWastageDataForCaseBook"; 
        public static string sp_GetWastageDataForLimpBound = "sp_GetWastageDataForLimpBound"; 
        public static string sp_GetWastageDataForSaddleStitched = "sp_GetWastageDataForSaddleStitched"; 
        public static string sp_GetLimpBoundRates = "sp_GetLimpBoundRates"; 
        public static string sp_GetSaddleStitchedRates = "sp_GetSaddleStitchedRates"; 
        public static string sp_GetAllColoursData = "sp_GetAllColoursData";
        public static string sp_GetGSMPricesById = "sp_GetGSMPricesById";
     


        public static string sp_GetAllUsers = "sp_GetAllUsers";
        public static string sp_GetUserById = "sp_GetUserById";
        public static string sp_SaveUser = "sp_SaveUser";
        public static string sp_DeleteUser = "sp_DeleteUser";
        public static string sp_UpdateUserDetails = "sp_UpdateUserDetails";
        public static string sp_DeleteRole = "sp_DeleteRole";
        public static string sp_DeleteRoleMappingData = "sp_DeleteRoleMappingData";
        public static string sp_CreateRoleMappingData = "sp_CreateRoleMappingData";
        public static string sp_CreateRole = "sp_CreateRole";
        public static string sp_GetParentNodesData = "sp_GetParentNodesData";
        public static string sp_GetNodesIdsForRole = "sp_GetNodesIdsForRole";
        public static string sp_GetRoleMappingData = "sp_GetRoleMappingData";
        public static string sp_GetRoleById = "sp_GetRoleById";
        public static string sp_GetAllRoles = "sp_GetAllRoles";
        public static string sp_GetAllPermissions = "sp_GetAllPermissions";
        public static string sp_GetAllParentNodeIds = "sp_GetAllParentNodeIds";
        public static string sp_UserChangePassword = "sp_UserChangePassword";
        public static string sp_UserForgotPassword = "sp_UserForgotPassword";
        public static string sp_InsertOTP = "sp_InsertOTPUser";
        public static string sp_CheckOTP = "sp_CheckOTP";
        public static string sp_CreateNewPassword = "sp_CreateNewPassword";
        public static string sp_InsertToken = "sp_InsertToken";
        public static string sp_CheckToken = "sp_CheckToken";
        public static string sp_GetallPermissionids = "sp_GetallPermissionids";
        public static string sp_GetRolesByUserType = "sp_GetRolesByUserType";
        public static string sp_LogInUser = "sp_LogInUser";
        public static string sp_DeleteToken = "sp_DeleteToken";
        public static string sp_CheckPermissionsByUserGuid = "sp_CheckPermissionsByUserGuid";

        public static string sp_GetAllOperators = "sp_GetAllOperators";
        public static string sp_GetOperatorById = "sp_GetOperatorById";
        public static string sp_InsertOperatordata = "sp_InsertOperatordata";
        public static string sp_UpdateOperatordata = "sp_UpdateOperatordata";
        public static string sp_DeleteOperator = "sp_DeleteOperator";
        public static string sp_DeleteGAgent = "sp_DeleteGAgent";
        public static string sp_UpdateGAgent = "sp_UpdateGAgent";
        public static string sp_InsertGAgent = "sp_InsertGAgent";
        public static string sp_GetGAgentById = "sp_GetGAgentById";
        public static string sp_GetAllGAgent = "sp_GetAllGAgent";
        public static string sp_GetOperatorsByGAgentId = "sp_GetOperatorsByGAgentId";
        public static string sp_GetAllUserTypes = "sp_GetAllUserTypes";


        //Logs
        public static NLog.Logger logger = NLog.Web.NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();

        //keys
        public const string EncryptionKey1 = "0123456789SALESCALCABCDEFGHIJKLMNOPQRSTUVWXYZ";
        public const string EncryptionKey2 = "0123456789SALESCALCLAUNDRYABCDEFGHIJKLMNOPQRSTUVWXYZ";

        public const string EncryptionKey3 = "123456$#@$^@1ERF";

        public const string EncryptionKey4 = "123456^@1ERF$#@$";





    }



}

