﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesCalc.Business.Model
{
    public class UserLogIn
    {
        public bool Success { get; set; }
        public string Token { get; set; }

        public string UserId { get; set; }
        public string UserGuid { get; set; }
        public int RoleId { get; set; }

       
    }
}
