﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesCalc.Business.Model
{
    public class ChangePassword
    {
        public string UserGuid { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    }
}
