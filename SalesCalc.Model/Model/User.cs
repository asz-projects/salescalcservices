﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesCalc.Business.Model
{
    public class User
    {
        public int UserId { get; set; }
        public string UserGuid { get; set; }
        //public string AdministrationCode { get; set; }
        //public string AdministrationId { get; set; }
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string MobileNo { get; set; }
        public string Password { get; set; }
        //public int CountryId { get; set; }
        //public int CityId { get; set; }
        public string UserTypeCode { get; set; }
        public string Address { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        //public int GAgentId { get; set; }
        //public int OperatorId { get; set; }

        
    }
    public class UserType
    {
        public int UserTypeId { get; set; }
        public string UserTypeName { get; set; }
        public string UserTypeCode { get; set; }

    }
    }
