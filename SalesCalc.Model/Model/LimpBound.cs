﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesCalc.Business.Model
{
    public class LimpBound
    {
        public List<Folded> Folded { get; set; }
        public List<Opened> Opened { get; set; }
        public List<Paper> Paper { get; set; }
    }

    public class LimpBoundWastage
    {
        public int LimpBoundWastageId { get; set; }
        public string LimpBoundWastageName { get; set; }
        public string LimpBoundWastageCode { get; set; }
        public double LimpBoundWastageCost { get; set; }

    }

    public class LimpBoundRates
    {
        public int LimpBoundRatesId { get; set; }
        public string LimpBoundRatesName { get; set; }
        public decimal Price { get; set; }
        public decimal To1000 { get; set; }
        public decimal To10000 { get; set; }
        public decimal To50000 { get; set; }
        public decimal Above50000 { get; set; }
    }
}
