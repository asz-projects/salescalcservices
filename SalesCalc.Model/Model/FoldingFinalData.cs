﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesCalc.Business.Model
{
    public class FoldingFinalData
    {
        public string SalesPerson { get; set; }
        public string Estimator { get; set; }
        public string Company { get; set; }
        public string Product { get; set; }
        public string Attn { get; set; }
        public string Title { get; set; }
        public int Extent { get; set; }
        public string Proofs { get; set; }

        public string FoldedSize { get; set; }
        public string OpenedSize { get; set; }
        //public int Varnish { get; set; }
        //public int SpotVarnish { get; set; }
        public int GlossLam { get; set; }
        public int MattLam { get; set; }
        public int VelvetLam { get; set; }
        public int AntiScuffMattLam { get; set; }
        public int ThroughoutUVVarnish { get; set; }
        public int SpotUV { get; set; }
        public int OrangePeelEffect { get; set; }
        public int FoilStamp { get; set; }
        public int BlindEmboss { get; set; }
        public string DieCut { get; set; }
        public string DieCutMould { get; set; }
        public int Machine { get; set; }
        public int ScoringLine { get; set; }
        public int Hand { get; set; }
        public double PaperWrapped { get; set; }
        public double Cartonize { get; set; }
        public double Pallet { get; set; }
        public double ShrinkWrap { get; set; }
        public double PackingTotal { get { return (PaperWrapped + Cartonize + Pallet + ShrinkWrap); } }       
        public int DeliveryLocations { get; set; }
        public int Courier { get; set; }
        public int FOB { get; set; }
        public int CIF { get; set; }
        public decimal DeliveryTotal { get { return (DeliveryLocations + Courier); } }
        public bool Processed { get; set; }
        public bool PMS { get; set; }
        public bool Metalic { get; set; }
        public string ProcessColor { get; set; }
        public string PMSColor { get; set; }
        public string MetalicColor { get; set; }
        public int GlossVarnish { get; set; }
        public int MattVarnish { get; set; }
        public int SpotGlossVarnish { get; set; }
        public int SpotMattVarnish { get; set; }
        public int SpotGlossandMattVarnish { get; set; }
        public string PantoneCodeComment { get; set; }
        public string PaperType { get; set; }
        public int PaperId { get; set; }
        public string GSM { get; set; }
        public string Price { get; set; }
        public UnitCost UnitCost1 { get; set; }
        public UnitCost UnitCost2 { get; set; }
        public UnitCost UnitCost3 { get; set; }
        public string UnitCostComment1 { get; set; }
        public string UnitCostComment2 { get; set; }
        public string UnitCostComment3 { get; set; }
        public PerThousand PerThousand1 { get; set; }
        public PerThousand PerThousand2 { get; set; }
        public string PerThousandComment1 { get; set; }
        public string PerThousandComment2 { get; set; }
        public TotalPrice TotalPrice1 { get; set; }
        public TotalPrice TotalPrice2 { get; set; }
        public string TotalPriceComment1 { get; set; }
        public string TotalPriceComment2 { get; set; }
        public List<int> QuantityRequired { get; set; }
        public int AdvanceCopy { get; set; }


    }

    public class FoldingFinalResult
    {
        public double PriceByReam { get; set; }
        public double PriceBySheet { get; set; }
        public int WorkAndTurn { get; set; }
        public string SheetSize { get; set; }
        public int CutValue { get; set; }
        public int NoOfUps { get; set; }
        public double ActualPaperReq { get; set; }
        public double Printing { get; set; }
        public double Varnish { get; set; }
        public double SpotVarnish { get; set; }
        public double GlossLam { get; set; }
        public double MattLam { get; set; }
        public double VelvetLam { get; set; }
        public double AntiScuffMattLam { get; set; }
        public double UVVarnish { get; set; }
        public double SpotUV { get; set; }
        public double OrangePeelEffect { get; set; }
        public double FoilStamp { get; set; }
        public double BlindEmboss { get; set; }
        public double Folding { get; set; }
        public double TotalPaper { get; set; }
        public double PaperRate { get; set; }
        public List<Prepress> prepress { get; set; }
        public List<Press> press { get; set; }
        public List<PostPress> postPress { get; set; }
        public PackingForFolding PackingForFolding { get; set; }
        public DeliveryForFolding DeliveryForFolding { get; set; }
        public double PackingCharges { get; set; }
        public double DeliveryCharges { get; set; }
        public double Price { get; set; }
        public double UnitCost1 { get; set; }
        public double UnitCost2 { get; set; }
        public double UnitCost3 { get; set; }
        public double PerThousand1 { get; set; }
        public double PerThousand2 { get; set; }
        public double Markup { get; set; }
        public double TotalPrice { get; set; }
        public double QutotedPrice { get; set; }
        public double UnitPrice { get; set; }

    }

    public class Prepress
    {
        public double Plotter { get; set; }
        public double CTPPlates { get; set; }
    }

    public class Press
    {
        public double PrintingMakeready { get; set; }
        public double PrintingProcess { get; set; }
        public double PrintingPMS { get; set; }
        public double PrintingMetalic { get; set; }
        public double VarnishMakeready { get; set; }
        public double VarnishProcess { get; set; }
        public double SpotVarnishMakeready { get; set; }
        public double SpotVarnishProcess { get; set; }
    }

    public class PostPress
    {
        public double GlossLaminate { get; set; }
        public double MattLaminate { get; set; }
        public double VelvetLaminate { get; set; }
        public double AntiScuffMattLaminate { get; set; }
        public double UVVarnish { get; set; }
        public double OrangePeelEffect { get; set; }
        public double FoilStamp { get; set; }
        public double BlindEmboss { get; set; }
        public double DieCut { get; set; }
        public double FoldingMakeready { get; set; }
        public double FoldingProcess { get; set; }
    }
    public class PackingForFolding
    {
        public double PaperWrap { get; set; }
        public double Cartonize { get; set; }
        public double Pallet { get; set; }
        public double ShrinkWrap { get; set; }
        public double PackingTotal { get { return (PaperWrap + Cartonize + Pallet + ShrinkWrap); } }
    }

    public class DeliveryForFolding
    {
        public double Location { get; set; }
        public double Courier { get; set; }
        public double DeliveryTotal { get { return (Location + Courier); } }
    }
    public class FoldingRates
    {
        public int FoldingRatesId { get; set; }
        public string FoldingRatesName { get; set; }
        public double To1000 { get; set; }
        public double To10000 { get; set; }
        public double To50000 { get; set; }
        public double Above50000 { get; set; }
        public double Price { get; set; }
    }

    public class Quantity
    {
        public int NoOfQuantity { get; set; }
        public double Price { get; set; }
    }

    public class UnitCost
    {
        public double UnitCost1 { get; set; }
        public double UnitCost2 { get; set; }
        public double UnitCost3 { get; set; }
    }

    public class PerThousand
    {
        public double PerThousand1 { get; set; }
        public double PerThousand2 { get; set; }
        public double PerThousand3 { get; set; }
    }

    public class TotalPrice
    {
        public double TotalPrice1 { get; set; }
        public double TotalPrice2 { get; set; }
        public double TotalPrice3 { get; set; }
    }
}
