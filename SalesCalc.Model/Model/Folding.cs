﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesCalc.Business.Model
{
    public class Folding
    {
        public List<Folded> Folded { get; set; }
        public List<Opened> Opened { get; set; }
        public List<Finishing> Finishing { get; set; }
        public List<PaperWrapped> PaperWrapped { get; set; }
        public List<SheetSize> SheetSize { get; set; }
        public List<Paper> Paper { get; set; }
        public List<Colours> colours { get; set; }
    }

    public class Folded
    {
        public int FoldedSizeId { get; set; }
        public string FoldedSize { get; set; }
        public string FoldedHeight { get; set; }
        public string FoldedWidth { get; set; }

    }

    public class Opened
    {
        public int OpenedSizeId { get; set; }
        public string OpenedSize { get; set; }
        public string OpenedHeight { get; set; }
        public string OpenedWidth { get; set; }
    }

    public class Finishing
    {
        public int FoldingFinishingId { get; set; }
        public string FinishingType { get; set; }
    }

    public class PaperWrapped
    {
        public int PaperWrappedId { get; set; }
        public string PaperWrappedQuantity { get; set; }
    }

    public class SheetSize
    {
        public int SheetSizeId { get; set; }
        public string Size { get; set; }
        public string SheetHeight { get; set; }
        public string SheetWidth { get; set; }
    }

    public class Paper
    {
        public int PaperId { get; set; }
        public string PaperType { get; set; }
        public List<GSMPrice> GSM { get; set; }
        public string Price { get; set; }
        public string PaperHeight { get; set; }
        public string PaperWidth { get; set; }
        public string Validity { get; set; }
    }

    public class FoldingWastage
    {
        public int FoldingWastageId { get; set; }
        public string FoldingWastageName { get; set; }
        public string FoldingWastageCode { get; set; }
        public double FoldingWastageCost { get; set; }

    }

    public class Colours
    {
        public int ColourId { get; set; }
        public string ColourTypeName { get; set; }
        public string ColourTypeCode { get; set; }
        public double ColourRates { get; set; }

    }
    public class GSMPrice
    {
       
        public string GSM { get; set; }
        public string Price { get; set; }
       
    }
}
