﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesCalc.Business.Model
{
    public class LimpBoundFinalData
    {
        public LimpBoundExtent limpBoundExtent { get; set; }
        public LimpBoundCover limpBoundCover { get; set; }
        public LimpBoundText limpBoundText { get; set; }
        public decimal PaperWrap { get; set; }
        public decimal Carton { get; set; }
        public decimal Pallet { get; set; }
        public decimal ShrinkWrap { get; set; }
        public decimal Fumigation { get; set; }
        public int Delivery { get; set; }
        public decimal Courier { get; set; }
        public List<int> QuantityRequired { get; set; }
    }

    public class LimpBoundExtent
    {
        public int Cover { get; set; }
        public int Text { get; set; }
        public int Flap { get; set; }
        public int Spine { get; set; }
    }

    public class LimpBoundCover
    {
        public string CoverSize { get; set; }
        public string GSM { get; set; }
        public string PaperType { get; set; }
        public string PaperCost { get; set; }
        public string FoldedSize { get; set; }
        public string OpenedSize { get; set; }
        public string ColorFront { get; set; }
        public string ColorBack { get; set; }
        //public string VarnishFront { get; set; }
        //public string VarnishBack { get; set; }
        //public string SpotVarnishFront { get; set; }
        //public string SpotVarnishBack { get; set; }

        public int Varnish { get; set; }
        public int SpotVarnish { get; set; }
        public int LaminateGloss { get; set; }
        public int LaminateMatt { get; set; }
        public int UVVarnish { get; set; }
        public int Extent { get; set; }
        public string SpotUV { get; set; }
        public string FoilStamp { get; set; }
        public string BlindEmboss { get; set; }
        public int Proofs { get; set; }
        public string SpotUVVarnish { get; set; }
        public string FoilStampProcess { get; set; }
        public string BlindEmbossProcess { get; set; }
    }


    public class LimpBoundText
    {
        public string TextSize { get; set; }
        public string GSM { get; set; }
        public string PaperType { get; set; }
        public string PaperCost { get; set; }
        public string FoldedSize { get; set; }
        public string OpenedSize { get; set; }
        public string ColorFront { get; set; }
        public string ColorBack { get; set; }
        //public string VarnishFront { get; set; }
        //public string VarnishBack { get; set; }
        //public string SpotVarnishFront { get; set; }
        //public string SpotVarnishBack { get; set; }

        public int Varnish { get; set; }
        public int SpotVarnish { get; set; }
        public int Proofs { get; set; }

    }

    public class LimpBoundFinalResult
    {
        public CoverResultForLimpbound coverResultForLimpbound { get; set; }
        public TextResultForLimpBound textResultForLimpBound { get; set; }
        public BindingForLimpBound bindingForLimpBound { get; set; }
        public PackingForLimpBound packingForLimpBound { get; set; }
        public DeliveryForLimpBound deliveryForLimpBound { get; set; }
        public decimal SubTotal { get; set; }
        public decimal MarkUp { get; set; }
        public decimal Total { get; set; }
        public decimal Quoted { get; set; }
        public decimal Unit { get; set; }
    }
    public class CoverResultForLimpbound
    {
        public decimal PrintingColour { get; set; }
        public decimal MakereadyPrintingColour { get; set; }
        public decimal CTPPlatesColours { get; set; }
        public decimal Plotter { get; set; }
        public decimal Varnish { get; set; }
        public decimal PrintingSpotVarnish { get; set; }
        public decimal MakereadySpotVarnish { get; set; }
        public decimal CTPPlatesSpotVarnish { get; set; }
        public decimal LaminateGloss { get; set; }
        public decimal LaminateMatt { get; set; }
        public decimal UVVarnish { get; set; }
        public decimal SpotUVVarnish { get; set; }
        public decimal FoilStampProcess { get; set; }
        public decimal FoilStampBlock { get; set; }
        public decimal BlindEmbossProcess { get; set; }
        public decimal BlindEmbossBlock { get; set; }
        public decimal PaperTotal { get; set; }
        public decimal FXProofs { get; set; }
        public decimal ScoreLine { get; set; }
        public decimal FoldAndPasteFlaps { get; set; }
        public decimal CoverTotal { get { return (PrintingColour + MakereadyPrintingColour + CTPPlatesColours + Plotter + Varnish + PrintingSpotVarnish + MakereadySpotVarnish + CTPPlatesSpotVarnish + LaminateGloss + LaminateMatt + UVVarnish + SpotUVVarnish + FoilStampProcess + FoilStampBlock + BlindEmbossProcess + BlindEmbossBlock + PaperTotal + FXProofs + ScoreLine + FoldAndPasteFlaps); } }
    }


    public class TextResultForLimpBound
    {
        public decimal PrintingColour { get; set; }
        public decimal MakereadyPrintingColour { get; set; }
        public decimal CTPPlatesColours { get; set; }
        public decimal Plotter { get; set; }
        public decimal Varnish { get; set; }
        public decimal PrintingSpotVarnish { get; set; }
        public decimal MakereadySpotVarnish { get; set; }
        public decimal CTPPlatesSpotVarnish { get; set; }
        public decimal Folding { get; set; }
        public decimal MakereadyFolding { get; set; }
        public decimal PaperTotal { get; set; }
        public decimal FXProofs { get; set; }
        public decimal TextTotal { get { return (PrintingColour + MakereadyPrintingColour + CTPPlatesColours + Plotter + Varnish + PrintingSpotVarnish + MakereadySpotVarnish + CTPPlatesSpotVarnish + Folding + MakereadyFolding + PaperTotal + FXProofs); } }
    }

    public class BindingForLimpBound
    {
        public decimal Collate { get; set; }
        public decimal Sewing { get; set; }
        public decimal MakeReadySewing { get; set; }
        public decimal DrawOnCover { get; set; }
        public decimal SetUpPerfectBlind { get; set; }
        public decimal BindingTotal { get { return ( Collate + Sewing + MakeReadySewing + DrawOnCover + SetUpPerfectBlind); } }
    }
    public class PackingForLimpBound
    {
        public decimal PaperWrap { get; set; }
        public decimal Cartonize { get; set; }
        public decimal Pallet { get; set; }
        public decimal ShrinkWrap { get; set; }
        public decimal PackingTotal { get { return (PaperWrap + Cartonize + Pallet + ShrinkWrap ); } }
    }

    public class DeliveryForLimpBound
    {
        public decimal Location { get; set; }
        public decimal Courier { get; set; }
        public decimal DeliveryTotal { get { return (Location + Courier); } }
    }
}
