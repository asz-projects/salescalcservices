﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesCalc.Business.Model
{
    public class SaddleStitched
    {
        public List<Folded> Folded { get; set; }
        public List<Opened> Opened { get; set; }
        public List<Paper> Paper { get; set; }
    }

    public class SaddleStitchedWastage
    {
        public int SaddleStitchedWastageId { get; set; }
        public string SaddleStitchedWastageName { get; set; }
        public string SaddleStitchedWastageCode { get; set; }
        public double SaddleStitchedWastageCost { get; set; }

    }

    public class SaddleStitchedRates
    {
        public int SaddleStitchedRatesId { get; set; }
        public string SaddleStitchedRatesName { get; set; }
        public decimal Price { get; set; }
        public decimal To1000 { get; set; }
        public decimal To10000 { get; set; }
        public decimal To50000 { get; set; }
        public decimal Above50000 { get; set; }
    }
}
