﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesCalc.Business.Model
{
    public class ParentNodes
    {
        public int NodeId { get; set; }

        public int ArrangeNodes { get; set; }
        public string Path { get; set; }
        public string Type { get; set; }

        public bool IsChecked { get; set; }
        public string NodeName { get; set; }

        public string NodeIcon { get; set; }

        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public List<Permissions> Permissions { get; set; }
    }
}
