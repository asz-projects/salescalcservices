﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesCalc.Business.Model
{
    public class Products
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string ProductCode { get; set; }
    }
}
