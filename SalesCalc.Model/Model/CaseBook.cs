﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesCalc.Business.Model
{
    public class CaseBook
    {
        public List<Folded> Folded { get; set; }
        public List<Opened> Opened { get; set; }
        public List<Extent> extents { get; set; }
        public List<Paper> Paper { get; set; }
        public List<ExtentMapping> extentMappings { get; set; }
    }

    public class Extent
    {
        public int ExtentId { get; set; }
        public string ExtentName { get; set; }
        public string ExtentCode { get; set; }
    }

    public class ExtentMapping
    {
        public int ExtentId { get; set; }
        public string ExtentName { get; set; }
        public List<CaseBookingFinishing> caseBookingFinishings { get; set; }
    }

    public class CaseBookingFinishing
    {
        public string CaseBookFinishingName { get; set; }
    }

    public class CaseBookWastage
    {
        public int CaseBookWastageId { get; set; }
        public string CaseBookWastageName { get; set; }
        public string CaseBookWastageCode { get; set; }
        public double CaseBookWastageCost { get; set; }

    }

    public class CaseBookRates
    {
        public int CaseBookRateId { get; set; }
        public string CaseBookRatesName { get; set; }
        public decimal Price { get; set; }
        public decimal To1000 { get; set; }
        public decimal To10000 { get; set; }
        public decimal To50000 { get; set; }
        public decimal Above50000 { get; set; }
    }
}
