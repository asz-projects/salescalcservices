﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesCalc.Business.Model
{
    public class Permissions
    {
        public int PermissionId { get; set; }


        public string Code { get; set; }
        public string Path { get; set; }

        public string PermissionName { get; set; }

        public string PermissionDescription { get; set; }

        public int NodeId { get; set; }

        public string State { get; set; }

        public string Type { get; set; }

        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public string Icon { get; set; }
        public bool IsChecked { get; set; }
    }
}
