﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesCalc.Business.Model
{
    public class Role
    {

        public int RoleId { get; set; }

        public string RoleDescription { get; set; }

        public string RoleCode { get; set; }
        public string RoleName { get; set; }

        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public bool IsDeleted { get; set; }

        public List<RoleMapping> RoleMappings { get; set; }

        public List<ParentNodes> ParentNodes { get; set; }
        public string  UserTypeCode { get; set; }
    }
   
}
