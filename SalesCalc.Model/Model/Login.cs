﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesCalc.Business.Model
{ 
    public class Login
    {
        public bool Success { get; set; }
        public string Token { get; set; }

        public string CustomerId { get; set; }
        public string CustomerGuid { get; set; }
        public string PasswordAttempts { get; set; }
    }
}
