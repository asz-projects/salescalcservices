﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesCalc.Business.Model
{
    public class CommonCalculation
    {
        public int WorkAndTurn { get; set; }
        public string SheetSize { get; set; }
        public int Cut { get; set; }
        public int Ups { get; set; }
        public int PPValue { get; set; }
    }
}
