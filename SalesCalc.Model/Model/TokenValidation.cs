﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesCalc.Business.Model
{
    public class TokenValidation
    {
        public bool Success { get; set; }
        public string UserId { get; set; }
    }
}
