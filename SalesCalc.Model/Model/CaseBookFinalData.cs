﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SalesCalc.Business.Model
{
    public class CaseBookFinalData
    {
        public string FoldedSize { get; set; }
        public string OpenedSize { get; set; }
        public string SpineWidth { get; set; }
        public string JacketFlap { get; set; }
        public bool IsCaseWrap { get; set; }
        public bool IsEndPaper { get; set; }
        public bool IsChipBoardCover { get; set; }
        public bool IsJacketNormal { get; set; }
        public bool IsJacketFrench { get; set; }
        public bool IsText { get; set; }
        public bool IsSlipCase { get; set; }
        public bool IsChipBoardSlip { get; set; }
        public CaseWrap caseWrap { get; set; }
        public EndPaper endPaper { get; set; }
        public ChipBoardCover chipBoardCover { get; set; }
        public JacketNormal jacketNormal { get; set; }
        public JacketFrench jacketFrench { get; set; }
        public JacketData jacketData { get; set; }
        public Text text { get; set; }
        public SlipCase slipCase { get; set; }
        public ChipBoardSlip chipBoardSlip { get; set; }
        public List<int> QuantityRequired { get; set; }
        public PackingAndDeliveryForCaseBook packingAndDeliveryForCaseBook { get; set; }
        public BindingForCaseBook bindingForCaseBook { get; set; }

    }

    public class CaseWrap
    {
        public string PPValue { get; set; }
        public string ColorFront { get; set; }
        public string ColorBack { get; set; }
        public string GSM { get; set; }
        public string PaperType { get; set; }
        public string PaperSize { get; set; }
        public string PaperCost { get; set; }
        public int Varnish { get; set; }
        public int SpotVarnish { get; set; }
        public int GlossLam { get; set; }
        public int MattLam { get; set; }
        public int UVVarnish { get; set; }
        public int SpotUV { get; set; }
        public int FoilStamp { get; set; }
        public int BlindEmboss { get; set; }
        public string SpotUVVarnish { get; set; } 
        public string FoilStampProcess { get; set; }
        public string BlindEmbossProcess { get; set; } 
        public string UnitPrice { get; set; } 
        public int Proofs { get; set; }
    }

    public class EndPaper
    {
        public string PPValue { get; set; }
        public string ColorFront { get; set; }
        public string ColorBack { get; set; }
        public string GSM { get; set; }
        public string PaperType { get; set; }
        public string PaperSize { get; set; }
        public string PaperCost { get; set; }
        public int Varnish { get; set; }
        public int SpotVarnish { get; set; }
        public string UnitPrice { get; set; } 

        public int Proofs { get; set; }
    }
    public class ChipBoardCover
    {
        public string PPValue { get; set; }
        public string ColorFront { get; set; }
        public string ColorBack { get; set; }
        public string GSM { get; set; }
        public string PaperType { get; set; }
        public string PaperSize { get; set; }
        public string PaperCost { get; set; }

    }

    public class JacketNormal
    {
        public string PPValue { get; set; }
        public string ColorFront { get; set; }
        public string ColorBack { get; set; }
        public string GSM { get; set; }
        public string PaperType { get; set; }
        public string PaperSize { get; set; }
        public string PaperCost { get; set; }
        public int SpotUV { get; set; }
        public int GlossLam { get; set; }
        public int MattLam { get; set; }
        public int UVVarnish { get; set; }
        public int FoilStamp { get; set; }
        public int BlindEmboss { get; set; }
    }

    public class JacketFrench
    {
        public string PPValue { get; set; }
        public string ColorFront { get; set; }
        public string ColorBack { get; set; }
        public string GSM { get; set; }
        public string PaperType { get; set; }
        public string PaperSize { get; set; }
        public string PaperCost { get; set; }
        public int SpotUV { get; set; }
        public int GlossLam { get; set; }
        public int MattLam { get; set; }
        public int UVVarnish { get; set; }
        public int FoilStamp { get; set; }
    }

    public class JacketData
    {
        public string SpotUVVarnish { get; set; } 
        public string FoilStampProcess { get; set; } 
        public string BlindEmbossProcess { get; set; } 
        public int Proofs { get; set; }
        public string UnitPrice { get; set; } 
    }

    public class Text
    {
        public string PPValue { get; set; }
        public string ColorFront { get; set; }
        public string ColorBack { get; set; }
        public string GSM { get; set; }
        public string PaperType { get; set; }
        public string PaperSize { get; set; }
        public string PaperCost { get; set; }
        public int Varnish { get; set; }
        public int SpotVarnish { get; set; }
        public int Proofs { get; set; }
    }

    public class SlipCase
    {
        public string PPValue { get; set; }
        public string ColorFront { get; set; }
        public string ColorBack { get; set; }
        public string GSM { get; set; }
        public string PaperType { get; set; }
        public string PaperSize { get; set; }
        public string PaperCost { get; set; }

    }

    public class ChipBoardSlip
    {
        public string PPValue { get; set; }
        public string ColorFront { get; set; }
        public string ColorBack { get; set; }
        public string GSM { get; set; }
        public string PaperType { get; set; }
        public string PaperSize { get; set; }
        public string PaperCost { get; set; }

    }

    public class PackingAndDeliveryForCaseBook
    {
        public int PaperWrapIn { get; set; } 
        public int CartonizeIn { get; set; }
        public int Pallet { get; set; } 
        public int ShrinkWrap { get; set; } 
        public int Courier { get; set; }
        public int Location { get; set; }
        public string UnitPrice { get; set; } 
    }

    public class BindingForCaseBook
    {
        public string UnitPrice { get; set; } 
        public int Proofs { get; set; }
    }

    public class CaseBookFinalResult
    {
        public CaseWrapResult caseWrapResult { get; set; }
        public EndPaperResult endPaperResult { get; set; }
        public ArlinResult arlinResult { get; set; }
        public GreyChipboardResult greyChipboardResult { get; set; }
        public JacketResult jacketResult { get; set; }
        public TextResult textResult { get; set; }
        public Binding binding { get; set; }
        public PackingAndDelivery packingAndDelivery { get; set; }
        public decimal SubTotal { get; set; }
        public decimal MarkUp { get; set; }
        public decimal Total { get; set; }
        public decimal Quoted { get; set; }
        public decimal Unit { get; set; }
    }

    public class CaseWrapResult
    {
        public decimal PrintingColour { get; set; }
        public decimal MakereadyPrintingColour { get; set; }
        public decimal CTPPlatesColours { get; set; }
        public decimal Plotter { get; set; }
        public decimal Varnish { get; set; }
        public decimal PrintingSpotVarnish { get; set; }
        public decimal MakereadySpotVarnish { get; set; }
        public decimal CTPPlatesSpotVarnish { get; set; }
        public decimal LaminateGloss { get; set; }
        public decimal LaminateMatt { get; set; }
        public decimal UVVarnish { get; set; }
        public decimal SpotUVVarnish { get; set; }
        public decimal FoilStampProcess { get; set; }
        public decimal FoilStampBlock { get; set; } 
        public decimal BlindEmbossProcess { get; set; }
        public decimal BlindEmbossBlock { get; set; }
        public decimal PaperTotal { get; set; }
        public decimal UnitPrice { get; set; } 
        public decimal CaseWrapTotal { get { return (PrintingColour+ MakereadyPrintingColour+ CTPPlatesColours+ Plotter+ Varnish+ PrintingSpotVarnish+ MakereadySpotVarnish+ CTPPlatesSpotVarnish+ LaminateGloss+ LaminateMatt+ UVVarnish+ SpotUVVarnish+ FoilStampProcess+ FoilStampBlock+ BlindEmbossProcess+ BlindEmbossBlock+ PaperTotal+ UnitPrice); } }
    }
    public class EndPaperResult 
    {
        public decimal PrintingColour { get; set; }
        public decimal MakereadyPrintingColour { get; set; }
        public decimal CTPPlatesColours { get; set; }
        public decimal Plotter { get; set; }
        public decimal Varnish { get; set; }
        public decimal PrintingSpotVarnish { get; set; }
        public decimal MakereadySpotVarnish { get; set; }
        public decimal CTPPlatesSpotVarnish { get; set; }
        public decimal Folding { get; set; }
        public decimal MakereadyFolding { get; set; }
        public decimal PaperTotal { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal EndPaperTotal { get { return (PrintingColour+ MakereadyPrintingColour+ CTPPlatesColours+ Plotter+ Varnish+ PrintingSpotVarnish+ MakereadySpotVarnish+ CTPPlatesSpotVarnish+ Folding+ MakereadyFolding+ PaperTotal+ UnitPrice); } }
    }
    public class ArlinResult 
    {
        public decimal BlindEmbossProcess { get; set; }
        public decimal BlindEmbossBlock { get; set; }
        public decimal ArlinTotal { get { return (BlindEmbossProcess+ BlindEmbossBlock); } }
    }

    public class GreyChipboardResult 
    {
        public decimal GreyChipboardTotal { get; set; }
    }
    public class JacketResult 
    {
        public decimal PrintingColour { get; set; }
        public decimal MakereadyPrintingColour { get; set; }
        public decimal CTPPlatesColours { get; set; }
        public decimal Plotter { get; set; }
        public decimal NormalLaminateGloss { get; set; }
        public decimal NormalLaminateMatt { get; set; }
        public decimal NormalUVVarnish { get; set; }
        public decimal FrenchLaminateGloss { get; set; }
        public decimal FrenchLaminateMatt { get; set; }
        public decimal FrenchUVVarnish { get; set; }
        public decimal SpotUVVarnish { get; set; }
        public decimal FoilStampProcess { get; set; }
        public decimal FoilStampBlock { get; set; }
        public decimal BlindEmbossProcess { get; set; }
        public decimal BlindEmbossBlock { get; set; }
        public decimal Folding { get; set; }
        public decimal Jacketing { get; set; }
        public decimal MakereadyFolding { get; set; }
        public decimal PaperTotal { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal JacketTotal { get { return (PrintingColour+ MakereadyPrintingColour+ CTPPlatesColours+ Plotter+ NormalLaminateGloss+ NormalLaminateMatt+ NormalUVVarnish+ FrenchLaminateGloss+ FrenchLaminateMatt+ FrenchUVVarnish+ SpotUVVarnish+ FoilStampProcess+ FoilStampBlock+ BlindEmbossProcess+ BlindEmbossBlock+ Folding+ Jacketing+ MakereadyFolding+ PaperTotal+ UnitPrice); } }
    }
    public class TextResult 
    {
        public decimal PrintingColour { get; set; }
        public decimal MakereadyPrintingColour { get; set; }
        public decimal CTPPlatesColours { get; set; }
        public decimal Plotter { get; set; }
        public decimal Varnish { get; set; }
        public decimal PrintingSpotVarnish { get; set; }
        public decimal MakereadySpotVarnish { get; set; }
        public decimal CTPPlatesSpotVarnish { get; set; }
        public decimal Folding { get; set; }
        public decimal MakereadyFolding { get; set; }
        public decimal PaperTotal { get; set; }
        public decimal FXProofs { get; set; }
        public decimal TextTotal { get { return (PrintingColour+ MakereadyPrintingColour+ CTPPlatesColours+ Plotter+ Varnish+ PrintingSpotVarnish+ MakereadySpotVarnish+ CTPPlatesSpotVarnish+ Folding+ MakereadyFolding+ PaperTotal+ FXProofs); } }
    }
    public class Binding 
    {
        public decimal CaseMake { get; set; }
        public decimal EndPapering { get; set; }
        public decimal Collate { get; set; }
        public decimal Sewing { get; set; }
        public decimal HeadAndTailBand { get; set; }
        public decimal CaseIn { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal BindingTotal { get { return (CaseMake + EndPapering + Collate + Sewing + HeadAndTailBand + CaseIn + UnitPrice); } }
    }
    public class PackingAndDelivery 
    {
        public decimal PaperWrapIn { get; set; }
        public decimal CartonizeIn { get; set; }
        public decimal Pallet { get; set; }
        public decimal ShrinkWrap { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal Location { get; set; }
        public decimal Courier { get; set; }
        public decimal PackingDeliveryTotal { get { return (PaperWrapIn+ CartonizeIn+ Pallet+ ShrinkWrap+ UnitPrice+ Location+ Courier); } }
    }
}
