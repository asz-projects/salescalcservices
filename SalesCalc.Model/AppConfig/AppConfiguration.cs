﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SalesCalc.Business.AppConfig
{
    public class AppConfiguration
    {
        public readonly string _connectionString = string.Empty;
        public readonly string _SalesCalcConnectionString = string.Empty;

        //Email
        public readonly string _EmailId = string.Empty;
        public readonly string _Password = string.Empty;

        public readonly string _SourceName = string.Empty;
        public readonly string _FoldingName = string.Empty;
        public readonly string _SourcePath = string.Empty;
        public readonly string _TargetPath = string.Empty;
        public readonly string _TargetPathIIS = string.Empty;



        public AppConfiguration()
        {
            var configurationBuilder = new ConfigurationBuilder();
            var path = Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json");
            configurationBuilder.AddJsonFile(path, false);

            var root = configurationBuilder.Build();
            _connectionString = root.GetSection("ConnectionStrings").GetSection("DefaultConnection").Value;
            _SalesCalcConnectionString = root.GetSection("ConnectionStrings").GetSection("SalesCalcConnection").Value;

            _EmailId = root.GetSection("Email").GetSection("EmailId").Value;
            _Password = root.GetSection("Email").GetSection("Password").Value;
            _SourceName = root.GetSection("ExcelLocalPath").GetSection("SourceName").Value;
            _FoldingName = root.GetSection("ExcelLocalPath").GetSection("FoldingName").Value;
            _SourcePath = root.GetSection("ExcelLocalPath").GetSection("SourcePath").Value;
            _TargetPath = root.GetSection("ExcelLocalPath").GetSection("TargetPath").Value;
            _TargetPathIIS = root.GetSection("ExcelIISPath").GetSection("TargetPathIIS").Value;

        }
        public string ConnectionString
        {
            get => _connectionString;
        }
        public string SalesCalcConnectionString
        {
            get => _SalesCalcConnectionString;
        }


        //Email
        public string EmailId
        {
            get => _EmailId;
        }

        public string Password
        {
            get => _Password;
        }

        public string SourceName
        {
            get => _SourceName;
        }
        public string FoldingName
        {
            get => _FoldingName;
        }

        public string SourcePath
        {
            get => _SourcePath;
        }
        public string TargetPath
        {
            get => _TargetPath;
        }

        public string TargetPathIIS
        {
            get => _TargetPathIIS;
        }

    }
}

