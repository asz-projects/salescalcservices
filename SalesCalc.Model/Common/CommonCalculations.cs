﻿using OfficeOpenXml.FormulaParsing.Excel.Functions.Information;
using SalesCalc.Business.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SalesCalc.Business.Common
{
    public class CommonCalculations
    {
        public static CommonCalculation GetCommonCalculation(string OpenedSize, string ClosedSize)
        {
            CommonCalculation commonCalculation = new CommonCalculation();
            var CCData = CalculateSheetSizeAndCut(OpenedSize);
            //commonCalculation.WorkAndTurn = 2;
            commonCalculation.Cut = Convert.ToInt32(CCData.PPValue/2);
            commonCalculation.PPValue = Convert.ToInt32(CCData.PPValue);
            commonCalculation.Ups = 2;
            commonCalculation.SheetSize = CCData.SheetSize;
            return commonCalculation;
        }

        public static CCData CalculateSheetSizeAndCut(string OpenedSize)
        {
            double OpenHeig = Convert.ToDouble(OpenedSize.Split('X').FirstOrDefault());
            double OpenWid = Convert.ToDouble(OpenedSize.Split('X').LastOrDefault());
            int OpenHeight = Convert.ToInt32(OpenHeig);
            int OpenWidth = Convert.ToInt32(OpenWid);
            int Gripper = 12;
            int Allowance = 6;
            decimal Ratio1 = 0;
            decimal Ratio2 = 0;
            decimal Ratio3 = 0;
            decimal Ratio4 = 0;
            decimal Ratio5 = 0;
            decimal Ratio6 = 0;
            List<Ratio> ratios = new List<Ratio>();
            int TrimPaperDep = 0;
            int TrimPaperWid = 0;
            int FinalN1 = 0;
            int FinalN2 = 0;
            int FinalN3 = 0;
            int FinalN4 = 0;
            int FinalN5 = 0;
            int FinalN6 = 0;

            int FinalM1 = 0;
            int FinalM2 = 0;
            int FinalM3 = 0;
            int FinalM4 = 0;
            int FinalM5 = 0;
            int FinalM6 = 0;

            int FinalDep = 0;
            int FinalWid = 0;

            CCData cCData = new CCData();
            int i;
            for (i = 1; i <= 6; i++)
            {
                Ratio ratio = new Ratio();
                if (i == 1 || i == 3 || i == 5)
                {
                    TrimPaperDep = GetPaperDep(i) - (2 * Gripper);
                    TrimPaperWid = GetPaperWid(i) - (2 * Gripper) - 6;
                }
                if (i == 2 || i == 4 || i == 6)
                {
                    TrimPaperDep = GetPaperDep(i) - (2 * Gripper) - 6;
                    TrimPaperWid = GetPaperWid(i) - (2 * Gripper);
                }
                var N1 = (TrimPaperDep / OpenHeight);
                var M1 = (TrimPaperWid / OpenWidth);

                var AD1 = (N1 - 1) * Allowance;
                var AW1 = (M1 - 1) * Allowance;

                var TotalDep = (N1 * OpenHeight) + AD1;
                var TotalWid = (M1 * OpenWidth) + AW1;

                if (TotalDep > TrimPaperDep)
                {
                    if(i == 1)
                    {
                        FinalN1 = N1 - 1;
                        FinalDep = FinalN1 * OpenHeight;
                    }
                    if (i == 2)
                    {
                        FinalN2 = N1 - 1;
                        FinalDep = FinalN2 * OpenHeight;
                    }
                    if (i == 3)
                    {
                        FinalN3 = N1 - 1;
                        FinalDep = FinalN3 * OpenHeight;
                    }
                    if (i == 4)
                    {
                        FinalN4 = N1 - 1;
                        FinalDep = FinalN4 * OpenHeight;
                    }
                    if (i == 5)
                    {
                        FinalN5 = N1 - 1;
                        FinalDep = FinalN5 * OpenHeight;
                    }
                    if (i == 6)
                    {
                        FinalN6 = N1 - 1;
                        FinalDep = FinalN6 * OpenHeight;
                    }

                }
                else
                {
                    if (i == 1)
                    {
                        FinalN1 = N1;
                        FinalDep = FinalN1 * OpenHeight;
                    }
                    if (i == 2)
                    {
                        FinalN2 = N1;
                        FinalDep = FinalN2 * OpenHeight;
                    }
                    if (i == 3)
                    {
                        FinalN3 = N1;
                        FinalDep = FinalN3 * OpenHeight;
                    }
                    if (i == 4)
                    {
                        FinalN4 = N1;
                        FinalDep = FinalN4 * OpenHeight;
                    }
                    if (i == 5)
                    {
                        FinalN5 = N1;
                        FinalDep = FinalN5 * OpenHeight;
                    }
                    if (i == 6)
                    {
                        FinalN6 = N1;
                        FinalDep = FinalN6 * OpenHeight;
                    }
                }

                if (TotalWid > TrimPaperWid)
                {
                    if(i == 1)
                    {
                        FinalM1 = M1 - 1;
                        FinalWid = FinalM1 * OpenWidth;
                    }
                    if (i == 2)
                    {
                        FinalM2 = M1 - 1;
                        FinalWid = FinalM2 * OpenWidth;
                    }
                    if (i == 3)
                    {
                        FinalM3 = M1 - 1;
                        FinalWid = FinalM3 * OpenWidth;
                    }
                    if (i == 4)
                    {
                        FinalM4 = M1 - 1;
                        FinalWid = FinalM4 * OpenWidth;
                    }
                    if (i == 5)
                    {
                        FinalM5 = M1 - 1;
                        FinalWid = FinalM5 * OpenWidth;
                    }
                    if (i == 6)
                    {
                        FinalM6 = M1 - 1;
                        FinalWid = FinalM6 * OpenWidth;
                    }

                }
                else
                {
                    if (i == 1)
                    {
                        FinalM1 = M1;
                        FinalWid = FinalM1 * OpenWidth;
                    }
                    if (i == 2)
                    {
                        FinalM2 = M1;
                        FinalWid = FinalM2 * OpenWidth;
                    }
                    if (i == 3)
                    {
                        FinalM3 = M1;
                        FinalWid = FinalM3 * OpenWidth;
                    }
                    if (i == 4)
                    {
                        FinalM4 = M1;
                        FinalWid = FinalM4 * OpenWidth;
                    }
                    if (i == 5)
                    {
                        FinalM5 = M1;
                        FinalWid = FinalM5 * OpenWidth;
                    }
                    if (i == 6)
                    {
                        FinalM6 = M1;
                        FinalWid = FinalM6 * OpenWidth;
                    }
                }

                 

                var PaperArea = GetPaperDep(i) * GetPaperWid(i);
                var FinalArea = FinalDep * FinalWid;

                ratio.RatioValue = Convert.ToDecimal(FinalArea) / Convert.ToDecimal(PaperArea);
                ratios.Add(ratio);

                if (i == 1)
                {
                    Ratio1 = ratio.RatioValue;
                }
                if (i == 2)
                {
                    Ratio2 = ratio.RatioValue;
                }
                if (i == 3)
                {
                    Ratio3 = ratio.RatioValue;
                }
                if (i == 4)
                {
                    Ratio4 = ratio.RatioValue;
                }
                if (i == 5)
                {
                    Ratio5 = ratio.RatioValue;
                }
                if (i == 6)
                {
                    Ratio6 = ratio.RatioValue;
                }

            }
            int SelectionValue = 0;
            var HeighstValue = ratios.OrderByDescending(item => item.RatioValue).FirstOrDefault();
            int j = 1;
            for (j = 1; j <= 6; j++)
            {
                if (j == 1)
                {
                    if (Ratio1 == HeighstValue.RatioValue)
                    {
                        SelectionValue = j;
                        break;
                    }

                }
                if (j == 2)
                {
                    if (Ratio2 == HeighstValue.RatioValue)
                    {
                        SelectionValue = j;
                        break;
                    }
                }
                if (j == 3)
                {
                    if (Ratio3 == HeighstValue.RatioValue)
                    {
                        SelectionValue = j;
                        break;
                    }
                }
                if (j == 4)
                {
                    if (Ratio4 == HeighstValue.RatioValue)
                    {
                        SelectionValue = j;
                        break;
                    }
                }
                if (j == 5)
                {
                    if (Ratio5 == HeighstValue.RatioValue)
                    {
                        SelectionValue = j;
                        break;
                    }
                }
                if (j == 6)
                {
                    if (Ratio6 == HeighstValue.RatioValue)
                    {
                        SelectionValue = j;
                        break;
                    }
                }

            }
            if (SelectionValue == 1)
            {
                cCData.SheetSize = "635X901";
                cCData.PPValue = 2 * FinalN1 * FinalM1;
            }
            if (SelectionValue == 2)
            {
                cCData.SheetSize = "635X901";
                cCData.PPValue = 2 * FinalN2 * FinalM2;
            }
            if (SelectionValue == 3)
            {
                cCData.SheetSize = "635X901";
                cCData.PPValue = 2 * FinalN3 * FinalM3;
            }
            if (SelectionValue == 4)
            {
                cCData.SheetSize = "635X940";
                cCData.PPValue = 2 * FinalN4 * FinalM4;
            }
            if (SelectionValue == 5)
            {
                cCData.SheetSize = "787X1092";
                cCData.PPValue = 2 * FinalN5 * FinalM5;
            }
            if (SelectionValue == 6)
            {
                cCData.SheetSize = "787X1092";
                cCData.PPValue = 2 * FinalN6 * FinalM6;
            }
            return cCData;
        }

        public static int GetPaperDep(int Value)
        {
            int PaperDep = 0;
            if (Value == 1)
            {
                PaperDep = 635;
            }
            else if (Value == 2)
            {
                PaperDep = 901;
            }
            else if (Value == 3)
            {
                PaperDep = 635;
            }
            else if (Value == 4)
            {
                PaperDep = 940;
            }
            else if (Value == 5)
            {
                PaperDep = 787;
            }
            else if (Value == 6)
            {
                PaperDep = 1092;
            }
            return PaperDep;
        }

        public static int GetPaperWid(int Value)
        {
            int PaperWid = 0;
            if (Value == 1)
            {
                PaperWid = 901;
            }
            else if (Value == 2)
            {
                PaperWid = 635;
            }
            else if (Value == 3)
            {
                PaperWid = 940;
            }
            else if (Value == 4)
            {
                PaperWid = 635;
            }
            else if (Value == 5)
            {
                PaperWid = 1092;
            }
            else if (Value == 6)
            {
                PaperWid = 787;
            }
            return PaperWid;
        }

        public class Ratio
        {
            public decimal RatioValue { get; set; }
        }

        public class CCData
        {
            public string SheetSize { get; set; }
            public int Cut { get; set; }
            public int PPValue { get; set; }
        }
    }
}
