﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesCalc.Business.Common
{
    public class IdUtils
    {
        public static string generateSampleId()
        {
            return Guid.NewGuid().ToString();
        }
    }
}
