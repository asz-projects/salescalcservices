﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using SalesCalc.Business.Constants;

namespace SalesCalc.Business.Common
{
    public class Email
    {
        public static void SendEmail(string[] toList, string[] ccList, string[] bccList, string subject, string body, string[] fileAttachments, bool isBodyHtml)
        {

            using (var smtpClient = new SmtpClient())
            using (var mailMessage = new MailMessage())
            {
                if (toList != null)
                {
                    if ((toList[0] != null) && (toList[0] != string.Empty))
                    {
                        mailMessage.To.Add(string.Join(",", toList));
                    }
                }

                if (ccList != null)
                {
                    if ((ccList[0] != null) && (ccList[0] != string.Empty))
                    {
                        mailMessage.CC.Add(string.Join(",", ccList));
                    }
                }
                if (bccList != null)
                {
                    if ((bccList[0] != null) && (bccList[0] != string.Empty))
                    {
                        mailMessage.Bcc.Add(string.Join(",", bccList));
                    }
                }
                mailMessage.From = new MailAddress(SalesCalcConstants.EmailId);

                mailMessage.Subject = subject;

                mailMessage.IsBodyHtml = true;
                mailMessage.IsBodyHtml = isBodyHtml;
                mailMessage.Body = body;

                if (fileAttachments != null)
                    foreach (var attachment in fileAttachments)
                    {
                        // Create  the file attachment for this e-mail message.
                        var data = new Attachment(attachment, MediaTypeNames.Application.Octet);

                        // Add time stamp information for the file.
                        var disposition = data.ContentDisposition;
                        disposition.CreationDate = File.GetCreationTime(attachment);
                        disposition.ModificationDate = File.GetLastWriteTime(attachment);
                        disposition.ReadDate = File.GetLastAccessTime(attachment);

                        // Add the file attachment to this e-mail message.
                        mailMessage.Attachments.Add(data);
                    }
                smtpClient.Host = "smtp.outlook.com";
                smtpClient.Port = 587;
                smtpClient.Credentials = new System.Net.NetworkCredential(SalesCalcConstants.EmailId, SalesCalcConstants.Password);
                smtpClient.EnableSsl = true;

                //smtpClient.Credentials = CredentialCache.DefaultNetworkCredentials;
                smtpClient.Send(mailMessage);
            }

        }
    }
}
