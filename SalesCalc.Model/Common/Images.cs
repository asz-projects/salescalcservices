﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;
using SalesCalc.Business.Constants;
using static System.Net.Mime.MediaTypeNames;

namespace SalesCalc.Business.Common
{
    public class Images
    {
        public string DownloadOrderAddOnImages(string base64String, string dirPath)
        {
            String imgName = Convert.ToString(new Guid()) + ".jpg";

            byte[] imgByteArray = Convert.FromBase64String(base64String);
            File.WriteAllBytes(dirPath + imgName, imgByteArray);
            return imgName;
        }

        public static Bitmap Base64StringToBitmap(string base64String)
        {
            Bitmap bmpReturn = null;


            byte[] byteBuffer = Convert.FromBase64String(base64String);
            MemoryStream memoryStream = new MemoryStream(byteBuffer);


            memoryStream.Position = 0;


            bmpReturn = (Bitmap)Bitmap.FromStream(memoryStream);


            memoryStream.Close();
            memoryStream = null;
            byteBuffer = null;


            return bmpReturn;
        }


    }
}
