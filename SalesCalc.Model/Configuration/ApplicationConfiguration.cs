﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesCalc.Business.Configuration
{
   public class ApplicationConfiguration
    {
        public TokenConfiguration JwtToken { get; set; } = new TokenConfiguration();
    }

    public class TokenConfiguration
    {
        public string Issuer { get; set; } = "https://timetrakker.com";
        public string Audience { get; set; } = "https://timetrakker.com";

        public string SigningKey { get; set; } = "4rTGTad3Asdd$123ads*asd3iotgfd#12axads9310#";

        public int TokenTimeoutMinutes { get; set; } = 600;
    }
}
