﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesCalc.Business.Configuration
{
    /// <summary>
    /// Application instance that holds constants and a few other reusable objects
    /// </summary>
    public class App
    {
        /// <summary>
        /// Global static Configuration instance
        /// -- Note this has to be set if read from configuration settings
        /// -- in application startup
        /// </summary>
        public static ApplicationConfiguration Configuration;

        static App()
        {
            // always assign but this may be reassigned during application configuration
            Configuration = new ApplicationConfiguration();
        }

    }
}
