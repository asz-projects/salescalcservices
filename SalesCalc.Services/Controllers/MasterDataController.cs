﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SalesCalc.Business.Repository;
using SalesCalc.Services.ErrorHandling;

namespace SalesCalc.Services.Controllers
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    public class MasterDataController : ControllerBase
    {
        private readonly ILogger<MasterDataController> _logger;
        public MasterDataController(ILogger<MasterDataController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IActionResult GetAllFoldingsData()
        {
            return Ok(MasterDataRepository.GetAllFoldingsData());
        }


        [HttpGet]
        public IActionResult GetAllCaseBookData()
        {
            return Ok(MasterDataRepository.GetAllCaseBookData());
        }


        [HttpGet]
        public IActionResult GetAllLimpBoundData()
        {
            return Ok(MasterDataRepository.GetAllLimpBoundData());
        }

        [HttpGet]
        public IActionResult GetAllSaddleStitchedData()
        {
            return Ok(MasterDataRepository.GetAllSaddleStitchedData());
        }
    }
}