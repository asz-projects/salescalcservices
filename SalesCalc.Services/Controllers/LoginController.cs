﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using SalesCalc.AuditTrial;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SalesCalc.Business.Configuration;
using SalesCalc.Business.Model;
using SalesCalc.Services.ErrorHandling;
using SalesCalc.Services.Token;
using Westwind.AspNetCore.Security;
using SalesCalc.AuditTrial.Models;
using Microsoft.Extensions.Logging;

using Microsoft.AspNetCore.Authorization;
using SalesCalc.Business.Repository;

namespace SalesCalc.Services.Controllers
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    public class LoginController : ControllerBase
    {
        private readonly ILogger<LoginController> _logger;
        public LoginController(ILogger<LoginController> logger)
        {
            _logger = logger;
        }


        [HttpPost]
        public Login SalesCalc(string Email)
        {
            Login LoginData = new Login();
            if (Email == "salescalc@gmail.com")
            {
                var token = JwtHelper.GetJwtToken(
                            Email,
                            App.Configuration.JwtToken.SigningKey,
                            App.Configuration.JwtToken.Issuer,
                            App.Configuration.JwtToken.Audience,
                            TimeSpan.FromMinutes(App.Configuration.JwtToken.TokenTimeoutMinutes),
                            new[]
                            {
                    new Claim("UserState", Email)
                            });

                LoginData.Token = new JwtSecurityTokenHandler().WriteToken(token);
                LoginData.Success = true;
            }
            return LoginData;
        }


        [HttpPost]

        public IActionResult AuthenticateUser(User user)
        {
            var CommonToken = Request.Headers["CommonToken"];
            var Success = TokenValidator.CheckCommonToken(CommonToken);
            if (Success)
            {
                UserLogIn LoginData = new UserLogIn();
                var Data = UserRepository.AuthenticateUser(user);
                if (Data.Success)
                {
                    var hepp = JwtHelper.GetJwtToken(
                                     user.Email,
                                     App.Configuration.JwtToken.SigningKey,
                                     App.Configuration.JwtToken.Issuer,
                                     App.Configuration.JwtToken.Audience,
                                     TimeSpan.FromMinutes(App.Configuration.JwtToken.TokenTimeoutMinutes),
                                     new[]
                                     {
                    new Claim("UserState", user.Email)
                                     });

                    LoginData.Token = new JwtSecurityTokenHandler().WriteToken(hepp);

                    LoginData.Success = true;
                    LoginData.UserGuid = Data.UserGuid;
                    LoginData.RoleId = Data.RoleId;
                    var sucess = UserRepository.InsertToken(LoginData);
                    if (!sucess)
                    {
                        LoginData.Token = "token is not inserted";
                        LoginData.Success = false;
                        LoginData.UserGuid = Data.UserGuid;
                        LoginData.RoleId = 0;
                    }
                }
                _logger.LogInformation("user signed in, GmailId = {0}", user.Email);
                return Ok(LoginData);
            }
            else
            {
                return Unauthorized();
            }
        }
        [Authorize]
        [HttpPost]
        public IActionResult ChangePassword(ChangePassword password)
        {
            var Authorization = Request.Headers["Authorization"];
            var UserId = Request.Headers["UserId"];
            var Result = TokenValidator.CheckToken(Authorization, UserId);
            if (Result.Success)
            {

                return Ok(UserRepository.ChangePassword(password));
            }
            else
            {
                return Unauthorized();
            }

        }
        [HttpGet]
        public IActionResult ForgotPasswordMail(string EmailId)
        {
            var CommonToken = Request.Headers["CommonToken"];
            var Success = TokenValidator.CheckCommonToken(CommonToken);
            //if (Success)
            //{
            return Ok(UserRepository.ForgotPasswordMail(EmailId));

            //}
            //else
            //{
            //    return Unauthorized();
            //}
        }
        [HttpGet]
        public IActionResult ForgotPasswordOTP(string EmailId)
        {
            var CommonToken = Request.Headers["CommonToken"];
            var Success = TokenValidator.CheckCommonToken(CommonToken);
            if (Success)
            {
                return Ok(UserRepository.ForgotPasswordOTP(EmailId));

            }
            else
            {
                return Unauthorized();
            }
        }

        [HttpGet]
        public IActionResult CheckOTP(string EmailId, string otp)
        {
            var CommonToken = Request.Headers["CommonToken"];
            var Success = TokenValidator.CheckCommonToken(CommonToken);
            if (Success)
            {
                return Ok(UserRepository.CheckOTP(EmailId, otp));

            }
            else
            {
                return Unauthorized();
            }
        }
        [HttpGet]
        public IActionResult CreateNewPassword(string EmailId, string password)
        {
            var CommonToken = Request.Headers["CommonToken"];
            var Success = TokenValidator.CheckCommonToken(CommonToken);
            if (Success)
            {
                return Ok(UserRepository.CreateNewPassword(EmailId, password));

            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [HttpGet]
        public IActionResult Logout()
        {
            var Authorization = Request.Headers["Authorization"];
            var UserId = Request.Headers["UserId"];
            var Result = TokenValidator.CheckToken(Authorization, UserId);
            if (Result.Success)
            {
                return Ok(UserRepository.DeleteToken(Authorization, UserId));
            }
            else
            {
                return Unauthorized();
            }
        }
    }
}