﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SalesCalc.Business.Model;
using SalesCalc.Business.Repository;
using SalesCalc.Services.ErrorHandling;
using SalesCalc.Services.Token;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalesCalc.Services.Controllers
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    public class UserController : ControllerBase
    {
        private readonly ILogger<UserController> _logger;
        public UserController(ILogger<UserController> logger)
        {
            _logger = logger;
        }

        [Authorize]
        [HttpGet]
        public IActionResult GetAllUsers()
        {
            var Authorization = Request.Headers["Authorization"];
            var UserId = Request.Headers["UserId"];
            var Result = TokenValidator.CheckToken(Authorization, UserId);
            if (Result.Success)
            {
                return Ok(UserRepository.GetAllUsers());
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [HttpGet]
        public IActionResult GetAllUserTypes()
        {
            var Authorization = Request.Headers["Authorization"];
            var UserId = Request.Headers["UserId"];
            var Result = TokenValidator.CheckToken(Authorization, UserId);
            if (Result.Success)
            {
                return Ok(UserRepository.GetAllUserTypes());
            }
            else
            {
                return Unauthorized();
            }
        }
        [Authorize]
        [HttpGet]
        public IActionResult GetUserById(string UserGuid)
        {
            var Authorization = Request.Headers["Authorization"];
            var UserId = Request.Headers["UserId"];
            var Result = TokenValidator.CheckToken(Authorization, UserId);
            if (Result.Success)
            {
                return Ok(UserRepository.GetUserById(UserGuid));
            }
            else
            {
                return Unauthorized();
            }
        }
        [Authorize]
        [HttpPost]
        public IActionResult InsertUser(User user)
        {
            var Authorization = Request.Headers["Authorization"];
            var UserId = Request.Headers["UserId"];
            var Result = TokenValidator.CheckToken(Authorization, UserId);
            if (Result.Success)
            {
                return Ok(UserRepository.InsertUser(user));
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [HttpGet]
        public IActionResult DeleteUser(string UserGuid)
        {
            var Authorization = Request.Headers["Authorization"];
            var UserId = Request.Headers["UserId"];
            var Result = TokenValidator.CheckToken(Authorization, UserId);
            if (Result.Success)
            {
                return Ok(UserRepository.DeleteUser(UserGuid));
            }
            else
            {
                return Unauthorized();
            }
        }
        [Authorize]
        [HttpPost]
        public IActionResult UpdateUserDetails(User user)
        {
            var Authorization = Request.Headers["Authorization"];
            var UserId = Request.Headers["UserId"];
            var Result = TokenValidator.CheckToken(Authorization, UserId);
            if (Result.Success)
            {
                return Ok(UserRepository.UpdateUserDetails(user));
            }
            else
            {
                return Unauthorized();
            }
        }
        [Authorize]
        [HttpGet]
        public IActionResult CheckPermissionsByUserGuid(string UserGuid, int permissionId)
        {
            var Authorization = Request.Headers["Authorization"];
            var UserId = Request.Headers["UserId"];
            var Result = TokenValidator.CheckToken(Authorization, UserId);
            if (Result.Success)
            {
                return Ok(UserRepository.CheckPermissionsByUserGuid(UserGuid, permissionId));
            }
            else
            {
                return Unauthorized();
            }
        }

      
       
       



    }
}
