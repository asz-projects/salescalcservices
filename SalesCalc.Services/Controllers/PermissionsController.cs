﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SalesCalc.Business.Repository;
using SalesCalc.Services.ErrorHandling;
using SalesCalc.Services.Token;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace SalesCalc.Services.Controllers
{
    
    [Route("api/[controller]/[Action]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]

    public class PermissionsController : ControllerBase
    {
        private readonly ILogger<PermissionsController> _logger;
        public PermissionsController(ILogger<PermissionsController> logger)
        {
            _logger = logger;
        }
        [Authorize]
        [HttpGet]
        public IActionResult GetAllPermissions()
        {
            var Authorization = Request.Headers["Authorization"];
            var UserId = Request.Headers["UserId"];
            var Result = TokenValidator.CheckToken(Authorization, UserId);
            if (Result.Success)
            {

                return Ok(PermissionsRepository.GetAllPermissions());
            }
            else
            {
                return Unauthorized();
            }
        }
    }
}
