﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SalesCalc.Business.Model;
using SalesCalc.Business.Repository;
using SalesCalc.Services.ErrorHandling;
using SalesCalc.Services.Token;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalesCalc.Services.Controllers
{
    
    [Route("api/[controller]/[Action]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]

    public class RoleController : ControllerBase
    {
        private readonly ILogger<RoleController> _logger;
        public RoleController(ILogger<RoleController> logger)
        {
            _logger = logger;
        }
        [Authorize]
        [HttpGet]
        public IActionResult GetAllRoles()
        {
            var Authorization = Request.Headers["Authorization"];
            var UserId = Request.Headers["UserId"];
            var Result = TokenValidator.CheckToken(Authorization, UserId);
            if (Result.Success)
            {
                return Ok(RoleRepository.GetAllRoles());
            }
            else
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [HttpGet]
        public IActionResult GetRolesByUserType(int UserTypeId)
        {
            var Authorization = Request.Headers["Authorization"];
            var UserId = Request.Headers["UserId"];
            var Result = TokenValidator.CheckToken(Authorization, UserId);
            if (Result.Success)
            {
                return Ok(RoleRepository.GetRolesByUserType(UserTypeId));
            }
            else
            {
                return Unauthorized();
            }
        }
        [Authorize]
        [HttpPost]
        public IActionResult GetRoleById(Role role)
        {
            var Authorization = Request.Headers["Authorization"];
            var UserId = Request.Headers["UserId"];
            var Result = TokenValidator.CheckToken(Authorization, UserId);
            if (Result.Success)
            {
                return Ok(RoleRepository.GetRoleById(role));
            }
            else
            {
                return Unauthorized();
            }
        }
        [Authorize]
        [HttpPost]
        public IActionResult SideNavRolesById(Role role)
        {
            var Authorization = Request.Headers["Authorization"];
            var UserId = Request.Headers["UserId"];
            var Result = TokenValidator.CheckToken(Authorization, UserId);
            if (Result.Success)
            {
                return Ok(RoleRepository.SideNavRolesById(role));
            }
            else
            {
                return Unauthorized();
            }
        }
        [Authorize]
        [HttpPost]
        public IActionResult SaveRole(Role role)
        {
            var Authorization = Request.Headers["Authorization"];
            var UserId = Request.Headers["UserId"];
            var Result = TokenValidator.CheckToken(Authorization, UserId);
            if (Result.Success)
            {
                if (role.RoleId == 0)
                {
                    role.RoleId = RoleRepository.SaveRole(role);
                }
                else
                {
                    role.RoleId = RoleRepository.SaveRole(role);
                    RoleRepository.DeleteRoleMappingData(role);
                }

                return Ok(RoleRepository.CreateRoleMappingData(role));
            }
            else
            {
                return Unauthorized();
            }

        }
        [Authorize]
        [HttpPost]
        public IActionResult DeleteRole(Role role)
        {
            var Authorization = Request.Headers["Authorization"];
            var UserId = Request.Headers["UserId"];
            var Result = TokenValidator.CheckToken(Authorization, UserId);
            if (Result.Success)
            {
                return Ok(RoleRepository.DeleteRole(role));
            }
            else
            {
                return Unauthorized();
            }
        }
    }
}
