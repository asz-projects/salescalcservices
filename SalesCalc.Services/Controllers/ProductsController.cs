﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SalesCalc.Business.Model;
using SalesCalc.Business.Repository;
using SalesCalc.Services.ErrorHandling;

namespace SalesCalc.Services.Controllers
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    [ServiceFilter(typeof(ApiExceptionFilter))]
    public class ProductsController : ControllerBase
    {
        private readonly ILogger<ProductsController> _logger;
        public ProductsController(ILogger<ProductsController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IActionResult GetAllProducts()
        {
            return Ok(ProductsRepository.GetAllProducts());
        }

        [HttpPost]
        public IActionResult GetExcelReportForFolding(FoldingFinalData finalData)
        {
            return Ok(ProductsRepository.GetExcelReportForFolding(finalData));
        }


        [HttpPost]
        public IActionResult GetExcelReportForCaseBook(CaseBookFinalData caseBookFinalData)
        {
            return Ok(ProductsRepository.GetExcelReportForCaseBook(caseBookFinalData));
        }

        [HttpPost]
        public IActionResult GetExcelReportForLimpBound(LimpBoundFinalData limpBoundFinalData)
        {
            return Ok(ProductsRepository.GetExcelReportForLimpBound(limpBoundFinalData));
        }

        [HttpPost]
        public IActionResult GetExcelReportForSaddleStitched(SaddleStitchedFinalData saddleStitchedFinalData)
        {
            return Ok(ProductsRepository.GetExcelReportForSaddleStitched(saddleStitchedFinalData));
        }

    }
}