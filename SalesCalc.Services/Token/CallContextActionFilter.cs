﻿using SalesCalc.AuditTrial;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using SalesCalc.AuditTrial.Models;

namespace SalesCalc.Services.Token
{
    public class CallContextActionFilter : IActionFilter
    {
        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext.HttpContext.Request.Headers["Authorization"] != StringValues.Empty)
            {
                try
                {
                    var loginUser = filterContext.HttpContext.Request.Headers["UserId"];
                    if (loginUser != StringValues.Empty && loginUser != "")
                    {

                        var parameters = GetParameterValues(filterContext);

                        AuditTrials.LogActivity(new Activity
                        {
                            SessionId = filterContext.HttpContext.Request.Headers["Authorization"].ToString().Replace("Bearer ", ""),
                            LoginId = loginUser.ToString(),
                            Application = "SalesCalc",
                            Module = filterContext.Controller.ToString(),
                            Action = filterContext.ActionDescriptor.DisplayName,
                            ActionResult = true,
                            ActivityOn = DateTime.Now,
                            OtherInfo = parameters
                        });


                    }

                }
                catch (Exception ex)
                {
                    // Don't raise any exceptions if the logging fails.
                }
            }
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
            //To do : after the action executes  
        }

        private static string GetParameterValues(ActionExecutingContext filterContext)
        {

            StringBuilder builder = new StringBuilder();

            foreach (KeyValuePair<string, object> entry in filterContext.ActionArguments)
            {
                builder.AppendFormat("ParamterName = {0}; ParamterValue = {1} \n ", entry.Key, JsonConvert.SerializeObject(entry.Value) ?? "");
            }

            return builder.ToString();

        }

    }
}
