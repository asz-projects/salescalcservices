﻿using Microsoft.Extensions.Primitives;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using SalesCalc.Business.Constants;
using SalesCalc.Business.Model;
using MySql.Data.MySqlClient;

namespace SalesCalc.Services.Token
{
    public class TokenValidator
    {
        public static string GenrateToken(string UserId)
        {
            return CreateToken(UserId);
        }

        private static string Secret = "ERMN05OPLoDvbTTa/QkqLNMI7cPLguaRyHzyg7n5qNBVjQmtBhz4SzYh4NBVCXi3KJHlSXKP+oi2+bXr6CUYTR==";
        public static string CreateToken(string username)
        {
            byte[] key = Convert.FromBase64String(Secret);
            SymmetricSecurityKey securityKey = new SymmetricSecurityKey(key);
            SecurityTokenDescriptor descriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] {
                      new Claim(ClaimTypes.Name, username)}),
                Expires = DateTime.UtcNow.AddMinutes(30),
                SigningCredentials = new SigningCredentials(securityKey,
                SecurityAlgorithms.HmacSha256Signature)
            };

            JwtSecurityTokenHandler handler = new JwtSecurityTokenHandler();
            JwtSecurityToken token = handler.CreateJwtSecurityToken(descriptor);
            return handler.WriteToken(token);
        }


        public static ClaimsPrincipal GetPrincipal(string token)
        {
            try
            {
                JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
                JwtSecurityToken jwtToken = (JwtSecurityToken)tokenHandler.ReadToken(token);
                if (jwtToken == null)
                    return null;
                byte[] key = Convert.FromBase64String(Secret);
                TokenValidationParameters parameters = new TokenValidationParameters()
                {
                    RequireExpirationTime = true,
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    IssuerSigningKey = new SymmetricSecurityKey(key)
                };
                SecurityToken securityToken;
                ClaimsPrincipal principal = tokenHandler.ValidateToken(token,
                      parameters, out securityToken);
                return principal;
            }
            catch
            {
                return null;
            }
        }
        public static string ValidateToken(string token,string userId)
        {
            string username = null;
            ClaimsPrincipal principal = GetPrincipal(token);
            if (principal == null)
                return null;
            ClaimsIdentity identity = null;
            try
            {
                identity = (ClaimsIdentity)principal.Identity;
            }
            catch (NullReferenceException)
            {
                return null;
            }
            Claim usernameClaim = identity.FindFirst(ClaimTypes.Name);
            username = usernameClaim.Value;
            return username;
        }
        public static TokenValidation CheckToken(StringValues Authorization, StringValues UserId)
        {
            TokenValidation tokenValidation = new TokenValidation();
            string AuthorizationString = string.Empty;
            string AuthorizationData = string.Empty;
            string UserIdData = string.Empty;
            var AuthorizationArray = Authorization.ToArray();
            if (AuthorizationArray.Length != 0)
            {
                AuthorizationString = AuthorizationArray[0];
                AuthorizationData = AuthorizationString.Replace("Bearer ", "");
            }
            var UserIdArray = UserId.ToArray();
            if (UserIdArray.Length != 0)
            {
                UserIdData = UserIdArray[0];
            }
            bool isSuccessFul = false;
            using (MySqlConnection connection = new MySqlConnection(SalesCalcConstants.DBConnectionString))
            {
                using (MySqlCommand command = new MySqlCommand(SalesCalcConstants.sp_CheckToken, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Connection.Open();
                    command.CommandTimeout = 180000;
                    command.Parameters.AddWithValue("@Token_", AuthorizationData);
                    command.Parameters.AddWithValue("@User_Id", UserIdData);
                    MySqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        isSuccessFul = (Convert.ToBoolean(reader["Success"]));
                    }
                    command.Connection.Close();
                }
            }
            if (isSuccessFul)
            {
                tokenValidation.UserId = UserIdData;
                tokenValidation.Success = true;
            }
            else
            {
                tokenValidation.UserId = "";
                tokenValidation.Success = false;
            }
            return tokenValidation;
        }

        public static bool CheckCommonToken(StringValues CommonToken)
        {
            string AuthorizationString = string.Empty;
            string AuthorizationData = string.Empty;
            var AuthorizationArray = CommonToken.ToArray();
            if (AuthorizationArray.Length != 0)
            {
                AuthorizationString = AuthorizationArray[0];
                AuthorizationData = AuthorizationString.Replace("Bearer ", "");
            }

            if (AuthorizationData == "zSo39f79-6869-43c2-bye3-1292f8b71don")
            {
                return true;
            }
            else
            {
                return false;
            }
        }


    }
}
