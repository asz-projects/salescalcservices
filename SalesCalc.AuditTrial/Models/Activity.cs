﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesCalc.AuditTrial.Models
{
    public class Activity
    {
        public int ActivityId { get; set; }
        public string SessionId { get; set; }
        public string LoginId { get; set; }
        public string Application { get; set; }
        public string Module { get; set; }
        public string Action { get; set; }
        public bool ActionResult { get; set; }
        public DateTime ActivityOn { get; set; }
        public string OtherInfo { get; set; }
    }
}
