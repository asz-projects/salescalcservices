﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace SalesCalc.AuditTrial.SqlServer
{
    public class QueryHelper
    {
        private readonly string _connectionString;

        public QueryHelper(string connectionString)
        {
            _connectionString = connectionString;
        }

        public IEnumerable<T> Read<T>(ParameterizedQuery pQuery, Func<IDataReader, T> make)
        {
            return Read(pQuery.MySqlQuery, pQuery.MySqlParameters, make);
        }

        public IEnumerable<T> Read<T>(string Mysql, List<MySqlParameter> MysqlParameters, Func<IDataReader, T> make)
        {
            using (var connection = new MySqlConnection(_connectionString))
            {
                connection.Open();

                using (var command = CreateCommand(Mysql, connection, MysqlParameters))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            yield return make(reader);
                        }
                    }
                }

                connection.Close();
            }
        }

        public object Scalar(ParameterizedQuery pQuery)
        {
            return Scalar(pQuery.MySqlQuery, pQuery.MySqlParameters);
        }

        public object Scalar(string Mysql, List<MySqlParameter> MysqlParameters)
        {
            object result;

            using (var connection = new MySqlConnection(_connectionString))
            {
                connection.Open();

                using (var command = CreateCommand(Mysql, connection, MysqlParameters))
                {
                    result = command.ExecuteScalar();
                }

                connection.Close();
            }

            return result;
        }

        public int Insert(ParameterizedQuery pQuery)
        {
            return Insert(pQuery.MySqlQuery, pQuery.MySqlParameters);
        }

        public int Insert(string Mysql, List<MySqlParameter> MysqlParameters)
        {
            int id;

            using (var connection = new MySqlConnection(_connectionString))
            {
                connection.Open();

                using (var command = CreateCommand(Mysql + ";SELECT SCOPE_IDENTITY();", connection, MysqlParameters))
                {
                    id = int.Parse(command.ExecuteScalar().ToString());
                }

                connection.Close();
            }

            return id;
        }

        public int Update(ParameterizedQuery pQuery)
        {
            return ExecuteNonQuery(pQuery.MySqlQuery, pQuery.MySqlParameters);
        }

        public int Update(string sql, List<MySqlParameter> MysqlParameters)
        {
            return ExecuteNonQuery(sql, MysqlParameters);
        }

        public int Delete(ParameterizedQuery pQuery)
        {
            return ExecuteNonQuery(pQuery.MySqlQuery, pQuery.MySqlParameters);
        }

        public int Delete(string Mysql, List<MySqlParameter> MysqlParameters)
        {
            return ExecuteNonQuery(Mysql, MysqlParameters);
        }

        public DataTable ExecuteQuery(string Mysql, List<MySqlParameter> MysqlParameters)
        {
            var data = new DataTable();

            using (var connection = new MySqlConnection(_connectionString))
            {
                connection.Open();
                using (var command = CreateCommand(Mysql, connection, MysqlParameters))
                using (var MysqlDataAdapter = new MySqlDataAdapter(command))
                {
                    command.CommandText = Mysql;
                    command.CommandType = CommandType.Text;
                    MysqlDataAdapter.Fill(data);
                }
                connection.Close();
            }

            return data;
        }

        public int ExecuteNonQuery(ParameterizedQuery pQuery)
        {
            return ExecuteNonQuery(pQuery.MySqlQuery, pQuery.MySqlParameters);
        }

        public int ExecuteNonQuery(string Mysql, List<MySqlParameter> MysqlParameters)
        {
            int rowsAffected;

            using (var connection = new MySqlConnection(_connectionString))
            {
                connection.Open();
                using (var command = CreateCommand(Mysql, connection, MysqlParameters))
                {
                    rowsAffected = command.ExecuteNonQuery();
                }
                connection.Close();
            }

            return rowsAffected;
        }

        public void ExecuteNonQuery(List<ParameterizedQuery> pQueries, bool inTransaction)
        {
            using (var connection = new MySqlConnection(_connectionString))
            {
                connection.Open();

                if (inTransaction)
                {
                    using (MySqlTransaction transaction = connection.BeginTransaction())
                    {
                        try
                        {
                            ExecuteNonQuery(connection, pQueries, transaction);

                            transaction.Commit();
                        }
                        catch
                        {
                            transaction.Rollback();
                            connection.Close();
                            throw;
                        }
                    }
                }
                else
                {
                    ExecuteNonQuery(connection, pQueries);
                }

                connection.Close();
            }
        }

        //public void ExportToCsv(string file, string separator, bool includeHeaders, ParameterizedQuery pQuery)
        //{
        //    using (var connection = new SqlConnection(_connectionString))
        //    {
        //        connection.Open();

        //        using (var command = CreateCommand(pQuery.SqlQuery, connection, pQuery.SqlParameters))
        //        {
        //            using (var reader = command.ExecuteReader())
        //            {
        //                CsvUtil.GenerateCsvFile(reader, file, separator, includeHeaders);
        //            }
        //        }

        //        connection.Close();
        //    }
        //}

        //public void ExportToExcel(string file, ParameterizedQuery pQuery)
        //{
        //    using (var connection = new SqlConnection(_connectionString))
        //    {
        //        connection.Open();

        //        using (var command = CreateCommand(pQuery.SqlQuery, connection, pQuery.SqlParameters))
        //        {
        //            using (var reader = command.ExecuteReader())
        //            {
        //                ExcelUtil.GenerateExcel(reader, file);
        //            }
        //        }

        //        connection.Close();
        //    }
        //}

        //public void CreateDatabaseView(string viewName, string sqlQuery)
        //{
        //    var sql = "CREATE VIEW " + viewName + " AS " + sqlQuery;

        //    DeleteDatabaseView(viewName);

        //    using (var connection = new SqlConnection(_connectionString))
        //    {
        //        connection.Open();

        //        using (var command = CreateCommand(sql, connection, null))
        //        {
        //            command.ExecuteScalar();
        //        }

        //        connection.Close();
        //    }
        //}

        //public void DeleteDatabaseView(string viewName)
        //{
        //    var sql = "DROP VIEW " + viewName;

        //    var schema = "dbo";
        //    var viewQualifiedName = schema + "." + viewName;
        //    // var view = ViewsHelper.GetViewsDetails(_connectionString, new List<string> { viewQualifiedName });

        //    var isExistingView = ViewsHelper.GetViewNames(_connectionString).Contains(viewQualifiedName);
        //    if (!isExistingView) return;

        //    using (var connection = new SqlConnection(_connectionString))
        //    {
        //        connection.Open();

        //        using (var command = CreateCommand(sql, connection, null))
        //        {
        //            command.ExecuteScalar();
        //        }

        //        connection.Close();
        //    }
        //}

        void ExecuteNonQuery(MySqlConnection connection, List<ParameterizedQuery> pQueries, MySqlTransaction transaction = null)
        {
            foreach (var pQuery in pQueries)
            {
                using (var command = CreateCommand(pQuery.MySqlQuery, connection, pQuery.MySqlParameters, transaction))
                {
                    command.ExecuteNonQuery();
                    command.Parameters.Clear();
                }
            }
        }

        MySqlCommand CreateCommand(string Mysql, MySqlConnection connection, List<MySqlParameter> MysqlParameters, MySqlTransaction transaction = null)
        {
            var command = connection.CreateCommand();
            //command.CommandTimeout = 0;
            command.CommandType = CommandType.Text;
            command.CommandText = Mysql;
            if (transaction != null)
                command.Transaction = transaction;

            if (MysqlParameters != null)
                command.Parameters.AddRange(MysqlParameters.ToArray());

            return command;
        }

    }

    public class ParameterizedQuery
    {
        public string MySqlQuery { get; set; }
        public List<MySqlParameter> MySqlParameters { get; set; }
    }
}