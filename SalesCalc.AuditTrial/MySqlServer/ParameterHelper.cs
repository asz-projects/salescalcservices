﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace SalesCalc.AuditTrial.SqlServer
{
    public static class ParameterHelper
    {
        public static MySqlParameter CreateSqlParameter(string parameterName, object value, MySqlDbType type, string typeName = "")
        {
            return new MySqlParameter
            {
                ParameterName = parameterName,
                Value = value ?? DBNull.Value,
                MySqlDbType = type
                //TypeName = typeName
            };
        }
    }
}
