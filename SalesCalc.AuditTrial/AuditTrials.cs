﻿using System;
using SalesCalc.AuditTrial.Models;
using SalesCalc.AuditTrial.Repository;

namespace SalesCalc.AuditTrial
{
    public static class AuditTrials
    {
        public static void LogActivity(Activity activity)
        {
            AuditTrialRepository.LogActivity(activity);
        }
    }
}
