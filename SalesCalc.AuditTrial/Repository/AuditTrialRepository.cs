﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using SalesCalc.AuditTrial.Models;
using SalesCalc.AuditTrial.SqlServer;

namespace SalesCalc.AuditTrial.Repository
{
    public class AuditTrialRepository : BaseRepository
    {
        public static void LogActivity(Activity activity)
        {

            const string Mysql = @"INSERT INTO ActivityLogs (SessionId, LoginId, Application, Module, Action, ActionResult, ActivityOn, OtherInfo)
                                 VALUES (@sessionId, @loginId, @application, @module, @action ,@actionResult, @activityOn, @OtherInfo)";

            MySqlServerQueryHelper.Insert(Mysql, Take(activity));
        }

        private static List<MySqlParameter> Take(Activity activity)
        {
            var parameters = new List<MySqlParameter>
            {
                ParameterHelper.CreateSqlParameter("@sessionId", activity.SessionId, MySqlDbType.VarChar),
                ParameterHelper.CreateSqlParameter("@loginId", activity.LoginId, MySqlDbType.VarChar),
                ParameterHelper.CreateSqlParameter("@application", activity.Application, MySqlDbType.VarChar),
                ParameterHelper.CreateSqlParameter("@module", activity.Module, MySqlDbType.VarChar),
                ParameterHelper.CreateSqlParameter("@action", activity.Action, MySqlDbType.VarChar),
                ParameterHelper.CreateSqlParameter("@actionResult", activity.ActionResult, MySqlDbType.Bit),
                ParameterHelper.CreateSqlParameter("@activityOn", DateTime.Now, MySqlDbType.DateTime),
                ParameterHelper.CreateSqlParameter("@OtherInfo", activity.OtherInfo, MySqlDbType.VarChar)
            };

            return parameters;
        }
    }
}
