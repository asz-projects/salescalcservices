﻿using System;
using System.Collections.Generic;
using System.Text;
using SalesCalc.AuditTrial.SqlServer;
using SalesCalc.Business.Constants;

namespace SalesCalc.AuditTrial.Repository
{
    public class BaseRepository
    {
        internal static QueryHelper MySqlServerQueryHelper = new QueryHelper(SalesCalcConstants.DBConnectionString);
    }
}
